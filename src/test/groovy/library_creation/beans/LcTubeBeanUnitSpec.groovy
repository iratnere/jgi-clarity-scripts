package library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationTubeIllumina
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationTubePacBio
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcTubeBean
import spock.lang.Shared
import spock.lang.Specification

class LcTubeBeanUnitSpec extends Specification {
    @Shared
    String correctIndex = 'dummyIndex'

    def setup() {
    }

    def cleanup() {
    }

    void "test validate Illumina"(
            indexName, status, failureMode, volume, concentration, actualTemplateSize,
            molarityQc,pcr
    ) {
        setup:
            def errorMsg
        LibraryCreationTubeIllumina adapter = Mock(LibraryCreationTubeIllumina)
        LcTubeBean bean = new LcTubeBean(
                    libraryLimsId: 'libraryLimsId',
                    libraryCreationQueue: 'Illumina Regular Fragment, 300bp',
                    lcAdapter: adapter
            )
            bean.libraryQcResult = new DropDownList(value:status)
            bean.libraryQcFailureMode = new DropDownList(value:failureMode)
            bean.libraryVolume = volume
            bean.libraryConcentration = concentration
            bean.libraryActualTemplateSize= actualTemplateSize
            bean.libraryIndexName = indexName
            bean.libraryMolarityQc = molarityQc
            bean.numberPcrCycles = pcr
        when:
            errorMsg = bean.validate()
        then:
            errorMsg
        where:
        indexName     |status       | failureMode       | volume |concentration |actualTemplateSize |molarityQc |pcr
        null          | Analyte.PASS | null              | null  |null    |null    | null  |null
        correctIndex  | Analyte.PASS | null              | 0     |0       |0       | 0     |0
        ''            | Analyte.PASS | null              | 27.27 |123.23  |123.123 | 27.27 |1
        correctIndex  | Analyte.FAIL | null              | 27.27 |123.23  |123.123 | 27.27 |1
        correctIndex  | Analyte.FAIL | null              | 0     |0       |0       | 0     |0
        correctIndex  | Analyte.FAIL | null              | null  |null    |null    | null  |null
        correctIndex  |null         | null              | 27.27  |123.123       |123.123            | 27.27     |1
        correctIndex  |'dummy'      | null              | 27.27  |123.123       |123.123            | 27.27     |1
        correctIndex  | Analyte.PASS | 'testFailureMode' | 27.27 |123.123 |123.123 | 27.27 |1
    }


    void "test validate no exception thrown Illumina"(
            indexName, status, failureMode, volume, concentration, actualTemplateSize,
            molarityQc,pcr
    ) {
        setup:
        LibraryCreationTubeIllumina adapter = Mock(LibraryCreationTubeIllumina)
        LcTubeBean bean = new LcTubeBean(
                libraryLimsId: 'libraryLimsId',
                libraryCreationQueue: 'Illumina Regular Fragment, 300bp',
                lcAdapter: adapter
        )
        bean.libraryQcResult = new DropDownList(value:status)
        bean.libraryQcFailureMode = new DropDownList(value:failureMode)
        bean.libraryVolume = volume
        bean.libraryConcentration = concentration
        bean.libraryActualTemplateSize= actualTemplateSize
        bean.libraryIndexName = indexName
        bean.libraryMolarityQc = molarityQc
        bean.numberPcrCycles = pcr
        when:
        def errorMsg = bean.validate()
        then:
        assert errorMsg == null
        where:
        indexName     |status        | failureMode       | volume   |concentration      |actualTemplateSize  |molarityQc     |pcr
        correctIndex  | Analyte.PASS | null              | 27.27 |123.123 |123.123 | 27.27 |1
        'n/a'         | Analyte.PASS | null              | 27.27 |123.123 |123.123 | 27.27 |1
        correctIndex  | Analyte.FAIL | 'testFailureMode' | 27.27 |123.123 |123.123 | 27.27 |1
        correctIndex  | Analyte.FAIL | 'testFailureMode' | 0     |0       |0       | 0     |0
        correctIndex  | Analyte.FAIL | 'testFailureMode' | null  |null    |null    | null  |null
    }

    void "test validate LMP"(
            indexName, status, failureMode, volume, concentration, actualTemplateSize,
            molarityQc,pcr
    ) {
        setup:
        def errorMsg
        LibraryCreationTubeIllumina adapter = Mock(LibraryCreationTubeIllumina)
        LcTubeBean bean = new LcTubeBean(
                libraryLimsId: 'libraryLimsId',
                libraryCreationQueue: 'Illumina Nextera LMP',
                lcAdapter: adapter
        )
        bean.libraryQcResult = new DropDownList(value:status)
        bean.libraryQcFailureMode = new DropDownList(value:failureMode)
        bean.libraryVolume = volume
        bean.libraryConcentration = concentration
        bean.libraryActualTemplateSize= actualTemplateSize
        bean.libraryIndexName = indexName
        bean.libraryMolarityQc = molarityQc
        bean.numberPcrCycles = pcr
        when:
        errorMsg = bean.validate()
        then:
        errorMsg
        when:
        bean.libraryActualInsertSize = 45.34
        errorMsg = bean.validate()
        then:
        !errorMsg
        noExceptionThrown()
        where:
        indexName     |status        | failureMode       | volume              |concentration      |actualTemplateSize  |molarityQc     |pcr
        correctIndex  | Analyte.PASS | null | 27.27 |123.23 |123.123 | 27.27 |1
    }

    void "test validate PacBio"(status, failureMode, volume,
                                concentration, actualTemplateSize
    ) {
        setup:
        def errorMsg
        LibraryCreationTubePacBio adapter = Mock(LibraryCreationTubePacBio)
        LcTubeBean bean = new LcTubeBean(
                libraryLimsId: 'libraryLimsId',
                libraryCreationQueue: 'PacBio >10kb w/ AMPure Bead Size Selection',
                lcAdapter: adapter
        )
        bean.libraryQcResult = new DropDownList(value:status)
        bean.libraryQcFailureMode = new DropDownList(value:failureMode)
        bean.libraryVolume = volume
        bean.libraryConcentration = concentration
        bean.libraryActualTemplateSize= actualTemplateSize
        when:
        errorMsg = bean.validate()
        then:
        errorMsg
        where:
        status        | failureMode       | volume              |concentration      |actualTemplateSize
        Analyte.PASS | null              | null  |null    |null
        Analyte.PASS | null              | 0.0   |0.0     |0.0
        Analyte.PASS | null              | 27.27 |123.123 |null
        Analyte.PASS | null              | 27.27 |null    |123.123
        null          | null              | 27.27               |123.123            |123.123
        'dummy'       | null              | 27.27               |123.123            |123.123
        Analyte.FAIL | null              | 27.27 |123.123 |123.123
        Analyte.PASS | 'testFailureMode' | 27.27 |123.123 |123.123
    }

    void "test validate no exception thrown PacBio"(status, failureMode, volume,
                                                    concentration, actualTemplateSize
    ) {
        setup:
        LibraryCreationTubePacBio adapter = Mock(LibraryCreationTubePacBio)
        LcTubeBean bean = new LcTubeBean(
                libraryLimsId: 'libraryLimsId',
                libraryCreationQueue: 'PacBio >10kb w/ AMPure Bead Size Selection',
                lcAdapter: adapter
        )
        bean.libraryQcResult = new DropDownList(value:status)
        bean.libraryQcFailureMode = new DropDownList(value:failureMode)
        bean.libraryVolume = volume
        bean.libraryConcentration = concentration
        bean.libraryActualTemplateSize= actualTemplateSize
        when:
        def errorMsg = bean.validate()
        then:
        assert errorMsg == null
        where:
        status        | failureMode       | volume              |concentration      |actualTemplateSize
        Analyte.PASS | null              | 27.27 |123.123 |123.123
        Analyte.FAIL | 'testFailureMode' | 27.27 |123.123 |123.123
        Analyte.FAIL | 'testFailureMode' | 0     |0       |0
        Analyte.FAIL | 'testFailureMode' | null  |null    |null
    }
}