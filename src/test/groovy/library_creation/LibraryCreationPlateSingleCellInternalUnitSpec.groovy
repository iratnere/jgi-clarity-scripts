package library_creation

import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationPlateSingleCellInternal
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification

class LibraryCreationPlateSingleCellInternalUnitSpec extends Specification{

    def setup() {
    }

    def cleanup() {
    }

    void "test getExpectedSortedContainerNodePlacements"() {
        setup:
        LibraryCreationProcess process = Mock(LibraryCreationProcess)
        LibraryCreationPlateSingleCellInternal adapter = new LibraryCreationPlateSingleCellInternal(process)
        def locationsMap = [
                'Plate1':['E:1', 'C:1', 'A:1'],
                'Plate2':['B:1', 'A:12'],
                'Plate3':['C:1', 'A:1', 'E:1']
        ]
        when:
        def result = adapter.getExpectedSortedContainerNodePlacements(locationsMap)
        then:
        result == [['A:1', 'C:1', 'E:1'],['A:12','B:1'],['A:1', 'C:1', 'E:1']]
        noExceptionThrown()
    }

    void "test checkLibraryLocations"() {
        setup:
        LibraryCreationProcess process = Mock(LibraryCreationProcess)
        LibraryCreationPlateSingleCellInternal adapter = new LibraryCreationPlateSingleCellInternal(process)
        when:
        adapter.checkLibraryLocations(
                [['A:1','C:1','E:1'], ['A:1'], ['A:1','C:1','E:1']],
                [['A:1','C:1','E:1'], ['A:1'], ['A:1','C:1','E:1']]
        )
        then:
        noExceptionThrown()
        when:
        adapter.checkLibraryLocations(
                [['A:1','C:1','E:1'],['A:1'],['A:1','C:1','E:1']],
                [['A:1','C:1','E:1'],['B:1'],['A:1','C:1','E:1']]
        )
        then:
        WebException exception = thrown()
        exception.message == LibraryCreationPlateSingleCellInternal.ERROR_MSG_PLACEMENTS
        when:
        adapter.checkLibraryLocations(
                [['A:1'], ['A:1','C:1','E:1'], ['A:1','C:1','E:1']],
                [['A:1','C:1','E:1'], ['A:1'], ['A:1','C:1','E:1']]
        )
        then:
        noExceptionThrown()
        when:
        adapter.checkLibraryLocations(
                [['E:1','A:1','C:1'],['A:1'],['A:1','C:1','E:1']],
                [['A:1','C:1','E:1'],['A:1'],['A:1','C:1','E:1']]
        )
        then:
        WebException exception1 = thrown()
        exception1.message == LibraryCreationPlateSingleCellInternal.ERROR_MSG_PLACEMENTS
    }

        void "test convertLibraryMolarity"() {
        setup:
            BigDecimal molarity = 5.43
            def result
        when:
            result = LibraryCreationPlateSingleCellInternal.convertLibraryMolarity(molarity)
        then:
            result == molarity * 1000
        when:
            molarity = -5
            result = LibraryCreationPlateSingleCellInternal.convertLibraryMolarity(molarity)
        then:
            result == molarity * 1000
        when:
            molarity = 0.0
            result = LibraryCreationPlateSingleCellInternal.convertLibraryMolarity(molarity)
        then:
            result == BigDecimal.ZERO
        when:
            molarity = null
            result = LibraryCreationPlateSingleCellInternal.convertLibraryMolarity(molarity)
        then:
            result == null
            noExceptionThrown()
    }

    void "test convertFragmentAnalyzerWell"() {
        setup:
            def row = 'A'
            def column = 2
            def result
        when:
            // Upper left coner of the 384 well plate quadrant
            result = LibraryCreationPlateSingleCellInternal.convertFragmentAnalyzerWell(row, column)
        then:
            result == 'A3'
        when:
            row = 'G'
            column = 7
            result = LibraryCreationPlateSingleCellInternal.convertFragmentAnalyzerWell(row, column)
        then:
            result == 'M13'
        when:
            row = 'H'
            column = 12
            result = LibraryCreationPlateSingleCellInternal.convertFragmentAnalyzerWell(row, column)
        then:
            result == 'O23'
        when:
            row = 'A'
            column = 12
            result = LibraryCreationPlateSingleCellInternal.convertFragmentAnalyzerWell(row, column)
        then:
            result == 'A23'
        when:
            row = 'D'
            column = 11
            result = LibraryCreationPlateSingleCellInternal.convertFragmentAnalyzerWell(row, column)
        then:
            result == 'G21'
    }

//Library plate 384 wells A1, C1, E1...O21 go into 96 index plate wells A1, B1, C1... H11
    void "test convert384Well"() {
        setup:
            String row = 'A'
            Integer column = 3
            def result
        when:
            // Upper left coner of the 384 well plate quadrant
            result = LibraryCreationPlateSingleCellInternal.convert384Well(row, column)
        then:
            result == 'A:2'
        when:
            row = 'M'
            column = 13
            result = LibraryCreationPlateSingleCellInternal.convert384Well(row, column)
        then:
            result == 'G:7'
        when:
            row = 'O'
            column = 23
            result = LibraryCreationPlateSingleCellInternal.convert384Well(row, column)
        then:
            result == 'H:12'
        when:
            row = 'A'
            column = 23
            result = LibraryCreationPlateSingleCellInternal.convert384Well(row, column)
        then:
            result == 'A:12'
        when:
            row = 'G'
            column = 21
            result = LibraryCreationPlateSingleCellInternal.convert384Well(row, column)
        then:
            result == 'D:11'
    }
}
