package library_creation

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.OutputTube
import spock.lang.Specification

/*
https://docs.google.com/document/d/1wCrj96ViyBELWnp3aeZk3f59neLDtG2SLnqLWqtC-vc/edit#heading=h.xdif52imvfe0
 */

class OutputTubeUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test getBarcodeRegExGroup"() {
        setup:
            def barcode = '01A01-04182018-02'//[01A01-04182018-02, 01, A01, 04182018, 02]
        when:
            def group = OutputTube.getBarcodeRegExGroup(barcode)
        then:
            group
            5 == group.size()
            ['01A01-04182018-02', '01', 'A01', '04182018', '02'] == group
    }

    void "test getIndexSet"() {
        setup:
            def barcode = '01A01-04182018-02'//[01A01-04182018-02, 01, A01, 04182018, 02]
        when:
            def indexSet = OutputTube.getIndexSet(barcode)
        then:
            indexSet
            indexSet == '01'
        when:
            barcode = '01H11-04182018-02'
            indexSet = OutputTube.getIndexSet(barcode)
        then:
            indexSet
            indexSet == '01'
    }

    void "test getLocation"() {
        setup:
            def barcode = '01A01-04182018-02'//[01A01-04182018-02, 01, A01, 04182018, 02]
        when:
            def location = OutputTube.getLocation(barcode)
        then:
            location
            location == 'A:1'
        when:
            barcode = '01H11-04182018-02'
            location = OutputTube.getLocation(barcode)
        then:
            location
            location == 'H:11'
    }

    void "test getBarcodeKey"() {
        setup:
        Map<String, List<Index>> barcodeToIndexes = barcodeToIndexes(illuminaTubes)
            String indexContainerBarcode = '01A01-04182018-02'
/*
            def barcodeKeySet = [
                    '01A01-04182018-02',
                    '03B12-040218-03',
                    '01C11-10262017-01',
                    '03D05-04022018-01',
                    'IT049',
                    'IT052',
                    'N702-N522',
                    'N704-N513'
            ] as Set
*/
        when:
            def result = OutputTube.getBarcodeKey(indexContainerBarcode)
        then:
            result == indexContainerBarcode
        when:
            indexContainerBarcode = '03B12-040218-03'
            result = OutputTube.getBarcodeKey(indexContainerBarcode)
        then:
            result == indexContainerBarcode
        when:
            indexContainerBarcode = 'IT052'
            result = OutputTube.getBarcodeKey(indexContainerBarcode)
        then:
            result == indexContainerBarcode
        when:
            indexContainerBarcode = 'N704-N513'
            result = OutputTube.getBarcodeKey(indexContainerBarcode)
        then:
            result == indexContainerBarcode
    }

    void "test findIndex udf null"() {
        setup:
            def barcode = null
        when:
            def result = OutputTube.findIndex(barcode, barcodeToIndexes(illuminaTubes))
        then:
            !result
            noExceptionThrown()
    }

    void "test findIndex tubes"() {
        setup:
            String barcode = 'N728-N502'
        when:
            def result = OutputTube.findIndex(barcode, barcodeToIndexes(illuminaTubes))
        then://['N728-N502','N728-N502','N728-N502','TGCAGCTA-CTCTCTAT',   null]
            result
            result.indexSet == barcode
            result.indexName == barcode
            result.indexSequence == 'TGCAGCTA-CTCTCTAT'
        when:
            barcode = 'IT052'
            result = OutputTube.findIndex(barcode, barcodeToIndexes(illuminaTubes))
        then:// ['IT052',   'IT052',    'IT052',    'AACAAA',               null],
            result
            result.indexSet == barcode
            result.indexName == barcode
            result.indexSequence == 'AACAAA'
    }

    def stockPlate = [
            //println "['$k','$i.indexSet','$i.indexName','$i.indexSequence','$i.plateLocation'],"
            ['01A01-04022018-03','01','N716-S513','ACTCGCTA-TCGACTAG','A:1'],
            ['01D11-04022018-03','01','N728-S517','TGCAGCTA-GCGTAAGA','D:11'],
            ['01E12-04022018-03','01','N729-S518','TCGACGTC-CTATTAAG','E:12'],
            ['01H12-04022018-03','01','N729-S522','	TCGACGTC-TTATGCGA','H:12']
    ]

    void "test findIndex stock plate"() {
        setup:
            String barcode = '01A01-04022018-03'
        when:
            def result = OutputTube.findIndex(barcode, barcodeToIndexes(stockPlate))
        then:
            result
            result.indexSet == '01'
            result.indexName == 'N716-S513'
            result.indexSequence == 'ACTCGCTA-TCGACTAG'
            result.plateLocation == 'A:1'
        when:
            barcode = '01E12-04022018-03'
            result = OutputTube.findIndex(barcode, barcodeToIndexes(stockPlate))
        then:
            result
            result.indexSet == '01'
            result.indexName == 'N729-S518'
            result.indexSequence == 'TCGACGTC-CTATTAAG'
            result.plateLocation == 'E:12'
        when:
            barcode = '01D11-04022018-03'
            result = OutputTube.findIndex(barcode, barcodeToIndexes(stockPlate))
        then:
            result
            result.indexSet == '01'
            result.indexName == 'N728-S517'
            result.indexSequence == 'TGCAGCTA-GCGTAAGA'
            result.plateLocation == 'D:11'
    }

    Map<String, List<Index>> barcodeToIndexes(List rows){
        Map<String, List<Index>> barcodeToIndexes = [:]
        rows.each{ List<String> row ->
            Index index = new Index(
                    indexSet: row[1],
                    indexName: row[2],
                    indexSequence: row[3],
                    plateLocation: row[4]
            )
            if (!barcodeToIndexes[row[0]])
                barcodeToIndexes[row[0]] = []
            barcodeToIndexes[row[0]].add(index)
        }
        barcodeToIndexes
    }

    def illuminaTubes = [
    //println "['$k','$i.indexSet','$i.indexName','$i.indexSequence','$i.plateLocation'],"
            ['IT049',   'IT049',    'IT049',    'AAACAT',               null],
            ['IT052',   'IT052',    'IT052',    'AACAAA',               null],
            ['j019s14', 'j019s14',  'j019s14',  'TAACGCAC-GTCTGCTT',    null],
            ['N728-N502','N728-N502','N728-N502','TGCAGCTA-CTCTCTAT',   null]
    ]
}