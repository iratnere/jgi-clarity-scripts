package library_creation

import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationPlateSingleCellInternal
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.OutputPlate
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcEchoTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.webtransaction.util.IdGenerator
import org.junit.jupiter.api.Test
import spock.lang.Specification

class LcPrepareLibraryCreationSheetUnitSpec extends Specification{

    @Test
    void "test validateNumberOfOutputContainers"() {
        setup:
        List<ContainerNode> containerNodeOutputs = []
        LibraryCreationProcess process = mockProcess(2, containerNodeOutputs)
        OutputPlate outputPlate = new OutputPlate(process)
        when:
        outputPlate.validateNumberOfOutputContainers(containerNodeOutputs)
        then:
        def e = thrown WebException
        e.message.contains("""
                Output plate does not pass custom validation.
                Number of output containers with value '2' exceeds maximum value 1.
                Please select a single 96 well plate as the output container.
            """)
    }

    static final def ECHO_FILE_HEADER = [
            'Source Plate Name',
            'Source Plate Barcode',
            'Source Row',
            'Source Column',
            'Destination Plate Name',
            'Destination Plate Barcode',
            'Destination Row',
            'Destination Column',
            'Transfer Volume'
    ]

    void "test beanHeaders"() {
        when:
        def headers = LibraryCreationPlateSingleCellInternal.beanHeaders
        then:
        assert ECHO_FILE_HEADER.join(',') == headers.join(',')
        assert headers.size() == ECHO_FILE_HEADER.size()
        noExceptionThrown()
    }

    void "test createEchoFileRow"() {
        setup:
        def beanValues = [
                'sourceContainerName',
                'sourceContainerUdfLabel',
                2,
                3,
                'destinationPlateName',
                'destinationPlateBarcode',
                6,
                7,
                LibraryCreationPlateSingleCellInternal.DEFAULT_LIBRARY_VOLUME_NL
        ]
        LcEchoTableBean bean = new LcEchoTableBean()
        bean.sourcePlateName = beanValues[0]
        bean.sourcePlateBarcode = beanValues[1]

        bean.sourceColumn = beanValues[3] as BigInteger
        bean.destinationPlateName = beanValues[4]

        bean.destinationRow = beanValues[6] as BigInteger
        bean.destinationColumn = beanValues[7] as BigInteger
        bean.transferVolumeNl = beanValues[8] as BigDecimal
        bean.destinationPlateBarcode = beanValues[5]
        bean.sourceRow = beanValues[2] as BigInteger
        when:
        def row = LibraryCreationPlateSingleCellInternal.createEchoFileRow(bean)
        then:
        assert row.join(',') == beanValues.join(',')
        noExceptionThrown()
    }

    ContainerNode generateMockContainerNodePlate(name = 'ContainerNode') {
        ContainerNode containerNodePlate = Mock(ContainerNode)
        containerNodePlate.id >> generateDummyId(name)
        containerNodePlate.getIsPlate() >> true
        containerNodePlate.isNinetySixWellPlate >> true
        containerNodePlate.getContainerTypeEnum() >> ContainerTypes.WELL_PLATE_96
        containerNodePlate
    }

    ArtifactNode generateMockArtifactNode() {
        def id = generateDummyId(ArtifactNode.class.simpleName)

        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

    LibraryCreationProcess mockProcess(int numberOutputs, List<ContainerNode> containerNodeOutputs) {
        List<ArtifactNode> artifactNodeOutputs = []
        ProcessNode processNode = Mock(ProcessNode)
        (1..numberOutputs).each {
            ContainerNode containerNodePlate = generateMockContainerNodePlate("ContainerNode$it")
            containerNodeOutputs << containerNodePlate
            ArtifactNode artifactNodeOutput = generateMockArtifactNode()
            artifactNodeOutput.getContainerNode() >> containerNodePlate
            artifactNodeOutputs << artifactNodeOutput
        }
        processNode.outputAnalytes >> artifactNodeOutputs

        LibraryCreationProcess process = Mock(LibraryCreationProcess)//, constructorArgs:[processNode])
        process.getContainerNodes(_) >> containerNodeOutputs
        process
    }
}
