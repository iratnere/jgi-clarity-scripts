package library_creation

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.OutputPlate
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import spock.lang.Specification

/*
https://docs.google.com/document/d/1wCrj96ViyBELWnp3aeZk3f59neLDtG2SLnqLWqtC-vc/edit#heading=h.xdif52imvfe0
 */

class OutputPlateUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test getConvertedLocation 384 plate"() {
        setup:
            def location = 'E:5'
            ContainerLocation containerLocation = Mock(ContainerLocation)
            containerLocation.rowColumn >> location

        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
            libraryStock.containerLocation >> containerLocation
            libraryStock.containerType >> ContainerTypes.WELL_PLATE_384.value
        when:
            def result = OutputPlate.getConvertedLocation(libraryStock)
        then:
            result
            result == 'C:3'
    }

    void "test getConvertedLocation 96 plate"() {
        setup:
            def location = 'E:5'
            ContainerLocation containerLocation = Mock(ContainerLocation)
            containerLocation.rowColumn >> location

        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
            libraryStock.containerLocation >> containerLocation
            libraryStock.containerType >> ContainerTypes.WELL_PLATE_96.value
        when:
            def result = OutputPlate.getConvertedLocation(libraryStock)
        then:
            result
            result == 'E:5'
    }

    String indexContainerBarcode
    def result

    void "test getBarcodeKey"() {
        setup:
            indexContainerBarcode = '01-04182018-02'
            def barcodeKeySet = [
                    '01-04182018-02',
                    '02-10262018-05',
                    '02-10262018-02',
                    'Illumina_TruSeq-HT_Dual-Index',
                    'Illumina_Single-Index'
            ] as Set
        when:
            result = OutputPlate.getBarcodeKey(indexContainerBarcode, barcodeKeySet)
        then:
            result == indexContainerBarcode
        when:
            indexContainerBarcode = 'Illumina_TruSeq-HT_Dual-Index'
            result = OutputPlate.getBarcodeKey(indexContainerBarcode, barcodeKeySet)
        then:
            result == indexContainerBarcode
        when:
            indexContainerBarcode = 'Illumina_Single-Index'
            result = OutputPlate.getBarcodeKey(indexContainerBarcode, barcodeKeySet)
        then:
            result == indexContainerBarcode
    }

    void "test findIndex udf null"() {
        setup:
            def set = null
            def location = 'A:10'
            ContainerLocation containerLocation = Mock(ContainerLocation)
            containerLocation.rowColumn >> location
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
            libraryStock.udfIndexContainerBarcode >> set
            libraryStock.containerLocation >> containerLocation
        when:
            def result = OutputPlate.findIndex(libraryStock, barcodeToIndexes(illuminaSingleIndexes))
        then:
            !result
            noExceptionThrown()
    }

    void "test findIndex single"() {
        setup:
            def set = 'Illumina_smRNA_Single-Index'
            def location = 'G:3'
            ContainerLocation containerLocation = Mock(ContainerLocation)
            containerLocation.rowColumn >> location
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
            libraryStock.udfIndexContainerBarcode >> set
            libraryStock.containerLocation >> containerLocation
        when:
            def result = OutputPlate.findIndex(libraryStock, barcodeToIndexes(illuminaSingleIndexes))
        then:
            result
            result.indexSet == set
            result.indexName == 'IT023'
            result.indexSequence == 'GAGTGG'
            result.plateLocation == location
    }

    void "test findIndex dual"() {
        setup:
            def set = 'Illumina_TruSeq-HT_Dual-Index'
            def location = 'B:12'
            ContainerLocation containerLocation = Mock(ContainerLocation)
            containerLocation.rowColumn >> location
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
            libraryStock.udfIndexContainerBarcode >> set
            libraryStock.containerLocation >> containerLocation
        when:
            def result = OutputPlate.findIndex(libraryStock, barcodeToIndexes(illuminaDualIndexes))
        then://'D712-D502','AGCGATAG-ATAGAGGC','B:12
            result
            result.indexSet == set
            result.indexName == 'D712-D502'
            result.indexSequence == 'AGCGATAG-ATAGAGGC'
            result.plateLocation == location
    }

    void "test findIndex stock plate"() {
        setup:
            def barcode = '01-04182018-02'
            def set = '01'
            def location = 'E:8'
            ContainerLocation containerLocation = Mock(ContainerLocation)
            containerLocation.rowColumn >> location
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
            libraryStock.udfIndexContainerBarcode >> barcode
            libraryStock.containerLocation >> containerLocation
        when:
            def result = OutputPlate.findIndex(libraryStock, barcodeToIndexes(stockPlate))
        then://  ['01-04182018-02','01','Tag058_i7-Tag058_i5','E:8','TGCTTGGT-ACCAAGCA'],
            result
            result.indexSet == set
            result.indexName == 'Tag058_i7-Tag058_i5'
            result.indexSequence == 'TGCTTGGT-ACCAAGCA'
            result.plateLocation == location
    }

    Map<String, List<Index>> barcodeToIndexes(List rows){
        Map<String, List<Index>> barcodeToIndexes = [:]
        rows.each{ List<String> row ->
            Index index = new Index(
                    indexSet: row[1],
                    indexName: row[2],
                    indexSequence: row[4],
                    plateLocation: row[3]
            )
            if (!barcodeToIndexes[row[0]])
                barcodeToIndexes[row[0]] = []
            barcodeToIndexes[row[0]].add(index)
        }
        barcodeToIndexes
    }

    def illuminaSingleIndexes = [
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT001','A:1','ATCACG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT002','B:1','CGATGT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT003','C:1','TTAGGC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT004','D:1','TGACCA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT005','E:1','ACAGTG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT006','F:1','GCCAAT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT007','G:1','CAGATC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT008','H:1','ACTTGA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT009','A:2','GATCAG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT010','B:2','TAGCTT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT011','C:2','GGCTAC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT012','D:2','CTTGTA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT013','E:2','AGTCAA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT014','F:2','AGTTCC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT015','G:2','ATGTCA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT016','H:2','CCGTCC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT017','A:3','GTAGAG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT018','B:3','GTCCGC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT019','C:3','GTGAAA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT020','D:3','GTGGCC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT021','E:3','GTTTCG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT022','F:3','CGTACG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT023','G:3','GAGTGG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT024','H:3','GGTAGC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT025','A:4','ACTGAT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT026','B:4','ATGAGC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT027','C:4','ATTCCT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT028','D:4','CAAAAG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT029','E:4','CAACTA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT030','F:4','CACCGG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT031','G:4','CACGAT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT032','H:4','CACTCA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT033','A:5','CAGGCG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT034','B:5','CATGGC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT035','C:5','CATTTT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT036','D:5','CCAACA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT037','E:5','CGGAAT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT038','F:5','CTAGCT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT039','G:5','CTATAC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT040','H:5','CTCAGA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT041','A:6','GACGAC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT042','B:6','TAATCG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT043','C:6','TACAGC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT044','D:6','TATAAT'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT045','E:6','TCATTC'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT046','F:6','TCCCGA'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT047','G:6','TCGAAG'],
            ['Illumina_smRNA_Single-Index','Illumina_smRNA_Single-Index','IT048','H:6','TCGGCA']
    ]

    def stockPlate = [
            //println "['$k','$i.indexSet','$i.indexName','$i.plateLocation','$i.indexSequence'],"
            ['01-04182018-02','01','Tag004_i7-Tag004_i5','A:1','AGCGTGTT-AACACGCT'],
            ['01-04182018-02','01','Tag000_i7-Tag000_i5','B:1','GAGCTCAA-TTGAGCTC'],
            ['01-04182018-02','01','Tag064_i7-Tag064_i5','C:1','ATAGCGGT-ACCGCTAT'],
            ['01-04182018-02','01','Tag001_i7-Tag001_i5','D:1','CGGTTGTT-AACAACCG'],
            ['01-04182018-02','01','Tag393_i7-Tag393_i5','E:1','TACCAACC-GGTTGGTA'],
            ['01-04182018-02','01','Tag002_i7-Tag002_i5','F:1','GCCTTGTT-AACAAGGC'],
            ['01-04182018-02','01','Tag514_i7-Tag514_i5','G:1','CTGACACA-TGTGTCAG'],
            ['01-04182018-02','01','Tag509_i7-Tag509_i5','H:1','TCGAACCA-TGGTTCGA'],
            ['01-04182018-02','01','Tag005_i7-Tag005_i5','A:2','CCAGTGTT-AACACTGG'],
            ['01-04182018-02','01','Tag390_i7-Tag390_i5','B:2','TGTACACC-GGTGTACA'],
            ['01-04182018-02','01','Tag006_i7-Tag006_i5','C:2','TCGCTGTT-AACAGCGA'],
            ['01-04182018-02','01','Tag391_i7-Tag391_i5','D:2','AGCTAACC-GGTTAGCT'],
            ['01-04182018-02','01','Tag008_i7-Tag008_i5','E:2','GGACTGTT-AACAGTCC'],
            ['01-04182018-02','01','Tag515_i7-Tag515_i5','F:2','ACGGAACA-TGTTCCGT'],
            ['01-04182018-02','01','Tag011_i7-Tag011_i5','G:2','GTTCGGTT-AACCGAAC'],
            ['01-04182018-02','01','Tag426_i7-Tag426_i5','H:2','ACAGCAAC-GTTGCTGT'],
            ['01-04182018-02','01','Tag013_i7-Tag013_i5','A:3','CGTAGGTT-AACCTACG'],
            ['01-04182018-02','01','Tag389_i7-Tag389_i5','B:3','TCATCACC-GGTGATGA'],
            ['01-04182018-02','01','Tag014_i7-Tag014_i5','C:3','TGTGCGTT-AACGCACA'],
            ['01-04182018-02','01','Tag504_i7-Tag504_i5','D:3','ACCATCCA-TGGATGGT'],
            ['01-04182018-02','01','Tag016_i7-Tag016_i5','E:3','GCTACGTT-AACGTAGC'],
            ['01-04182018-02','01','Tag422_i7-Tag422_i5','F:3','CGCTTAAC-GTTAAGCG'],
            ['01-04182018-02','01','Tag017_i7-Tag017_i5','G:3','TCCGAGTT-AACTCGGA'],
            ['01-04182018-02','01','Tag417_i7-Tag417_i5','H:3','AGTCTCAC-GTGAGACT'],
            ['01-04182018-02','01','Tag018_i7-Tag018_i5','A:4','CCTCAGTT-AACTGAGG'],
            ['01-04182018-02','01','Tag384_i7-Tag384_i5','B:4','TTCGTACC-GGTACGAA'],
            ['01-04182018-02','01','Tag021_i7-Tag021_i5','C:4','ACGGTCTT-AAGACCGT'],
            ['01-04182018-02','01','Tag412_i7-Tag412_i5','D:4','GTAACGAC-GTCGTTAC'],
            ['01-04182018-02','01','Tag027_i7-Tag027_i5','E:4','GAACGCTT-AAGCGTTC'],
            ['01-04182018-02','01','Tag382_i7-Tag382_i5','F:4','ATTGAGCC-GGCTCAAT'],
            ['01-04182018-02','01','Tag028_i7-Tag028_i5','G:4','GTGAGCTT-AAGCTCAC'],
            ['01-04182018-02','01','Tag411_i7-Tag411_i5','H:4','CAATCGAC-GTCGATTG'],
            ['01-04182018-02','01','Tag030_i7-Tag030_i5','A:5','CCTTCCTT-AAGGAAGG'],
            ['01-04182018-02','01','Tag405_i7-Tag405_i5','B:5','TGACTGAC-GTCAGTCA'],
            ['01-04182018-02','01','Tag032_i7-Tag032_i5','C:5','GTCTCCTT-AAGGAGAC'],
            ['01-04182018-02','01','Tag406_i7-Tag406_i5','D:5','ACGATGAC-GTCATCGT'],
            ['01-04182018-02','01','Tag033_i7-Tag033_i5','E:5','TACGCCTT-AAGGCGTA'],
            ['01-04182018-02','01','Tag373_i7-Tag373_i5','F:5','AGTAGTCC-GGACTACT'],
            ['01-04182018-02','01','Tag034_i7-Tag034_i5','G:5','AGAGCCTT-AAGGCTCT'],
            ['01-04182018-02','01','Tag369_i7-Tag369_i5','H:5','TCTCTTCC-GGAAGAGA'],
            ['01-04182018-02','01','Tag035_i7-Tag035_i5','A:6','GAGGACTT-AAGTCCTC'],
            ['01-04182018-02','01','Tag380_i7-Tag380_i5','B:6','AGAATGCC-GGCATTCT'],
            ['01-04182018-02','01','Tag037_i7-Tag037_i5','C:6','GCTGGATT-AATCCAGC'],
            ['01-04182018-02','01','Tag501_i7-Tag501_i5','D:6','AGCAAGCA-TGCTTGCT'],
            ['01-04182018-02','01','Tag040_i7-Tag040_i5','E:6','CGACCATT-AATGGTCG'],
            ['01-04182018-02','01','Tag394_i7-Tag394_i5','F:6','TCGGTTAC-GTAACCGA'],
            ['01-04182018-02','01','Tag043_i7-Tag043_i5','G:6','CACGTTGT-ACAACGTG'],
            ['01-04182018-02','01','Tag392_i7-Tag392_i5','H:6','GTTCAACC-GGTTGAAC'],
            ['01-04182018-02','01','Tag050_i7-Tag050_i5','A:7','CAGAGTGT-ACACTCTG'],
            ['01-04182018-02','01','Tag507_i7-Tag507_i5','B:7','GGATACCA-TGGTATCC'],
            ['01-04182018-02','01','Tag051_i7-Tag051_i5','C:7','ACCTCTGT-ACAGAGGT'],
            ['01-04182018-02','01','Tag418_i7-Tag418_i5','D:7','TGGATCAC-GTGATCCA'],
            ['01-04182018-02','01','Tag052_i7-Tag052_i5','E:7','CTTGCTGT-ACAGCAAG'],
            ['01-04182018-02','01','Tag423_i7-Tag423_i5','F:7','GCCATAAC-GTTATGGC'],
            ['01-04182018-02','01','Tag053_i7-Tag053_i5','G:7','ATGCCTGT-ACAGGCAT'],
            ['01-04182018-02','01','Tag385_i7-Tag385_i5','H:7','GAAGTACC-GGTACTTC'],
            ['01-04182018-02','01','Tag055_i7-Tag055_i5','A:8','CGAACTGT-ACAGTTCG'],
            ['01-04182018-02','01','Tag387_i7-Tag387_i5','B:8','TAGTGACC-GGTCACTA'],
            ['01-04182018-02','01','Tag056_i7-Tag056_i5','C:8','TGGCATGT-ACATGCCA'],
            ['01-04182018-02','01','Tag511_i7-Tag511_i5','D:8','CACTGACA-TGTCAGTG'],
            ['01-04182018-02','01','Tag058_i7-Tag058_i5','E:8','TGCTTGGT-ACCAAGCA'],
            ['01-04182018-02','01','Tag419_i7-Tag419_i5','F:8','CATACCAC-GTGGTATG'],
            ['01-04182018-02','01','Tag063_i7-Tag063_i5','G:8','TTGTCGGT-ACCGACAA'],
            ['01-04182018-02','01','Tag528_i7-Tag528_i5','H:8','CCAAGCAA-TTGCTTGG'],
            ['01-04182018-02','01','Tag065_i7-Tag065_i5','A:9','TAACCGGT-ACCGGTTA'],
            ['01-04182018-02','01','Tag503_i7-Tag503_i5','B:9','ATGGTCCA-TGGACCAT'],
            ['01-04182018-02','01','Tag066_i7-Tag066_i5','C:9','GTCTAGGT-ACCTAGAC'],
            ['01-04182018-02','01','Tag506_i7-Tag506_i5','D:9','AAGAGCCA-TGGCTCTT'],
            ['01-04182018-02','01','Tag075_i7-Tag075_i5','E:9','AGAAGCGT-ACGCTTCT'],
            ['01-04182018-02','01','Tag381_i7-Tag381_i5','F:9','TATTCGCC-GGCGAATA'],
            ['01-04182018-02','01','Tag077_i7-Tag077_i5','G:9','TGTACCGT-ACGGTACA'],
            ['01-04182018-02','01','Tag489_i7-Tag489_i5','H:9','AACGGTCA-TGACCGTT'],
            ['01-04182018-02','01','Tag078_i7-Tag078_i5','A:10','CCATACGT-ACGTATGG'],
            ['01-04182018-02','01','Tag496_i7-Tag496_i5','B:10','TTGAGGCA-TGCCTCAA'],
            ['01-04182018-02','01','Tag079_i7-Tag079_i5','C:10','TTGGACGT-ACGTCCAA'],
            ['01-04182018-02','01','Tag396_i7-Tag396_i5','D:10','CACCTTAC-GTAAGGTG'],
            ['01-04182018-02','01','Tag081_i7-Tag081_i5','E:10','ACCGTAGT-ACTACGGT'],
            ['01-04182018-02','01','Tag378_i7-Tag378_i5','F:10','GTGAATCC-GGATTCAC'],
            ['01-04182018-02','01','Tag082_i7-Tag082_i5','G:10','GCGATAGT-ACTATCGC'],
            ['01-04182018-02','01','Tag402_i7-Tag402_i5','H:10','CTCGATAC-GTATCGAG'],
            ['01-04182018-02','01','Tag083_i7-Tag083_i5','A:11','CGTTGAGT-ACTCAACG'],
            ['01-04182018-02','01','Tag500_i7-Tag500_i5','B:11','GATCGAGT-ACTCGATC'],
            ['01-04182018-02','01','Tag085_i7-Tag085_i5','C:11','GATCGAGT-ACTCGATC'],
            ['01-04182018-02','01','Tag497_i7-Tag497_i5','D:11','CTATCGCA-TGCGATAG'],
            ['01-04182018-02','01','Tag088_i7-Tag088_i5','E:11','AGTGCAGT-ACTGCACT'],
            ['01-04182018-02','01','Tag370_i7-Tag370_i5','F:11','GACATTCC-GGAATGTC'],
            ['01-04182018-02','01','Tag091_i7-Tag091_i5','G:11','ACACCAGT-ACTGGTGT'],
            ['01-04182018-02','01','Tag398_i7-Tag398_i5','H:11','TGTGGTAC-GTACCACA'],
            ['01-04182018-02','01','Tag100_i7-Tag100_i5','A:12','GAGTCTCT-AGAGACTC'],
            ['01-04182018-02','01','Tag097_i7-Tag097_i5','B:12','GCATGTCT-AGACATGC'],
            ['01-04182018-02','01','Tag480_i7-Tag480_i5','C:12','ATGCACGA-TCGTGCAT'],
            ['01-04182018-02','01','Tag098_i7-Tag098_i5','D:12','CAAGGTCT-AGACCTTG'],
            ['01-04182018-02','01','Tag484_i7-Tag484_i5','E:12','TGTCCAGA-TCTGGACA'],
            ['01-04182018-02','01','Tag099_i7-Tag099_i5','F:12','TAGCGTCT-AGACGCTA'],
            ['01-04182018-02','01','Tag447_i7-Tag447_i5','G:12','AGCTCCTA-TAGGAGCT'],
            ['01-04182018-02','01','Tag445_i7-Tag445_i5','H:12','CTCAGCTA-TAGCTGAG']
    ]

    def illuminaDualIndexes = [
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D701-D501','A:1','ATTACTCG-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D701-D502','B:1','ATTACTCG-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D701-D503','C:1','ATTACTCG-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D701-D504','D:1','ATTACTCG-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D701-D505','E:1','ATTACTCG-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D701-D506','F:1','ATTACTCG-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D701-D507','G:1','ATTACTCG-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D701-D508','H:1','ATTACTCG-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D702-D501','A:2','TCCGGAGA-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D702-D502','B:2','TCCGGAGA-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D702-D503','C:2','TCCGGAGA-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D702-D504','D:2','TCCGGAGA-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D702-D505','E:2','TCCGGAGA-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D702-D506','F:2','TCCGGAGA-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D702-D507','G:2','TCCGGAGA-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D702-D508','H:2','TCCGGAGA-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D703-D501','A:3','CGCTCATT-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D703-D502','B:3','CGCTCATT-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D703-D503','C:3','CGCTCATT-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D703-D504','D:3','CGCTCATT-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D703-D505','E:3','CGCTCATT-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D703-D506','F:3','CGCTCATT-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D703-D507','G:3','CGCTCATT-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D703-D508','H:3','CGCTCATT-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D704-D501','A:4','GAGATTCC-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D704-D502','B:4','GAGATTCC-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D704-D503','C:4','GAGATTCC-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D704-D504','D:4','GAGATTCC-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D704-D505','E:4','GAGATTCC-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D704-D506','F:4','GAGATTCC-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D704-D507','G:4','GAGATTCC-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D704-D508','H:4','GAGATTCC-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D705-D501','A:5','ATTCAGAA-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D705-D502','B:5','ATTCAGAA-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D705-D503','C:5','ATTCAGAA-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D705-D504','D:5','ATTCAGAA-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D705-D505','E:5','ATTCAGAA-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D705-D506','F:5','ATTCAGAA-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D705-D507','G:5','ATTCAGAA-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D705-D508','H:5','ATTCAGAA-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D706-D501','A:6','GAATTCGT-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D706-D502','B:6','GAATTCGT-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D706-D503','C:6','GAATTCGT-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D706-D504','D:6','GAATTCGT-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D706-D505','E:6','GAATTCGT-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D706-D506','F:6','GAATTCGT-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D706-D507','G:6','GAATTCGT-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D706-D508','H:6','GAATTCGT-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D707-D501','A:7','CTGAAGCT-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D707-D502','B:7','CTGAAGCT-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D707-D503','C:7','CTGAAGCT-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D707-D504','D:7','CTGAAGCT-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D707-D505','E:7','CTGAAGCT-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D707-D506','F:7','CTGAAGCT-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D707-D507','G:7','CTGAAGCT-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D707-D508','H:7','CTGAAGCT-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D708-D501','A:8','TAATGCGC-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D708-D502','B:8','TAATGCGC-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D708-D503','C:8','TAATGCGC-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D708-D504','D:8','TAATGCGC-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D708-D505','E:8','TAATGCGC-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D708-D506','F:8','TAATGCGC-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D708-D507','G:8','TAATGCGC-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D708-D508','H:8','TAATGCGC-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D709-D501','A:9','CGGCTATG-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D709-D502','B:9','CGGCTATG-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D709-D503','C:9','CGGCTATG-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D709-D504','D:9','CGGCTATG-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D709-D505','E:9','CGGCTATG-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D709-D506','F:9','CGGCTATG-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D709-D507','G:9','CGGCTATG-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D709-D508','H:9','CGGCTATG-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D710-D501','A:10','TCCGCGAA-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D710-D502','B:10','TCCGCGAA-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D710-D503','C:10','TCCGCGAA-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D710-D504','D:10','TCCGCGAA-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D710-D505','E:10','TCCGCGAA-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D710-D506','F:10','TCCGCGAA-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D710-D507','G:10','TCCGCGAA-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D710-D508','H:10','TCCGCGAA-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D711-D501','A:11','TCTCGCGC-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D711-D502','B:11','TCTCGCGC-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D711-D503','C:11','TCTCGCGC-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D711-D504','D:11','TCTCGCGC-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D711-D505','E:11','TCTCGCGC-AGGCGAAG'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D711-D506','F:11','TCTCGCGC-TAATCTTA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D711-D507','G:11','TCTCGCGC-CAGGACGT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D711-D508','H:11','TCTCGCGC-GTACTGAC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D712-D501','A:12','AGCGATAG-TATAGCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D712-D502','B:12','AGCGATAG-ATAGAGGC'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D712-D503','C:12','AGCGATAG-CCTATCCT'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D712-D504','D:12','AGCGATAG-GGCTCTGA'],
            ['Illumina_TruSeq-HT_Dual-Index','Illumina_TruSeq-HT_Dual-Index','D712-D505','E:12','AGCGATAG-AGGCGAAG']
    ]
}