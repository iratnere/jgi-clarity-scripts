import static cucumber.api.groovy.EN.And
import static net.sf.json.JSONObject.fromObject

And(~'^the response is a list containing (.*) and (.*)$') { String baseType, String type -> //sequencing project id, sow items
    def respJson = response.contentAsString
    def resp = fromObject(respJson)

    def spId = resp[baseType.toLowerCase().replace(' ', '-')]
    assert spId
    def sowItems = resp[type.toLowerCase().replace(' ', '-')]
    assert sowItems?.size() > 0
}

And(~'^(.*) \\[id: (\\d+)\\] has the following attributes:$') { String propertyType, String sowId, json ->
    def respJson = response.contentAsString
    def resp = fromObject(respJson)
    resource = propertyType.toLowerCase().replace(' ', '-') //sow-item
    resourceIdName = resource.plus('-id') //sow-item-id
    def sowItems = resp[resource.plus('s')]

    def expectedItem = fromObject(json)
    def sowItem = sowItems.find{
        it[resourceIdName] as String == sowId
    }
    expectedItem.each{ key,value ->
        assert sowItem[key] == value
    }
}