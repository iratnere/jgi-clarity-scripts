package gov.doe.jgi.pi.pps.clarity.model.sample

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNodeInterface

/**
 * Created by dscott on 11/25/2014.
 */
class SampleFactory {

    private static ClaritySample generateSample(SampleNodeInterface sampleNode) {
        String sowItemId = sampleNode.getUdfAsString(ClarityUdf.SAMPLE_SOW_ITEM_ID.udf)
        boolean isCustomAliquot = SowItemTypeCv.CUSTOM_ALIQUOT.value.equalsIgnoreCase(sampleNode.getUdfAsString(ClarityUdf.SAMPLE_SOW_ITEM_TYPE.udf))
        if (sowItemId && isCustomAliquot) {
            return new CustomAliquotScheduledSample(sampleNode)
        }
        if (sowItemId && !isCustomAliquot) {
            return new ScheduledSample(sampleNode)
        }
        if (sampleNode.name.isLong()) {
            return new PmoSample(sampleNode)
        }
        return new ClaritySample(sampleNode)
    }

    static ClaritySample sampleInstance(SampleNodeInterface sampleNode) {
        if (!sampleNode) {
            return null
        }
        if (!sampleNode.cache) {
            sampleNode.cache = generateSample(sampleNode)
        }
        return  (ClaritySample) sampleNode.cache
    }

    static ClaritySample getSample(String sampleLimsId, NodeManager nodeManager) {
        SampleNode sampleNode = nodeManager.getSampleNode(sampleLimsId)
        return sampleInstance(sampleNode)
    }

    static Map<String, ClaritySample> getSamples(Collection<String> sampleLimsIds, NodeManager nodeManager) {
        return (Map<String, ClaritySample>) nodeManager.getSampleNodes(sampleLimsIds).inject([:]){ Map sampleMap, SampleNode sampleNode ->
            ClaritySample sample = sampleInstance(sampleNode)
            sampleMap[sampleNode.id] = sample
            sampleMap
        }
    }

}