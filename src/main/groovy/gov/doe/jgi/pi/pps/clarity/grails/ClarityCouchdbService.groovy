package gov.doe.jgi.pi.pps.clarity.grails

import gov.doe.jgi.pi.pps.couchdb.grails.CouchDbService
import gov.doe.jgi.pi.pps.couchdb.grails.CouchDbUtilityService
import gov.doe.jgi.pi.pps.couchdb.util.CouchDb
import gov.doe.jgi.pi.pps.util.exception.BundleAwareException
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ClarityCouchdbService {

    CouchDbUtilityService couchDbUtilityService
    CouchDbService couchDbService

    static enum CouchDatabase {
        WEB_TRANSACTION('clarity-web-transaction')

        final CouchDb couchDb
        final String name

        CouchDatabase(String name) {
            this.name = name
            this.couchDb = new CouchDb(name:name)
        }

        String toString() {
            return name
        }

    }


    //http://fillmore.jgi-psf.org:5984/clarity-web-transaction/_design/clarity-logs/_view/external-id?key=%22clarity-process.24-376721%22
    static enum DesignDoc {
        CLARITY_LOGS('clarity-logs', CouchDatabase.WEB_TRANSACTION),
        UDF_UPDATE('udf-update', CouchDatabase.WEB_TRANSACTION)

        final String name
        final CouchDatabase database

        private DesignDoc(String name, CouchDatabase database) {
            this.name = name
            this.database = database
        }

        String getDocumentPath() {
            return "${database.name}/${name}.json"
        }

        String toString() {
            return name
        }
    }

	static transactional = false

    static final Logger logger = LoggerFactory.getLogger(ClarityCouchdbService.class.name)

    JSONObject designDocJson(DesignDoc designDoc) {
        String urlString = "resources/couchdb/design_docs/${designDoc.documentPath}"
        logger.info "design DOC URL: ${urlString}"
        URL url =  Thread.currentThread().getContextClassLoader().getResource(urlString)
        if (!url) {
            throw new BundleAwareException([code:'url.invalid', args:[urlString]])
        }
        String viewText = url.text
        JSONObject viewObj = new JSONObject(viewText)
        viewObj.remove('_rev')
        viewObj.remove('_id')
        return viewObj
    }

    //http://magnemite.jgi-psf.org:5984/clarity-web-transaction/_design/udf-update/_view/udf-updates
    void addDesignDoc(DesignDoc designDoc) {
        logger.info "adding design doc \"${designDoc}\""
        JSONObject designJson = designDocJson(designDoc)
        String viewId = "_design/${designDoc}"
        String stuffId = "${designDoc.database}-design-${designDoc}"

        JSONObject cloneDesign = new JSONObject()
        cloneDesign.putAll(designJson)

        JSONObject existingView = couchDbService.get(designDoc.database.name, viewId)
        if (existingView) {
            logger.info "existing view:\n${existingView.toString(2)}"
            designJson['_rev'] = existingView['_rev']
            cloneDesign['view-version'] = existingView['_rev']
        }

        JSONObject lastDesign = couchDbUtilityService.getStuff(stuffId)
        couchDbUtilityService.putStuff(stuffId,cloneDesign)
        JSONObject thisDesign = couchDbUtilityService.getStuff(stuffId)

        logger.info "last design doc:\n${lastDesign.toString(2)}"
        logger.info "this design doc:\n${thisDesign.toString(2)}"

        /*
        if (JsonUtil.areEqual(lastDesign, thisDesign)) {
            logger.info "match for desgin doc ${designDoc}: no update"
            return //no update necessary
        }
        logger.info "no match for desgin doc ${designDoc}: insert or update"
        */

        //JSONObject insertOrForceUpdate(String couchDbName, JSONObject json, String id, Integer maxAttempts = 4) {
        couchDbService.insertOrForceUpdate(designDoc.database.name, designJson, viewId)
    }

    void addDesignDocs() {
        DesignDoc.values().each { DesignDoc designDoc ->
            addDesignDoc(designDoc)
        }
    }

    void initializeDatabases() {
        CouchDatabase.values().each { CouchDatabase couchDatabase ->
            couchDbUtilityService.initializeDatabase(couchDatabase.couchDb)
        }
        addDesignDocs()
    }


}
