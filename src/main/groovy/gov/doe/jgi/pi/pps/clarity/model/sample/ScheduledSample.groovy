package gov.doe.jgi.pi.pps.clarity.model.sample

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.domain.*
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNodeInterface
import gov.doe.jgi.pi.pps.util.exception.BundleAwareException
import gov.doe.jgi.pi.pps.util.exception.WebException

/**
 * Created by duncanscott on 4/7/15.
 */
class ScheduledSample extends ClaritySample {

    static final String[] getActiveSowQCFailureModes() {
        return SowQcFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }

    ScheduledSample(SampleNodeInterface sampleNode) {
        super(sampleNode)
    }

    String getPmoSampleLimsId() {
        sampleNode.getUdfAsString(ClarityUdf.SAMPLE_LIMSID.udf)
    }

    void setPmoSampleLimsId(String pmoSampleLimsId) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_LIMSID.udf,pmoSampleLimsId)
    }

    Long getPmoSampleId() {
        pmoSample?.pmoSampleId
    }

    PmoSample getPmoSample() {
        String pmoSampleId = pmoSampleLimsId
        if (!pmoSampleId ) {
            throw new BundleAwareException([code:'scheduledSample.noSampleLimsId', args:[sampleNode.id]])
        }
        SampleNode pmoSampleNode = sampleNode.nodeManager.getSampleNode(pmoSampleLimsId)
        ClaritySample pmoSample = SampleFactory.sampleInstance(pmoSampleNode)
        if (pmoSample instanceof PmoSample) {
            return (PmoSample) pmoSample
        }
        if (!pmoSampleNode) {
            throw new BundleAwareException([code:'sample.notFoundForLimsId', args:[pmoSampleId]])
        }
        throw new BundleAwareException([code:'sample.pmoSampleExpected', args:[pmoSample]])
    }

    Long getSowItemId() {
        sampleNode.getUdfAsBigInteger(ClarityUdf.SAMPLE_SOW_ITEM_ID.udf)?.toLong()
    }

    void setSowItemId(def sowItemId) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_SOW_ITEM_ID.udf, sowItemId)
        sampleAnalyte.setReagentLabel(sowItemId)
    }

    String toString() {
        "${this.class.simpleName}[limsid:${id},sowItemId:${sowItemId}]"
    }

    Long getSowItemNumber() {
        sampleNode.getUdfAsBigInteger(ClarityUdf.SAMPLE_SOW_ITEM_NUMBER.udf)?.toInteger()
    }

    List<LibraryCreationQueueCv> getLibraryCreationQueuesFromSpecsId() {
        List<LibraryCreationQueueCv> queues = libraryCreationSpecs?.libraryCreationQueues?.findAll{it.active == 'Y'} as List
        if (!queues || queues.isEmpty())
            throw new WebException([code:'ScheduledSample.libraryCreationQueues.notFound', args:[udfLibraryCreationSpecsId]], 422)
        queues
    }

    @Override
    LibraryCreationQueueCv getLibraryCreationQueue() {
        List<LibraryCreationQueueCv> queues = libraryCreationQueuesFromSpecsId
        if (queues && queues.size() == 1) {
            return queues[0]
        }
        boolean isPlate = pmoSample.sampleNode.artifactNode.containerNode.isNinetySixWellPlate
        if (isPlate)
            return queues.find{it.plateTargetMassLibTrialNg && it.plateTargetVolumeLibTrialUl}
        return queues.find{it.tubeTargetMassLibTrialNg && it.tubeTargetVolumeLibTrialUl}
    }

    LibraryCreationSpecsCv getLibraryCreationSpecs() {
        LibraryCreationSpecsCv libraryCreationSpecs = LibraryCreationSpecsCv.findById(udfLibraryCreationSpecsId as Long)
        if (!libraryCreationSpecs)
            throw new WebException([code: 'ScheduledSample.libraryCreationSpecs.notFound', args: [udfLibraryCreationSpecsId]], 422)
        libraryCreationSpecs
    }

    BigInteger getUdfLibraryCreationSpecsId() {
        return sampleNode.getUdfAsBigInteger(ClarityUdf.SAMPLE_LIBRARY_CREATION_SPECS_ID.udf)
    }

    void setUdfLibraryCreationSpecsId(BigInteger value) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_LIBRARY_CREATION_SPECS_ID.udf, value)
    }

    BigInteger getUdfLibraryCreationQueueId() {
        return libraryCreationQueue?.id
    }

    String getUdfLibraryIndexSet() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_LIBRARY_INDEX_SET.udf)
    }

    void setUdfLibraryIndexSet(String libraryIndexSet) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_LIBRARY_INDEX_SET.udf, libraryIndexSet)
    }

    String getUdfScientificProgram() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_SCIENTIFIC_PROGRAM.udf)
    }

    void setUdfScientificProgram(String scientificProgram) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_SCIENTIFIC_PROGRAM.udf, scientificProgram)
    }

    BigDecimal getUdfTargetTemplateSizeBp() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_TARGET_TEMPLATE_SIZE_BP.udf)
    }

    void setUdfTargetTemplateSizeBp(BigDecimal targetTemplateSizeBp) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_TARGET_TEMPLATE_SIZE_BP.udf, targetTemplateSizeBp)
    }

    BigDecimal getUdfTargetAliquotMassNg() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_TARGET_ALIQUOT_MASS_NG.udf)
    }

    void setUdfTargetAliquotMassNg(BigDecimal targetAliquotMassNg) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_TARGET_ALIQUOT_MASS_NG.udf, targetAliquotMassNg)
    }

    BigDecimal getUdfTargetInsertSizeKb() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_TARGET_INSERT_SIZE_KB.udf)
    }

    BigDecimal setUdfTargetInsertSizeKb(BigDecimal targetInsertSizeKb){
        sampleNode.setUdf(ClarityUdf.SAMPLE_TARGET_INSERT_SIZE_KB.udf, targetInsertSizeKb)
    }

    String getUdfExomeCaptureProbeSet() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_EXOME_CAPTURE_PROBE_SET.udf)
    }

    void setUdfExomeCaptureProbeSet(String exomeCaptureProbeSet) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_EXOME_CAPTURE_PROBE_SET.udf, exomeCaptureProbeSet)
    }

    String getUdfItagPrimerSet() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_ITAG_PRIMER_SET.udf)
    }

    void setUdfItagPrimerSet(String itagPrimerSet) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_ITAG_PRIMER_SET.udf, itagPrimerSet)
    }

    BigDecimal getUdfDegreeOfPooling() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_DEGREE_OF_POOLING.udf)
    }

    void setUdfDegreeOfPooling(BigDecimal degreeOfPooling) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_DEGREE_OF_POOLING.udf, degreeOfPooling)
    }

    String getUdfRunMode() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_RUN_MODE.udf)
    }

    void setUdfRunMode(String runMode) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_RUN_MODE.udf, runMode)
    }

    String getUdfLogicalAmountUnits() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_LOGICAL_AMOUNT_UNITS.udf)
    }

    void setUdfLogicalAmountUnits(String logicalAmountUnits) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_LOGICAL_AMOUNT_UNITS.udf, logicalAmountUnits)
    }

    String getUdfSowItemType() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_SOW_ITEM_TYPE.udf)
    }

    void setUdfSowItemType(String sowItemType) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_SOW_ITEM_TYPE.udf, sowItemType)
    }

    BigDecimal getUdfTargetLogicalAmount() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_TARGET_LOGICAL_AMOUNT.udf)
    }

    void setUdfTargetLogicalAmount(BigDecimal targetLogicalAmount) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_TARGET_LOGICAL_AMOUNT.udf, targetLogicalAmount)
    }

    BigDecimal getUdfLcAttempt() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_LC_ATTEMPT.udf)
    }

    void setUdfLcAttempt(BigDecimal lcAttempt) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_LC_ATTEMPT.udf, lcAttempt)
    }

    String getUdfDestContainerName() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_DEST_CONTAINER_NAME.udf)
    }

    String getUdfDestContainerLocation() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_DEST_CONTAINER_LOCATION.udf)
    }

    def incrementUdfLcAttempt() {
        BigDecimal lca = udfLcAttempt
        if (!lca) {
            throw new WebException([code:'ClaritySample.udfLcAttempt.notSet', args:[this.id, lca]], 422)
        }
        setUdfLcAttempt(calculateAttempt(lca))
    }

    def incrementUdfLcFailedAttempt() {
        setUdfLcFailedAttempt(calculateAttempt(udfLcFailedAttempt))
    }

    static def calculateAttempt(def udfValue) {
        return udfValue ? (udfValue + 1) : 1
    }

    BigDecimal getUdfSowItemQcTypeId() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_SOW_ITEM_QC_TYPE_ID.udf)
    }

    void setUdfSowItemQcTypeId(BigDecimal sowItemQcTypeId) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_SOW_ITEM_QC_TYPE_ID.udf, sowItemQcTypeId)
    }

    BigDecimal getUdfAccountId() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_ACCOUNT_ID.udf)
    }

    void setUdfAccountId(BigDecimal accountId) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_ACCOUNT_ID.udf, accountId)
    }

    String getUdfSowItemQcResult() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_SOW_QC_RESULT.udf)
    }

    void setUdfSowItemQcResult(String sowItemResult) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_SOW_QC_RESULT.udf, sowItemResult)
    }

    String getUdfSowItemQcFailureMode() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_SOW_QC_FAILURE_MODE.udf)
    }

    void setUdfSowItemQcFailureMode(String sowItemFailureMode) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_SOW_QC_FAILURE_MODE.udf, sowItemFailureMode)
    }

    BigDecimal getUdfLcFailedAttempt() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_LC_FAILED_ATTEMPT.udf)
    }

    void setUdfLcFailedAttempt(BigDecimal lcFailedAttempt) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_LC_FAILED_ATTEMPT.udf, lcFailedAttempt)
    }

    String toPEUInputData() {
        """
            sample id: $id
            sow type: $udfSowItemType
            DOP: $udfDegreeOfPooling
            run mode: $udfRunMode
            logicalAmountUnits: $udfLogicalAmountUnits
        """
    }

    BigInteger getReadLengthBp() {
        return runModeCv?.readLengthBp
    }

    BigInteger getReadTotal() {
        return runModeCv?.readTotal
    }

    String getSequencer() {
        return sequencerModelCv?.sequencerModel
    }

    String getPlatform() {
        return sequencerModelCv?.platform
    }

    @Override
    boolean getIsItag() {
        return udfItagPrimerSet as Boolean
    }

    @Override
    boolean getIsExomeCapture() {
        return udfExomeCaptureProbeSet as Boolean
    }

    @Override
    boolean getIsIllumina() {
        return sequencerModelCv?.isIllumina
    }

    @Override
    boolean getIsSmallRna() {
        return libraryCreationSpecs?.libraryCreationSpecs?.contains(Analyte.SMALL_RNA_QUEUE)
    }

    @Override
    boolean getIsInternalSingleCell() {
        return (SowItemTypeCv.SINGLE_CELL_INTERNAL.value == udfSowItemType)
    }

    @Override
    boolean getIsDapSeq() {
        return (SowItemTypeCv.DAP_SEQ.value == udfSowItemType)
    }

    BigInteger getUdfPoolNumber() {
        return sampleNode.getUdfAsBigInteger(ClarityUdf.SAMPLE_POOL_NUMBER.udf)
    }

    BigInteger getUdfBatchId() {
        return sampleNode.getUdfAsBigInteger(ClarityUdf.SAMPLE_BATCH_ID.udf)
    }

    RunModeCv getRunModeCv() {
        String runModeName = udfRunMode
        ClarityWebTransaction.logger.info "Scheduled sample<$id> run mode udf: $runModeName"
        if (runModeName) {
            RunModeCv runModeCv = RunModeCv.findByRunMode(runModeName)
            if (!runModeCv?.id) {
                throw new WebException("${this}: run mode not found for the value [${runModeName}]", 422)
            }
            return runModeCv
        }
        return null
    }

    SequencerModelCv getSequencerModelCv() {
        return runModeCv?.sequencerModel
    }

    boolean getIsPacBio() {
        sequencerModelCv?.isPacBio
    }

    boolean getIsPacBioSequel() {
        sequencerModelCv?.isSequel
    }

    boolean getIsSIPOriginalScheduledSample() {
        return udfSowItemType == SowItemTypeCv.SIP_SOURCE_SAMPLE.value
    }

    boolean getIsControlScheduledSample() {
        return this.pmoSample.udfIsotopeLabel == 'Unlabeled'
    }

    String getSIPGroupName() {
        return this.pmoSample.udfGroupName
    }

    String getPmoSampleArtifactLimsId() {
        pmoSample.sampleAnalyte.id
    }

    void passOnGroupFailure() {
        if(udfSowItemQcResult == Analyte.PASS) {
            udfSowItemQcResult = Analyte.FAIL
            udfSowItemQcFailureMode = GROUP_FAILURE
        }
    }

    boolean getIsControlSample() {
        return pmoSample.isControlSample
    }

    boolean getPassedQc() {
        return udfSowItemQcResult == Analyte.PASS
    }

    String getLcWorkflowName() {
        return ClaritySampleAliquot.LIBRARY_CREATION_WORKFLOW_PREFIX + libraryCreationQueue?.libraryCreationQueue
    }

}