package gov.doe.jgi.pi.pps.couchdb.util

/**
 * Created by duncanscott on 4/17/15.
 */
interface CouchDbEntityBeforeAfter extends CouchDbEntity {

    void beforeInsert()
    void afterInsert()

    void beforeUpdate()
    void afterUpdate()

    void beforeDelete()
    void afterDelete()

    void beforeGet()
    void afterGet()

}