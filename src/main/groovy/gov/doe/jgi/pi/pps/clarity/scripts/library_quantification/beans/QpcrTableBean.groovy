package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans

import gov.doe.jgi.pi.pps.clarity.domain.LibraryQpcrFailureModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping

/**
 * Created by datjandra on 6/26/2015.
 */
class QpcrTableBean {

    QpcrTableBean() {}

    QpcrTableBean(String[] passFailValues, String[] failureModeValues) {
        passFail = new DropDownList()
        passFail.setControlledVocabulary(passFailValues)

        failureMode = new DropDownList()
        failureMode.setControlledVocabulary(failureModeValues)
    }

    @FieldMapping(header = 'Name', cellType = CellTypeEnum.STRING)
    public String name

    @FieldMapping(header = 'Cp', cellType = CellTypeEnum.NUMBER)
    public BigDecimal cp

    @FieldMapping(header = 'Concentration', cellType = CellTypeEnum.NUMBER)
    public BigDecimal concentration

    @FieldMapping(header = 'Standard', cellType = CellTypeEnum.NUMBER)
    public BigDecimal standard

    @FieldMapping(header = 'Library', cellType = CellTypeEnum.STRING)
    public String library

    @FieldMapping(header = 'Library Pass/Fail', cellType= CellTypeEnum.DROPDOWN)
    public DropDownList passFail

    @FieldMapping(header = 'Failure modes', cellType= CellTypeEnum.DROPDOWN)
    public DropDownList failureMode

    @FieldMapping(header = 'DOP', cellType = CellTypeEnum.FORMULA)
    public BigDecimal dop

    @FieldMapping(header = 'Well', cellType = CellTypeEnum.STRING)
    public String well

    @FieldMapping(header = 'Ave pM', cellType =  CellTypeEnum.FORMULA)
    public BigDecimal libraryMolarityPm

    @FieldMapping(header = 'Volume used uL', cellType =  CellTypeEnum.NUMBER)
    public BigDecimal volumeUsed

    @FieldMapping(header = 'Notes', cellType =  CellTypeEnum.STRING)
    public String notes

    String getLibraryName() {
        return library.split(' ')[0]
    }

    String getWellLocation() {
        return library.split('\\s+').last().split('\\.')[0]
    }

    //Analyte inputAnalyte

    void populateRequiredFields(int passCount){
        passFail = new DropDownList()
        failureMode = new DropDownList(controlledVocabulary: LibraryQpcrFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode })
        if(passCount > 0)
            passFail.value = AttributeValues.PASS
        else{
            passFail.value = AttributeValues.FAIL
            failureMode.value = failureMode.controlledVocabulary[0]
        }
        volumeUsed = 2
    }
}
