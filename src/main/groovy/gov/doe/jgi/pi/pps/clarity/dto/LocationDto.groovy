package gov.doe.jgi.pi.pps.clarity.dto

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation

/**
 * Created by dscott on 4/13/2016.
 */
class LocationDto implements Comparable<LocationDto> {

    ContainerDto container
    ArtifactDto artifact

    String containerId
    String row
    String column

    String getRowColumn() {
        "${row}:${column}"
    }

    String toString() {
        idString(containerId,row,column)
    }

    String getKey() {
        idString(containerId,row,column)
    }

    static String containerLocationKey(ContainerLocation containerLocation) {
        if (containerLocation) {
            return idString(containerLocation.containerId,containerLocation.row,containerLocation.column)
        }
        return ''
    }

    private static String idString(String containerId, String row, String column) {
        "${containerId}:${row}:${column}".toString()
    }

    @Override
    int compareTo(LocationDto other) {
        int result = this.containerId.compareTo(other.containerId)
        if (result) {return result}
        if (this.row.isInteger() && other.row.isInteger()) {
            result = this.row.toInteger().compareTo(other.row.toInteger())
        } else {
            result = this.row.compareTo(other.row)
        }
        if (result) {return result}
        if (this.column.isInteger() && other.column.isInteger()) {
            result = this.column.toInteger().compareTo(other.column.toInteger())
        } else {
            result = this.column.compareTo(other.column)
        }
        result
    }
}
