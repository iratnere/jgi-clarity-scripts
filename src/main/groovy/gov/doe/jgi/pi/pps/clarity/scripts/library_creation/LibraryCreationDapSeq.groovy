package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import doe.jgi.pi.pps.dapseq.parser.DapSeqJsonSubmission
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.grails.ArtifactNodeService
import gov.doe.jgi.pi.pps.clarity.grails.PostProcessService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.LibraryNameReservationService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UserDefinedField
import gov.doe.jgi.pi.pps.dapseq.DapSeqJsonSubmissionService
import gov.doe.jgi.pi.pps.dapseq.DapSeqJsonValidationService
import groovy.json.JsonBuilder
import org.apache.commons.lang.RandomStringUtils

/**
 * Created by tlpaley on 10/17/19
 * Clarity DAP Seq LC Requirements
 * https://docs.google.com/document/d/1zc7V6mqTOIqsglkILLEKsc68uV604mmOso1IYFE3kuw/edit
 * input sample aliquots:
 * Illumina Tubes Dap-Seq (sample udfSowItemType == SowItemTypeCv.DAP_SEQ_SAMPLE.value)
 */
class LibraryCreationDapSeq extends LibraryCreation {

    private final static int DEFAULT_POOL_NUMBER = 1
    private final static String JSON_PLACEHOLDER = 'DAP-Seq.json'

    List<ClarityLibraryPool> clarityLibraryPools //currently, one DAP-Seq pool only

    LibraryCreationDapSeq(LibraryCreationProcess process) {
        this.process = process
        this.output = new OutputTube(process)
    }

    static LibraryNameReservationService getLibraryNameReservationService() {
        return ClarityWebTransaction.currentTransaction
                .requireApplicationBean(LibraryNameReservationService.class) as LibraryNameReservationService
    }

    static ArtifactNodeService getArtifactNodeService() {
        return ClarityWebTransaction.currentTransaction
                .requireApplicationBean(ArtifactNodeService.class) as ArtifactNodeService
    }

    @Override
    String getLcTableBeanClassName() {
        null
    }

    @Override
    void updateInputsAndSamples() {
        process.inputAnalytes.each { it.udfVolumeUl = BigDecimal.ZERO }
    }

    String processJson() {
        ArtifactNode fileNode = process.getFileNode(JSON_PLACEHOLDER)
        downloadJson(fileNode.id)
    }

    def transferJsonDataToClarityLibraryPool(DapSeqJsonSubmission submission, ClarityLibraryPool dapSeqPool = clarityLibraryPools[0]) {
        dapSeqPool.udfConcentrationNgUl = submission.dapPool.concentrationNgUl
        dapSeqPool.udfActualTemplateSizeBp = submission.dapPool.avgTemplateSizeBp.toBigDecimal()
        dapSeqPool.udfVolumeUl = submission.dapPool.volumeUl
        dapSeqPool.udfLibraryMolarityQcPm = submission.dapPool.molarityPm

        dapSeqPool.udfLibraryQcResult = Analyte.PASS
        dapSeqPool.systemQcFlag = Boolean.TRUE
        dapSeqPool.containerUdfLibraryQcResult = Analyte.PASS
    }

    List<ClarityLibraryPool> createClarityLibraryPools() {
        ClarityWebTransaction.logger.info "Create DAP-Seq library pool"
        ProcessNode poolCreationProcessNode = createPoolCreationProcessNode()
        return poolCreationProcessNode.outputAnalytes.collect{(ClarityLibraryPool) AnalyteFactory.analyteInstance(it) }
    }

    ProcessNode createPoolCreationProcessNode() {
        ProcessNode poolCreationProcessNode = createAndConfigureOutputPools()
        if (!poolCreationProcessNode) {
            process.postErrorMessage("Cannot create DAP-Seq pool: $poolCreationProcessNode.")
        }
        return poolCreationProcessNode
    }

    def createAndConfigureOutputPools() {
        PostProcessService taskGenerationService = ClarityWebTransaction.requireCurrentTransaction()
                .requireApplicationBean(PostProcessService) as PostProcessService
        Stage stage = Stage.AUTOMATIC_POOL_CREATION
        StepConfigurationNode stepConfigurationNode = StageUtility.getStepConfigurationNode(process.nodeManager, stage)
        ProcessParams processParams = new ProcessParams(stepConfigurationNode, poolNumberToPoolMemberArtifactNodes)
        processParams.processUdfMap = getProcessUdfsToDefaultValueMap(stepConfigurationNode)
        taskGenerationService.postPoolingProcess(process.nodeManager, stage, processParams)
    }

    Map<Integer,Collection<ArtifactNode>> getPoolNumberToPoolMemberArtifactNodes(){
        Map<Integer,Collection<ArtifactNode>> poolNumberPoolMembersMap = [:]
        poolNumberPoolMembersMap[DEFAULT_POOL_NUMBER] = process.outputAnalytes.collect{it.artifactNode as ArtifactNode}
        return poolNumberPoolMembersMap
    }

    static Map<UserDefinedField, Object> getProcessUdfsToDefaultValueMap(StepConfigurationNode stepConfigurationNode) {
        Map<UserDefinedField, Object> processUdfsMap = [:]
        StageUtility.getProcessUdfs(stepConfigurationNode)?.each { UserDefinedField udf ->
            processUdfsMap[udf] = ClarityUdf.DEFAULT_UDF_VALUE
        }
        processUdfsMap
    }

    String downloadJson(String artifactId) {
        InputStream inputStream = process.nodeManager.downloadFile(artifactId)
        return "${inputStream}"
    }

    @Override
    ExcelWorkbook populateLibraryCreationSheet(){
        return null
    }

    static DapSeqJsonSubmission validateSubmittedJson(Object json, List<Analyte> inputAnalytes) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        DapSeqJsonValidationService validationService = (DapSeqJsonValidationService) webTransaction.requireApplicationBean(DapSeqJsonValidationService)
        Set<ScheduledSample> scheduledSamples = []
        inputAnalytes.each { Analyte analyte ->
            Set<ScheduledSample> analyteScheduledSamples = analyte.parentScheduledSamples
            if (analyteScheduledSamples) {
                scheduledSamples.addAll(analyteScheduledSamples)
            }
        }
        validationService.validateSubmittedJson(json, scheduledSamples)
    }

    static String saveSubmittedJson(DapSeqJsonSubmission submission, ClarityLibraryPool dapSeqPool, Long submittedByContactId) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        DapSeqJsonSubmissionService submissionService = (DapSeqJsonSubmissionService) webTransaction.requireApplicationBean(DapSeqJsonSubmissionService)
        submissionService.submitJsonForDapSeqLibraryPool(submission, dapSeqPool, submittedByContactId)
    }

    @Override
    void updateLibraryStockUdfs() {
        String json = processJson()
        //validate json (uploaded JSON + collection of output analyte ids (libraries))
        DapSeqJsonSubmission submission = validateSubmittedJson(json, process.inputAnalytes)

        clarityLibraryPools = createClarityLibraryPools() // currently, only one pool will be created
        libraryNameReservationService.assignLibraryNames(clarityLibraryPools.sort{ it.id })

        //save submission to database and associate with clarity LIMSID of pool
        String documentUrl = saveSubmittedJson(submission, clarityLibraryPools[0], process.researcher.contactId)
        if (!documentUrl) {
            ClarityProcess.postErrorMessage("Cannot save submitted JSON file.")
        }
        //update library stocks
        process.updateNonWorksheetUdfs() //library stocks
        transferJsonDataToClarityLibraryStocks(submission)
        //output.updateLibraryIndexUdf(process.outputAnalytes as List<ClarityLibraryStock>)

        //update library pool
        transferJsonDataToClarityLibraryPool(submission)
        clarityLibraryPools[0].artifactNode.readOnly = false
        clarityLibraryPools[0].containerNode.readOnly = false
    }

    def transferJsonDataToClarityLibraryStocks(DapSeqJsonSubmission submission) {
        process.outputAnalytes.each {
            ClarityLibraryStock clarityLibraryStock = it as ClarityLibraryStock
            //clarityLibraryStock.udfConcentrationNgUl = lcPlateTableBean.libraryConcentration
            //clarityLibraryStock.udfActualTemplateSizeBp = lcPlateTableBean.libraryTemplateSize
            //clarityLibraryStock.udfVolumeUl = lcPlateTableBean.libraryVolume
            //clarityLibraryStock.udfLibraryMolarityQcPm = lcPlateTableBean.libraryMolarity
            //clarityLibraryStock.udfIndexContainerBarcode = indexSet
            //clarityLibraryStock.udfIndexName = lcPlateTableBean.libraryIndexName
            clarityLibraryStock.udfNumberPcrCycles = submission.numberPcrCyclesDapAmplification?.toBigDecimal()

            clarityLibraryStock.udfLibraryQcResult = Analyte.PASS
            clarityLibraryStock.systemQcFlag = Boolean.TRUE
        }
    }

    @Override
    Section getTableSectionLibraries() {
        return null
    }

    static List<ClarityLibraryPool> findCreatedPool(Analyte libraryStock){ //only one valid pool created
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        List<String> descendantLimsIds = artifactNodeService.getDescendantList(libraryStock.id)
        List<ClarityLibraryPool> clarityPools = []
        if (descendantLimsIds) {
            nodeManager.getArtifactNodes(descendantLimsIds).each {
                Analyte clarityPool = AnalyteFactory.analyteInstance(it as ArtifactNode)
                if (clarityPool && clarityPool instanceof ClarityLibraryPool && clarityPool.systemQcFlag)
                    clarityPools << clarityPool
            }
        }
        if (!clarityPools)
            ClarityProcess.postErrorMessage("""
$libraryStock
Cannot find valid DAP-Seq pool.
DescendantLimsIds: $descendantLimsIds
            """)
        if (clarityPools.size() > 1)
            ClarityProcess.postErrorMessage("""
$libraryStock
Found multiple valid DAP-Seq submissions.
Please abort the process.
            """)
        return clarityPools as List<ClarityLibraryPool>
    }

    @Override
    void  updateLibraryPoolUdfs() {
        //transferJsonDataToClarityLibraryPool method called on Record Details
        clarityLibraryPools = findCreatedPool(process.outputAnalytes[0])

        clarityLibraryPools[0].updateLibraryCreationUdfs(process.researcher?.fullName)
        clarityLibraryPools[0].artifactNode.readOnly = false
        clarityLibraryPools[0].containerNode.readOnly = false
    }

    @Override
    void moveToNextWorkflow() {
        List<ClarityLibraryPool> passedPools = clarityLibraryPools
        process.routeAnalytes(defaultStage, passedPools)
        //process.postInfoMessage(buildInfoMessage() as String)
    }

    @Override
    void createPoolPrintLabelsFile() {
        process.writeLabels(clarityLibraryPools)
    }

    @Override
    void validateBatch() {
        validateRunModes(process.inputAnalytes)
        validateParentProcesses(process.inputAnalytes)
    }

    /*
Because we are running a DAP-Seq pooling process without a script
we are checking for constraints that are mandatory like : same run mode.
 */
    static void validateRunModes(List<Analyte> inputAnalytes) {
        def runModes = inputAnalytes.collect{ (it.claritySample as ScheduledSample).udfRunMode }?.unique()
        if (!runModes.find() || runModes?.size() != 1) {
            ClarityProcess.postErrorMessage("""
"Cannot create a DAP-Seq Pool. 
Libraries in this pool cannot be pooled.
Invalid parent samples run modes: $runModes.
All parent scheduled samples should have the same run mode.
            """)
        }
    }

    /*
    None of the input parent scheduled samples have been through Aliquot Creation process more than once
     */
    static void validateParentProcesses(List<Analyte> inputAnalytes) {
        inputAnalytes.each{
            List<String> processIds = ArtifactNodeService.getProcessIdsForInputArtifactId(it.parentAnalyte.id, ProcessType.AC_ALIQUOT_CREATION)
            if (processIds?.size() != 1) {
                ClarityProcess.postErrorMessage("""
Input analyte: $it
Invalid parent '$ProcessType.AC_ALIQUOT_CREATION.value' processes: $processIds. 
Expected only one parent '$ProcessType.AC_ALIQUOT_CREATION.value' process: ${it.artifactNode.parentProcessId}
                """)
            }
        }
    }

    @Override
    void createPrintLabelsFile() {} // do not print libraries labels

    String getTemplate(){
        return null
    }

    //for testing only: will not be used on production
    @Override
    void uploadExtraFiles() {
        uploadTestJson(process)
    }

    //for testing only: will not be used on production
    static void uploadTestJson(LibraryCreationProcess process) {
        ArtifactIndexService artifactIndexService = ClarityWebTransaction.requireCurrentTransaction()
                .requireApplicationBean(ArtifactIndexService) as ArtifactIndexService
        List<PmoSample> pmoSamples = process.inputAnalytes.collect { it.claritySample.pmoSample }
        List<String> indexes = getIndexes(artifactIndexService)
        File file = createTestJson(pmoSamples, indexes)
        ArtifactNode fileNode = process.getFileNode('Extra2')
        process.nodeManager.uploadFile(fileNode.id, file)
    }

    //for testing only: will not be used on production
    static List<String> getIndexes(ArtifactIndexService artifactIndexService) {
        List<Index> indexSets = artifactIndexService.getIndexSetsByDataStoreId(templateIndex)
        Map<String, List<Index>> indexes = artifactIndexService.getIndexes(indexSets.collect{it.indexSet})
        return indexes.values().flatten().collect{(it as Index).indexName}
    }

    /**
     * Creates a test json file for a DAP-Seq Library Creation process.
     * In json creates two primer plates with 96 wells.
     * For testing only: will not be used on production.
     *
     * @param pmoSamples the input PMO samples.
     * @param indexes the primer plate wells index names. Should be unique per json submission.
     * If 40 input organisms then 7680 unique indexes should be provided  (7680 = 40*96*2)
     * @return a json file
     */
    static File createTestJson(List<PmoSample> pmoSamples, List<String> indexes) {
        def organisms = []
        def proteinPlates = []
        pmoSamples.each { PmoSample pmoSample ->
            SequencingProject sp = pmoSample.sequencingProject
            def spRow = [
                    "sample-id": pmoSample.pmoSampleId,
                    "sample-name": "${pmoSample.pmoSampleId}_${RandomStringUtils.random(6, true, false)}",
                    "sequencing-project-id": sp.sequencingProjectId,
                    "sequencing-project-name": sp.udfSequencingProjectName,
                    "genus": "Arabidopsis",
                    "species": "thaliana",
                    "strain": "Col0",
                    "reference": "NCBI TAIR10.1",
                    "lot-number": "4/1/19",
                    "amplification-cycles": 7
            ]
            organisms << spRow

            if (proteinPlates.size() < 2) { //add 2 primer plates
                def wells = buildWells(pmoSamples, indexes)
                String randomString = RandomStringUtils.random(9, true, true)
                def plateRow = [
                        "plate-barcode": randomString,
                        "barcode-dap-lib-ref": "434245_NCBI TAIR10.1",
                        "wells": wells
                ]
                proteinPlates << plateRow
            }
        }
        def builder = new JsonBuilder(
                "submitted-by-cid": 2633,
                "TF-DNA-source": [
                        "genus": "Arabidopsis",
                        "species": "lyrata",
                        "strain": "NA",
                        "source": "Clone/cDNA/gDNA",
                        "reference": "NCBI TAIR10.1",
                        "lot-number": "abcd-12-aabbcc"
                ],
                "dap-experiment-date": "2019-12-04",
                "number-pcr-cycles-dap-amplification": 1,
                "dap-pool": [
                        "molarity-pm": 3.53,
                        "avg-template-size-bp": 400,
                        "concentration-ng-ul": 5.5,
                        "volume-ul": 2.21
                ],
                "organisms": organisms,
                "protein-plates": proteinPlates
        )
        File file = new File('TestDAPSeq.json')
        file.write(builder.toPrettyString())
        file
    }

    //for testing only: will not be used on production
    static def getPlateWells() {
        def plateWells = []
        (1..12).each { column ->
            ('A'..'H').each { row ->
                def well = column < 10 ? "${row}0$column" : "${row}$column"
                plateWells << well
            }
        }
        plateWells
    }

    //for testing only: will not be used on production
    static def buildWells(List<PmoSample> pmoSamples, List<String> indexes) {
        def wells = []
        plateWells.each {
            def organismIndexes = []
            pmoSamples.each { PmoSample pmoSample ->
                String randomString = RandomStringUtils.random(9, true, true)
                String indexName = indexes.pop()
                def info = [
                        "sample-id": pmoSample.pmoSampleId,
                        "index-name": indexName,
                        "demultiplexed-library-name": "library_name_${pmoSample.pmoSampleId}_${randomString}"
                ]
                organismIndexes << info
            }

            def wellsRow = [
                    "well-location": it,
                    "protein-id": "At1g010$it",
                    "source": "NCBI",
                    "nickname": "bZIP53$it",
                    "group-name": "bZIP",
                    "dna-sequence": "ATGCAAATAACGAGTATGCCACGACCGAAGCCAGGCATTACGACGATTTAGCAGATAGACCG",
                    "synbio-construct-name": "At1g010${it}_bZIP_bZIP53${it}_Arabidopsis_lyrata_NA_a",
                    "organism-indexes": organismIndexes
            ]
            wells << wellsRow
        }
        wells
    }

    static int getTemplateIndex() {
        return 512
    }
}