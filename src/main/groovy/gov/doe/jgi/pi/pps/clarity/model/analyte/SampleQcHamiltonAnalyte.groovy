package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.util.LabelBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import groovy.transform.PackageScope

/**
 * Created by dscott on 2/27/2015.
 */
class SampleQcHamiltonAnalyte extends Analyte {

    @PackageScope
    SampleQcHamiltonAnalyte(ArtifactNode artifactNode) {
        super(artifactNode)
    }

    @Override
    String getPrintLabelText() {
        if (isOnPlate) {
            List<ScheduledSample> scheduledSamples = claritySample.scheduledSamples
            def targetTemplateSize = scheduledSamples?scheduledSamples[0].udfTargetTemplateSizeBp:0
            return new LabelBean(
                    pos_1_1: containerId,
                    pos_1_2: containerUdfLabel,
                    pos_1_4: "${targetTemplateSize != null ? targetTemplateSize : ''} ${claritySample.sequencingProject.udfMaterialCategory}",
                    pos_1_5: containerId
            ) as String
        }
        return null
    }

    void setUdfMetabolomicsSampleId(String metabolomicsSampleId){
        artifactNode.setUdf(ClarityUdf.SAMPLE_METABOLOMICS_SAMPLE_ID.udf, metabolomicsSampleId)
    }

    String getUdfMetabolomicsSampleId(){
        return artifactNode.getUdfAsString(ClarityUdf.SAMPLE_METABOLOMICS_SAMPLE_ID.udf)
    }

    BigDecimal getUdfIsotopeEnrichment(){
        return artifactNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_ISOTOPE_ENRICHMENT.udf)
    }

    void setUdfIsotopeEnrichment(BigDecimal isotopeEnrichment){
        artifactNode.setUdf(ClarityUdf.SAMPLE_ISOTOPE_ENRICHMENT.udf, isotopeEnrichment)
    }

    String getUdfSampleQCResult() {
        return artifactNode.getUdfAsString(ClarityUdf.SAMPLE_QC_RESULT.udf)
    }

    void setUdfSampleQCResult(String result) {
        artifactNode.setUdf(ClarityUdf.SAMPLE_QC_RESULT.udf, result)
    }

    String getUdfSampleQCFailureMode() {
        return artifactNode.getUdfAsString(ClarityUdf.SAMPLE_QC_FAILURE_MODE.udf)
    }

    void setUdfSampleQCFailureMode(String result) {
        artifactNode.setUdf(ClarityUdf.SAMPLE_QC_FAILURE_MODE.udf, result)
    }

    void passOnGroupFailure() {
        if(udfSampleQCResult == Analyte.PASS) {
            udfSampleQCResult = Analyte.FAIL
            udfSampleQCFailureMode = ClaritySample.GROUP_FAILURE
        }
    }

    boolean getIsControlSample() {
        return claritySample.pmoSample.isControlSample
    }

    boolean getPassedQc() {
        return udfSampleQCResult == Analyte.PASS
    }

    void setSystemQcFlag(boolean qcFlag) {
        artifactNode.systemQcFlag = qcFlag
    }
}
