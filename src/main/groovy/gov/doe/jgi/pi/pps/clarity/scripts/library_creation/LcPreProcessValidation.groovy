package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

/**
 * Created by tlpaley on 12/5/14.
 *
 * Clarity DAP Seq LC Requirements
 * https://docs.google.com/document/d/1zc7V6mqTOIqsglkILLEKsc68uV604mmOso1IYFE3kuw/edit?ts=5da8f61f
 */
class LcPreProcessValidation extends ActionHandler {

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        LibraryCreationProcess clarityProcess = process as LibraryCreationProcess
        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        clarityProcess.lcAdapter.validateInputAnalyteClass()
        clarityProcess.lcAdapter.validateInputsContainerType()
        clarityProcess.lcAdapter.validateBatch() //SingleCell and DAP-Seq
    }
}