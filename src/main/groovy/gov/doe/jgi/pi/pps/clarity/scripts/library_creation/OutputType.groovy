package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock

/**
 * Created by tlpaley on 1/3/16.
 */
interface OutputType {

    void validate()
    void update()
    void print()
    void removeCorners(List beansList)
    void moveOutputToNextWorkflow(List<ClarityLibraryStock> analytes)
    void transferBeansDataToClarityLibraryStocks()
    void updateLibraryIndexUdf(List<ClarityLibraryStock> analytes)
}