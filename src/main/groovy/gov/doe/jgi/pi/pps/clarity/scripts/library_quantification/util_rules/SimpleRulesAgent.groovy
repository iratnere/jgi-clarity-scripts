package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules

/**
 * Created by datjandra on 7/6/2015.
 */
class SimpleRulesAgent {

    protected Set<Rule> rules

    SimpleRulesAgent(Set<Rule> ruleSet) {
        rules = ruleSet
    }

    Action execute(ObjectWithDynamicAttributes state) {
        for (Rule rule : rules) {
            if (rule.evaluate(state)) {
                return rule.action
            }
        }
        return null
    }
}
