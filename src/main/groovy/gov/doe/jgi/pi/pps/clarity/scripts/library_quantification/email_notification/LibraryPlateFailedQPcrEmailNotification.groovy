package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.email_notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

class LibraryPlateFailedQPcrEmailNotification extends EmailNotification{
    static final String SUBJECT = 'Library failed qPCR - '
    static final String TEMPLATE_FILE_NAME = 'library_plate_failed_qpcr'

    @Override
    protected getToList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected getCcList(List<EmailDetails> emailDetailsList) {
        return ''
    }

    @Override
    protected String getSubjectLine(List<EmailDetails> emailDetailsList) {
        String plateName = emailDetailsList.first().containerName
        return "$SUBJECT$plateName"
    }

    @Override
    protected getFromList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes) {
        return analytes.collect{new LibraryPlateFailedQPcrEmailDetails(it)}
    }

    @Override
    protected getBinding(List<EmailDetails> emailDetailsList) {
        String plateName = emailDetailsList.first().containerName
        return [plateName: plateName, details: getSampleList(emailDetailsList)]
    }

    @Override
    protected String getTemplateFileName() {
        return TEMPLATE_FILE_NAME
    }
}
