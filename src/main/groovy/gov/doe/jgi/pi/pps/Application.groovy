package gov.doe.jgi.pi.pps

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.boot.context.ApplicationPidFileWriter
import org.springframework.boot.web.context.WebServerPortFileWriter
//import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
//@SpringBootApplication(exclude = [HibernateJpaAutoConfiguration.class, DataSourceAutoConfiguration.class])
class Application {

    static void main(String[] args) {
        //SpringApplication.run(Application, args)
        SpringApplication app = new SpringApplication(Application.class)
        app.addListeners(new ApplicationPidFileWriter())
        app.addListeners(new WebServerPortFileWriter())
        app.run(args)
    }

}
