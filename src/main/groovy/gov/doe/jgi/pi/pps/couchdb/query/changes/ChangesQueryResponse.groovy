package gov.doe.jgi.pi.pps.couchdb.query.changes

import org.grails.web.json.JSONObject

/**
 * Created by duncanscott on 6/9/15.
 */
class ChangesQueryResponse {

    ChangesQuery changesQuery

    BigInteger lastSeq
    List<ChangesQueryResponseResult> results = []

    ChangesQueryResponse(ChangesQuery changesQuery, JSONObject json) {
        this.changesQuery = changesQuery
        this.lastSeq = json?.'last_seq' as BigInteger
        json?.'results'?.each { JSONObject resultJson ->
            results << new ChangesQueryResponseResult(resultJson,this)
        }
    }

    String toString() {
        "ChangesQueryResponse last-seq: ${lastSeq}, results: ${results}"
    }
}
