package gov.doe.jgi.pi.pps.webtransaction.util

import gov.doe.jgi.pi.pps.util.exception.WebException
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONElement
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class RequestUtils {

	static Logger logger = LoggerFactory.getLogger(RequestUtils.class.name)
	
	private static final String HEADER_PRAGMA = "Pragma";
	private static final String HEADER_EXPIRES = "Expires";
	private static final String HEADER_CACHE_CONTROL = "Cache-Control";
 
	static void setNoCache(HttpServletResponse httpServletResponse) {
	   httpServletResponse.setHeader(HEADER_PRAGMA, 'no-cache');
	   httpServletResponse.setDateHeader(HEADER_EXPIRES, 0);
	   httpServletResponse.setHeader(HEADER_CACHE_CONTROL, 'no-cache');
	   httpServletResponse.addHeader(HEADER_CACHE_CONTROL, 'no-store');
	}

	static JSONObject extractJsonObject(HttpServletRequest servletRequest) throws WebException {
		def json = null
		try {
			json = servletRequest.JSON
		} catch (Throwable t) {
			logger.error(t)
			throw new WebException([code:'json.invalid'], 400, t)
		}

		if (json instanceof JSONObject) {
			return (JSONObject) json
		} else {
			throw new WebException([code:'json.object.expected'], 400)
		}
	}

	static JSONArray extractJsonArray(HttpServletRequest servletRequest) throws WebException {
		def json = null
		try {
			json = servletRequest.JSON
		} catch (Throwable t) {
			logger.error(t)
			throw new WebException([code:'json.invalid'], 400, t)
		}

		if (json instanceof JSONArray) {
			return (JSONArray) json
		} else {
			throw new WebException([code:'json.array.expected'], 400)
		}

	}

	static JSONElement extractJsonElement(HttpServletRequest servletRequest) throws WebException {

        def json = null

        try {
            json = servletRequest.JSON
        } catch (Throwable t) {
            logger.error("invalid json", t)
            throw new WebException([code:'json.invalid'], 400, t)
        }

        if (json instanceof JSONElement) {
            return (JSONElement) json
        } else {
            throw new WebException([code:'json.invalid'], 400)
        }

    }
	
}
