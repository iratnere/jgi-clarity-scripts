package gov.doe.jgi.pi.pps.dapseq

import doe.jgi.pi.pps.dapseq.couch.CouchDb
import doe.jgi.pi.pps.dapseq.couch.CouchDbResponse
import doe.jgi.pi.pps.dapseq.parser.DapSeqJsonSubmission
import doe.jgi.pi.pps.dapseq.schema.DapSeqValidator
import doe.jgi.pi.pps.dapseq.schema.ValidatorResponse
import duncanscott.org.groovy.utils.json.mapper.MappingError
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.SampleIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.async.PromiseList
import grails.core.GrailsApplication
import groovy.util.logging.Slf4j
import org.apache.http.HttpStatus
import org.grails.web.json.JSONElement

@Slf4j
class DapSeqJsonValidationService {

    private int GET_INDEXES_BATCH_SIZE = 20

    GrailsApplication grailsApplication
    DapSeqJsonRetrievalService dapSeqJsonRetrievalService
    ArtifactIndexService artifactIndexService

    void checkCouchDbConnection() {
        ClarityWebTransaction clarityWebTransaction = ClarityWebTransaction.requireCurrentTransaction()
        String couchDbUrl = grailsApplication.config.getProperty('couchdb.dapseq.url')
        String username = grailsApplication.config.getProperty('couchdb.dapseq.username')
        String password = grailsApplication.config.getProperty('couchdb.dapseq.password')

        CouchDb couchDb = new CouchDb(couchDbUrl,username,password)
        if (!couchDb.checkAuthorized()) {
            clarityWebTransaction.exitWithErrorMessage("unable to login to CouchDb [${couchDbUrl}]",500)
        }
    }

    private static void reportMappingErrors(DapSeqJsonSubmission submission, String introErrorMessage = "dap-seq JSON submission failed validation") {
        List<MappingError> errors = submission.mappingErrors
        if (!errors) {
            return
        }
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        if (introErrorMessage) {
            webTransaction.addErrorMessage(introErrorMessage)
        }
        errors.each { MappingError mappingError ->
            webTransaction.addErrorMessage("${mappingError}")
        }
        webTransaction.exit(422)
    }

    void checkIndexesValid(DapSeqJsonSubmission submission, boolean setIndexSequences = true) {
        //submission should not have errors at this point and
        //this call should not cause the web-transaction to exit.
        reportMappingErrors(submission)
        Map<String, String> allDapIndexes = artifactIndexService.getDapSeqIndexMap()
        try {
            Map<String, String> indexNameSequence = [:]
            submission.indexNames.each { String indexName ->
                String indexSequence = allDapIndexes[indexName]
                if (!indexSequence) {
                    submission.addError("sequence not found for index name [${indexName}]")
                } else {
                    indexNameSequence[indexName] = indexSequence
                }
            }
            artifactIndexService.validatePoolIndexes("DAP pool", indexNameSequence.values())?.each {
                submission.addError(it)
            }
        } catch (IllegalArgumentException iae) {
            submission.addError(iae.getMessage())
        }
        //this call will cause web-transaction to exit if any errors were thrown by artifactIndexService.getIndexes call above
        reportMappingErrors(submission, "dap-seq JSON submission failed validation due to invalid indexes")
    }

    //Web service: All input organisms (input parent scheduled samples) should correspond to json organisms(samples)
    void checkInputSamples(DapSeqJsonSubmission submission, Collection<ScheduledSample> scheduledSamples) {
        ClarityWebTransaction clarityWebTransaction = ClarityWebTransaction.requireCurrentTransaction()
        Map<Long,List<ScheduledSample>> pmoSampleIdScheduledSample = [:].withDefault{[]}
        scheduledSamples?.each { ScheduledSample scheduledSample ->
            Long pmoSampleId = scheduledSample.pmoSampleId
            if (pmoSampleId) {
                pmoSampleIdScheduledSample[pmoSampleId] << scheduledSample
            } else {
                clarityWebTransaction.addErrorMessage("no PMO sample ID found for ${scheduledSample}")
            }
        }
        if (clarityWebTransaction.errorMessages) {
            clarityWebTransaction.exit(422)
        }
        if (!pmoSampleIdScheduledSample) {
            clarityWebTransaction.exitWithErrorMessage("no PMO sample IDs associated with input scheduled samples", 422)
        }
        pmoSampleIdScheduledSample.each {Long pmoSampleId, List<ScheduledSample> sampleScheduledSamples ->
            if (sampleScheduledSamples.size() > 1) {
                clarityWebTransaction.addErrorMessage("PMO sample ID [${pmoSampleId}] represented by multiple scheduled samples ${scheduledSamples}")
            }
        }
        if (clarityWebTransaction.errorMessages) {
            clarityWebTransaction.exit(422)
        }

        Set<Long> jsonSampleIds = submission.organismSampleIds
        Set<Long> pmoSampleIds = new HashSet(pmoSampleIdScheduledSample.keySet())

        if (!jsonSampleIds) {
            clarityWebTransaction.exitWithErrorMessage("no sample IDs extracted from JSON submission", 422)
        }
        if (!jsonSampleIds.containsAll(pmoSampleIds)) {
            pmoSampleIds.removeAll(jsonSampleIds)
            clarityWebTransaction.exitWithErrorMessage(
                    "the following parent PMO sample IDs of input scheduled samples are not represented in the JSON submission: ${pmoSampleIds.sort()}",
                    422)
        }

        if (!pmoSampleIds.containsAll(jsonSampleIds)) {
            jsonSampleIds.removeAll(pmoSampleIds)
            clarityWebTransaction.exitWithErrorMessage(
                    "the following sample-id's in the JSON submission are not represented by parent PMO samples of process inputs: ${jsonSampleIds.sort()}",
                    422)
        }
    }

    void checkPmoSampleIds(Collection<Long> pmoSampleIds) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = webTransaction.requireNodeManager()
        NodeConfig nodeConfig = nodeManager.nodeConfig

        PromiseList<Long> notFoundIds = new PromiseList<>()
        pmoSampleIds?.each { Long pmoSampleId ->
            notFoundIds << grails.async.Promises.task {
                SampleParameterValue spv = SampleParameter.NAME.setValue(pmoSampleId as String)
                SampleIdIterator sampleIdIterator = new SampleIdIterator(nodeConfig,[spv])
                if (!sampleIdIterator.find()) {
                    return pmoSampleId
                }
                return null
            }
        }
        List<Long> badIds = notFoundIds.get().findAll().sort()
        badIds.each { Long badPmoSampleId ->
            webTransaction.addErrorMessage("sample not found for PMO sample ID [${badPmoSampleId}]")
        }
        if (badIds) {
            webTransaction.exit(422)
        }
    }


    void validateOnly() {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        JSONElement json = webTransaction.jsonSubmission
        DapSeqJsonSubmission submission = validateSubmittedJson(json)
        checkPmoSampleIds(submission.organismSampleIds)
        boolean setIndexSequences = false
        checkIndexesValid(submission, setIndexSequences)
        webTransaction.statusCode = HttpStatus.SC_OK
    }


    DapSeqJsonSubmission validateSubmittedJson(Object json) {
        DapSeqJsonSubmission submission = DapSeqJsonSubmission.newSubmission(json)
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()

        //include couchdb authorization (verify credentials and that couchdb is alive)
        checkCouchDbConnection()

        CouchDbResponse latestSchema = dapSeqJsonRetrievalService.latestSchema
        if (!latestSchema.success) {
            webTransaction.exitWithErrorMessage("failed to retrieve JSON schema for validation of dap-seq JSON submission ${latestSchema}", latestSchema.statusCode)
        }
        String schemaId = latestSchema.json['$id']
        if (!schemaId) {
            webTransaction.exitWithErrorMessage("failed to obtain ID of JSON schema for validation of dap-seq JSON submission", 500)
        }

        DapSeqValidator schemaValidator = new DapSeqValidator(latestSchema.json.toJSONString())
        ValidatorResponse validatorResponse = schemaValidator.validate(submission.json.toJSONString())
        if (!validatorResponse.validated) {
            webTransaction.addErrorMessage("dap-seq JSON submission failed schema validation")
            validatorResponse.errorMessages.each {String errorMessage ->
                webTransaction.addErrorMessage(errorMessage)
            }
            webTransaction.exit(422)
        }
        submission.addValidation(schemaId)

        /*
        Web service: All input organisms should be in all wells in all plates with distinct indexes
        Web service: All sample-ids associated with wells in JSON submission should be represented by organisms in the JSON submission.
        Should all indexes be represented at most once in a protein plate?
         */
        if (!submission.validate()) {
            reportMappingErrors(submission)
        }
        //ensure that indexes used are valid and registered
        checkIndexesValid(submission)
        submission
    }


    DapSeqJsonSubmission validateSubmittedJson(Object json, Collection<ScheduledSample> scheduledSamples) {
        DapSeqJsonSubmission submission = validateSubmittedJson(json)
        //Web service: All input organisms (input parent scheduled samples) should correspond to json organisms(samples)
        checkInputSamples(submission,scheduledSamples)
        submission
    }

}
