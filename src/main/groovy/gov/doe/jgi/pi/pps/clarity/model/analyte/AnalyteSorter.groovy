package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity_node_manager.util.JgiArtifactComparator

/**
 * Created by duncanscott on 9/23/16.
 *
 https://issues.jgi-psf.org/browse/PPS-3229

 On loading artifact nodes in a batch, it is convenient if they are returned in an order.
 Sorting rules are as described below:
 1. artifacts in tubes should be placed before artifacts on plates.
 2. all artifacts in tubes should be sorted by the PMO sample id
 3. Plates must be sorted by the container name.
 4. all artifacts on a plate should be sorted by their plate location (A1, B1, C1,...., A2, B2, C2,.....)
 */

class AnalyteSorter {

    static List sort(Iterable<Analyte>  analytes) {
        List<Analyte> analyteList = []
        analytes?.each { analyteList << it }
        sortInPlace(analyteList)
        analyteList
    }

    static void sortInPlace(List<Analyte> analytes) {
        analytes.sort { Analyte a1, Analyte a2 -> JgiArtifactComparator.staticCompare(a1.artifactNodeInterface, a2.artifactNodeInterface) }
    }

}
