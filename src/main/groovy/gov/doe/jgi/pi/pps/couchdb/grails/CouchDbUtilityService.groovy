package gov.doe.jgi.pi.pps.couchdb.grails

import gov.doe.jgi.pi.pps.couchdb.query.all_docs.AllDocsQuery
import gov.doe.jgi.pi.pps.couchdb.query.all_docs.AllDocsQueryResponse
import gov.doe.jgi.pi.pps.couchdb.query.changes.ChangesQuery
import gov.doe.jgi.pi.pps.couchdb.query.changes.ChangesQueryResponse
import gov.doe.jgi.pi.pps.couchdb.query.changes.ChangesQueryResponseResult
import gov.doe.jgi.pi.pps.couchdb.transaction.CouchdbWebTransactionRecorder
import gov.doe.jgi.pi.pps.couchdb.util.CouchDb
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransaction
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovyx.net.http.*
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Transactional
class CouchDbUtilityService {

    static final String LAST_CHANGE_LOADED = 'last-change-loaded'
    static final String SUBMISSION_URI = 'submission-uri'

    CouchDbService couchDbService
    Logger logger = LoggerFactory.getLogger(CouchDbUtilityService.class.name)
    GrailsApplication grailsApplication

    OnDemandCache<CouchDb> cachedPluginUtilCouchDb = new OnDemandCache<>()
    OnDemandCache<CouchDb> cachedPluginTestCouchDb = new OnDemandCache<>()

    boolean getDisabled() {
        (boolean) grailsApplication.config.couchdb.disabled
    }

    String getCouchDbBaseUrl() {
        couchDbService.couchDbBaseUrl
    }

    String couchDbUrl(String couchDbName) {
        couchDbService.couchDbUrl(couchDbName)
    }

    CouchDb getGrailsPluginUtilDb() {
        return cachedPluginUtilCouchDb.fetch {
            CouchDb utilityDb = new CouchDb(name: 'grails-plugin-util')
            return utilityDb
        }
    }

    CouchDb getGrailsPluginTestDb() {
        return cachedPluginTestCouchDb.fetch {
            CouchDb testDb = new CouchDb(name: 'grails-plugin-test')
            return testDb
        }
    }

    void initializeDatabase(CouchDb couchdb) {
        if (couchDbService.disabled) {
            return
        }
        String url = couchDbService.couchDbUrl(couchdb.name)
        logger.info "initializing couchdb ${couchdb} at URL: ${url}"
        HTTPBuilder http = new HTTPBuilder(url)
        try {
            http.request(Method.PUT, ContentType.JSON) { req ->
                response.success = { HttpResponseDecorator resp, Map json ->
                    logger.info "created ${couchdb}"
                }
            }
        } catch (HttpResponseException e) {
            if (412 != e.response.status) {
                throw e
            }
        }
    }

    void initializeAllDatabases() {
        initializeDatabase(grailsPluginUtilDb)
        initializeDatabase(grailsPluginTestDb)
    }

    void deleteDatabase(CouchDb couchdb) {
        if (couchDbService.disabled) {
            return
        }
        String url = couchDbService.couchDbUrl(couchdb.name)
        HTTPBuilder http = new HTTPBuilder(url)
        http.request( Method.DELETE ) {
            response.success = { HttpResponseDecorator resp, respJson ->
                logger.info "deleted database ${url}"
            }
        }
    }

    void deleteStuff(String stuffId) {
        CouchDb utilDb = getGrailsPluginUtilDb()
        couchDbService.delete(utilDb.name,stuffId)
    }

    void putStuff(String stuffId, JSONObject json) {
        CouchDb utilDb = getGrailsPluginUtilDb()
        couchDbService.insertOrForceUpdate(utilDb.name, json, stuffId)
    }

    JSONObject getStuff(String stuffId) {
        CouchDb utilDb = getGrailsPluginUtilDb()
        JSONObject s = couchDbService.get(utilDb.name, stuffId)
        if (s == null) {
            s = new JSONObject()
        }
        return s
    }

    ChangesQueryResponse performChangesQuery(ChangesQuery query) {
        ChangesQueryResponse changesQueryResponse = null
        String url = couchDbService.couchDbUrl(query.couchdb.name)
        HTTPBuilder http = new HTTPBuilder(url)
        http.get(contentType:ContentType.JSON, path:'_changes',query:[since:query.since,limit:query.limit]) { HttpResponseDecorator resp, Map json ->
            changesQueryResponse = new ChangesQueryResponse(query, JsonUtil.toJsonObject(json))
        }
        return changesQueryResponse
    }

    AllDocsQueryResponse performAllDocsQuery(AllDocsQuery query) {
        AllDocsQueryResponse allDocsQueryResponse = null
        String url = couchDbService.couchDbUrl(query.couchDb.name)
        HTTPBuilder http = new HTTPBuilder(url)
        http.post(contentType:ContentType.JSON, path:'_all_docs',query:query.params) { HttpResponseDecorator resp, Map json ->
            allDocsQueryResponse = new AllDocsQueryResponse(query, JsonUtil.toJsonObject(json))
        }
        return allDocsQueryResponse
    }

    void loadChanges(CouchDb couchDb, Integer limit, String changesStuffPrefix = 'changes-seq-') {
        synchronized (couchDb.changesQueue) {
            if (couchDb.changesQueue.isEmpty()) {
                String stuffId = "${changesStuffPrefix}${couchDb.name}"
                JSONObject lastChangeLoadedObj = getStuff(stuffId)
                if (couchDb.loadChangesSince) {
                    lastChangeLoadedObj[LAST_CHANGE_LOADED] = couchDb.loadChangesSince
                    putStuff(stuffId, lastChangeLoadedObj)
                } else {
                    Long lastChangeLoaded = lastChangeLoadedObj[LAST_CHANGE_LOADED] as Long
                    if (lastChangeLoaded != null) {
                        couchDb.loadChangesSince = lastChangeLoaded
                        //log.debug "${couchDb} saved since ${couchDb.loadChangesSince}"
                    }
                }
                ChangesQuery changesQuery = new ChangesQuery(couchDb, couchDb.loadChangesSince, limit)
                ChangesQueryResponse changesQueryResponse = performChangesQuery(changesQuery)
                if (changesQueryResponse?.results) {
                    couchDb.changesQueue.addAll(changesQueryResponse.results)
                    //log.debug "${couchDb} queue size after appending results: ${couchDb.changesQueue.size()}"
                    couchDb.loadChangesSince = changesQueryResponse.results[-1].seq
                    //log.debug "${couchDb} saved since ${couchDb.loadChangesSince}"
                } else {
                    //log.info "no results to append to queue"
                }
            }
        }
    }

    ChangesQueryResponseResult nextChange(CouchDb couchDb, Integer limit, String changesStuffPrefix = 'changes-seq-') {
        synchronized (couchDb.changesQueue) {
            ChangesQueryResponseResult change = couchDb.changesQueue.poll()
            if (!change) {
                loadChanges(couchDb, limit, changesStuffPrefix)
                change = couchDb.changesQueue.poll()
            }
            return change
        }
    }

    BigInteger taskChangeSeq(CouchDb couchDb, String changesStuffPrefix = 'changes-seq-') {
        String stuffId = "${changesStuffPrefix}${couchDb.name}"
        synchronized (couchDb.changesQueue) {
            JSONObject lastChangeLoadedObj = getStuff(stuffId)
            return lastChangeLoadedObj[LAST_CHANGE_LOADED] as BigInteger
        }
    }

    void setTaskChangeSeq(CouchDb couchDb, BigInteger changeSeq, String changesStuffPrefix = 'changes-seq-') {
        if (!(changeSeq != null && changeSeq >= 0)) {
            throw new RuntimeException ('Change sequence must be greater than or equal to 0.')
        }
        String stuffId = "${changesStuffPrefix}${couchDb.name}"
        synchronized (couchDb.changesQueue) {
            JSONObject lastChangeLoadedObj = getStuff(stuffId)
            lastChangeLoadedObj[LAST_CHANGE_LOADED] = changeSeq
            putStuff(stuffId, lastChangeLoadedObj)
            couchDb.loadChangesSince = changeSeq
        }
    }

    void logWebTransaction(WebTransaction webTransaction, CouchdbWebTransactionRecorder couchdbRecorder) {
        String databaseUrl = couchDbService.couchDbUrl(couchdbRecorder.couchDb.name)
        String submissionUri = "${databaseUrl}${webTransaction.transactionId}"
        webTransaction.jsonResponse.put(SUBMISSION_URI, submissionUri)
        JSONObject couchDbRecord = couchdbRecorder.couchDbLogRecord(webTransaction)
        couchDbService.insert(couchdbRecorder.couchDb.name,couchDbRecord,webTransaction.transactionId)
    }

}
