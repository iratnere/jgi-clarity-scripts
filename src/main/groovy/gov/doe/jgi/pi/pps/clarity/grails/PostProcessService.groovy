package gov.doe.jgi.pi.pps.clarity.grails

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.gorm.transactions.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Propagation

class PostProcessService {

    Logger logger = LoggerFactory.getLogger(PostProcessService.class.name)

    RoutingService routingService

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    String postInputProcess(ClarityProcess currentProcess, Stage stage, List<ArtifactNode> inputArtifacts) {
        if (!inputArtifacts) {
            logger.warn "Process not created because there are no inputs"
            return null
        }

        logger.info "Routing Analytes ${inputArtifacts.collect { it.id }} to ${stage.value}"
        List<RoutingRequest> requests = RoutingRequest.generateRequestsToRouteArtifactIdsToUri(stage.uri, inputArtifacts.collect {
            it.id
        })
        routingService.submitRoutingRequests(requests)
        logger.info "Posting process to stage ${stage.value} with inputs ${inputArtifacts*.id}"

        NodeManager nodeManager = currentProcess.nodeManager
        StageNode stageNode = nodeManager.getStageNode(stage.workflow.id,stage.id)
        ProcessParams processDetails = new ProcessParams(stageNode.stepConfigurationNode, inputArtifacts)
        processDetails.researcherId = currentProcess.processNode.technicianId
        String processId = currentProcess.nodeManager.executeClarityInputProcess(processDetails)
        processId
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    String postInputProcess(Stage stage, List<ArtifactNode> inputArtifacts, NodeManager nodeManager, String technicianId) {
        NodeConfig nodeConfig = nodeManager.nodeConfig
        logger.info "Routing Analytes ${inputArtifacts.collect { it.id }} to ${stage.value}"
        List<RoutingRequest> requests = RoutingRequest.generateRequestsToRouteArtifactIdsToUri(stage.uri, inputArtifacts.collect {
            it.id
        })
        routingService.submitRoutingRequests(requests)
        logger.info "Posting process to stage ${stage.value} with inputs ${inputArtifacts*.id}"
        ProcessParams processDetails = new ProcessParams(stage.stageNode(nodeManager).stepConfigurationNode, inputArtifacts)
        processDetails.researcherId = technicianId
        String processId = nodeManager.executeClarityInputProcess(processDetails)
        processId
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    String postInputProcess(Stage stage, ProcessParams processDetails) {
        List<ArtifactNode> inputArtifacts = processDetails.inputArtifacts
        NodeConfig nodeConfig = processDetails.nodeManager.nodeConfig
        logger.info "Routing Analytes ${inputArtifacts.collect { it.id }} to ${stage.value}"
        List<RoutingRequest> requests = RoutingRequest.generateRequestsToRouteArtifactIdsToUri(stage.uri, inputArtifacts.collect {
            it.id
        })
        routingService.submitRoutingRequests(requests)
        logger.info "Posting process to stage ${stage.value} with inputs ${inputArtifacts*.id}"
        String processId = processDetails.nodeManager.executeClarityInputProcess(processDetails)
        processId
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    ProcessNode postPoolingProcess(NodeManager nodeManager, Stage stage, ProcessParams processParams) {
        NodeConfig nodeConfig = nodeManager.nodeConfig
        def inputArtifacts = processParams.inputArtifacts
        logger.info "Routing Analytes ${inputArtifacts.collect { it.id }} to ${stage.value}"
        List<RoutingRequest> requests = RoutingRequest.generateRequestsToRouteArtifactIdsToUri(stage.uri, inputArtifacts.collect {
            it.id
        })
        routingService.submitRoutingRequests(requests)
        logger.info "Posting process to stage ${stage.value} with inputs ${inputArtifacts*.id}"
        String processId = nodeManager.executeClarityPoolingProcess(processParams)
        ProcessNode postedProcess = nodeManager.getProcessNode(processId)
        postedProcess.outputAnalytes.each{it.httpRefresh()}
        postedProcess
    }

}
