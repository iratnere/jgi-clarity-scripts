package gov.doe.jgi.pi.pps.clarity.cv

import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseInsensitive


enum SampleStatusCv {
	CREATED('Created'), 
	AWAITING_COLLABORATOR_METADATA('Awaiting Collaborator Metadata'),
	BIOSAFETY_REVIEW('BioSafety Review'),
	AWAITING_SHIPPING_APPROVAL('Awaiting Shipping Approval'),
	AWAITING_STERILITY_DOCUMENT('Awaiting Sterility Document'),
	STERILITY_DOCUMENT_REVIEW('Sterility Document Review'),
	AWAITING_SAMPLE_RECEIPT('Awaiting Sample Receipt'),
	SAMPLE_RECEIVED('Sample Received'),
	AWAITING_SAMPLE_QC('Awaiting Sample QC'),
	SAMPLE_QC_IN_PROGRESS('Sample QC In Progress'),
	SAMPLE_QC_COMPLETE('Sample QC Complete'),
	AVAILABLE_FOR_USE('Available For Use'),
	EXHAUSTED('Exhausted'),
	ABANDONED('Abandoned'),
	NEEDS_ATTENTION('Needs Attention'),
	ON_HOLD('On Hold'),
	DELETED('Deleted')

	final String value

	SampleStatusCv(String value) {
		this.value = value
	}
	
	static List<SampleStatusCv> toList() {
		List<SampleStatusCv> statusList = []
		for (SampleStatusCv cv : SampleStatusCv.values()) {
			statusList << cv
		}
		return statusList
	}

	
	public String toString() {
		return value
	}
	
	private static final EnumConverterCaseInsensitive<SampleStatusCv> converter = new EnumConverterCaseInsensitive(SampleStatusCv)
	
	public static SampleStatusCv toEnum(value) {
		return converter.toEnum(value)
	}

}
