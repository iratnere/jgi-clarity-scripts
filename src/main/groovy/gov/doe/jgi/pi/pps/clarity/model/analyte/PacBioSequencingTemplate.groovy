package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import groovy.transform.PackageScope

/**
 * Created by datjandra on 9/21/2015.
 */
class PacBioSequencingTemplate extends Analyte {

    @PackageScope
    PacBioSequencingTemplate(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
    }

    BigInteger getAcquisitionTime() {
        /*
        https://docs.google.com/document/d/1B5yAijzSeYw7V6Iz9CxNxmyLeLqkfKdnhma4_E3Yyrk
        for Sequel workflows
        Acquisition Time*;  show the read length from run mode as default value
        */
        return artifactNodeInterface.getUdfAsBigInteger(ClarityUdf.ANALYTE_ACQUISITION_TIME.udf, getReadLength())
    }

    void setAcqusitionTime(BigInteger time) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_ACQUISITION_TIME.udf, time)
    }

    String getAutomationName() {
        artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_AUTOMATION_NAME.udf,'')
    }

    BigInteger getInsertSize() {
        return artifactNodeInterface.getUdfAsBigInteger(ClarityUdf.ANALYTE_PACBIO_INSERT_SIZE.udf)
    }

    String getSequencingMode() {
        artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_SEQUENCING_MODE.udf)?.toUpperCase()
    }

    void setSequencingMode(String value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_SEQUENCING_MODE.udf, value)
    }

    String getGenerateCcsData() {
        artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_GENERATE_CCS_DATA.udf)?.toUpperCase()
    }

    void setGenerateCcsData(String value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_GENERATE_CCS_DATA.udf, value)
    }

    String getSizeSelection() {
        artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_SIZE_SELECTION.udf)?.toUpperCase()
    }

    BigDecimal getOnPlateLoadingConcentrationPm() {
        if (udfPacBioLoadingConcentrationNm != null)
            return udfPacBioLoadingConcentrationNm * 1000
        return null
    }

    BigDecimal getMovieTimeHrs() {
        if (acquisitionTime != null)
            return acquisitionTime / 60
        return null
    }

    BigDecimal getUdfPacBioLoadingConcentrationNm() {
        return  artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_PACBIO_LOADING_CONCENTRATION_NM.udf)
    }

    String getWellLocationInPacBioFormat() {
        String well = artifactNodeInterface.location
        String pacBioWell = '1'
        if ('1:1' != well) {
            pacBioWell = well
            List<String> rowCol = well.split(':')
            if (2 == rowCol.size()) {
                if (1 == rowCol[1]?.size() && rowCol[1].isInteger()) {
                    pacBioWell = "${rowCol[0]}0${rowCol[1]}"
                } else {
                    pacBioWell = "${rowCol[0]}${rowCol[1]}"
                }
            }
        }
        return pacBioWell
    }

    PacBioBindingComplex getParentAnalyte() {
        ArtifactNodeInterface parentArtifact = artifactNodeInterface.getSingleParentAnalyte()
        return AnalyteFactory.analyteInstance(parentArtifact) as PacBioBindingComplex
    }

    private static String toExcelString(val) {
        String strVal = val as String
        /*
        if (val?.isNumber()) {
            return 'barcode ' + strVal
        }
        */
        strVal
    }

    String getDnaTemplatePrepKitBoxBarcode() {
        //parentLibrary could be ClarityLibraryStock or ClarityLibraryPool; LC process udf
        return parentLibrary.smrtbellTemplatePrepKit
    }

    @Override
    Analyte getParentLibrary() {
        PacBioBindingComplex bindingComplexParent = parentAnalyte
        PacBioAnnealingComplex annealingComplexParent = bindingComplexParent.parentAnalyte
        return annealingComplexParent.parentAnalyte //could be ClarityLibraryStock or ClarityLibraryPool
    }

    String getSampleIsBarcoded() {
        if (isClarityLibraryPool)
            return Boolean.TRUE.toString().toUpperCase()
        return Boolean.FALSE.toString().toUpperCase()
    }

    String getIsCollection() {
        if (isClarityLibraryPool)
            return Boolean.FALSE.toString().toUpperCase()
        return Boolean.TRUE.toString().toUpperCase()
    }

    String getSameBarcodesOnBothEndsOfSequence() {
        if (isClarityLibraryPool)
            return Boolean.TRUE.toString().toUpperCase()
        return null
    }

    boolean getIsClarityLibraryPool() {
        return (parentLibrary instanceof ClarityLibraryPool)
    }

    String getBindingKitBoxBarcodeAsExcelString() {
        toExcelString(bindingKitBoxBarcode)
    }

    String getBindingKitBoxBarcode() {
        return parentAnalyte.udfBindingKitBoxBarcode
    }

    BigInteger getReadLength() {
        Analyte library = parentLibrary //could be ClarityLibraryStock or ClarityLibraryPool
        return library.readLengthBp
    }

    String getRunMode() {
        Analyte library = parentLibrary //could be ClarityLibraryStock or ClarityLibraryPool
        return library.sequencerModelCv.sequencerModel
    }

    //PPS-3103
    String getSampleNameForCsv(){
        return "${artifactNodeInterface.singleParentAnalyte.singleParentAnalyte.singleParentAnalyte.name}_${id}"
    }
}