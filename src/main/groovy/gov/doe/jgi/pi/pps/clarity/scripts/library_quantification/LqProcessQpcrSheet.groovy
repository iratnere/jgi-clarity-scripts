package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.KeyValueSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.AttributeValues
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.PlatePassFailKeyValueBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.QpcrTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode

/**
 * Created by datjandra on 7/23/2015.
 */
class LqProcessQpcrSheet extends ActionHandler {

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        ExcelWorkbook excelWorkbook = null
        try {
            excelWorkbook = process.getQpcrWorkbook()
            if (!excelWorkbook) {
                throw new RuntimeException()
            }
        } catch (Exception e) {
            process.postErrorMessage("qPCR worksheet was not attached")
        }

        Section dilutionSection = new TableSection(1, QpcrTableBean.class, 'H27', 'V395')
        excelWorkbook.addSection(dilutionSection)

        Section passFailSection = new KeyValueSection(1, PlatePassFailKeyValueBean.class, 'H22')
        excelWorkbook.addSection(passFailSection)

        TableSection nameSection = new TableSection(1, QpcrTableBean.class, 'C1', 'C385', true)
        excelWorkbook.addSection(nameSection)
        excelWorkbook.load()
        validateWorkbook(excelWorkbook)
        process.setCompleteStage()
    }

    void validateWorkbook(ExcelWorkbook excelWorkbook) {
        Map<String, Analyte> inputsByName = new HashMap<String, Analyte>()
        List<Analyte> inputs = process.inputAnalytes
        for (Analyte input : inputs) {
            inputsByName.put(input.name, input)
        }
        ClarityWebTransaction.logger.info "Loaded ${inputs.size()} input analytes"
        List<QpcrTableBean> dilutionTable = excelWorkbook.sections.values()[0].getData()

        PlatePassFailKeyValueBean platePassFailKeyValue = excelWorkbook.sections.values()[1].data
        String platePassFailValue = platePassFailKeyValue.passFail.value
        ContainerNode plateContainerNode = process.inputAnalytes*.containerNode.find{it.isPlate} //PPV: Combination of multiple tubes and ONE plate
        validatePlate(plateContainerNode, platePassFailValue)

        ClarityWebTransaction.logger.info "Loaded ${dilutionTable.size()} rows from dilution table"
        Set<String> observedInputs = new HashSet<String>()
        for (QpcrTableBean tableBean : dilutionTable) {
            String[] words = tableBean.library.split('\\s+')
            String name = words.take(words.size() - 1).join(' ')
            Analyte inputAnalyte = inputsByName.get(name)
            if (!inputAnalyte) {
                continue
            }

            if (observedInputs.contains(inputAnalyte.id)) {
                continue
            }
            observedInputs.add(inputAnalyte.id)

            ClarityLibraryStock libraryStock = null
            ClarityLibraryPool libraryPool = null
            if (inputAnalyte instanceof ClarityLibraryStock) {
                libraryStock = inputAnalyte as ClarityLibraryStock
            } else if (inputAnalyte instanceof ClarityLibraryPool) {
                libraryPool = inputAnalyte as ClarityLibraryPool
            }

            if (libraryStock) {
                String passFail = tableBean.passFail.value
                if (!passFail) {
                    String errorMessage = "Library Pass/Fail must be set"
                    process.postErrorMessage(errorMessage)
                }

                if (platePassFailValue?.equals(AttributeValues.DONE) && libraryStock.containerNode.isPlate) {
                    if (!passFail.equals(Analyte.FAIL) && !passFail.equals(Analyte.PASS)) {
                        String errorMessage = "if plate ${AttributeValues.DONE} analytes must be either 'Pass' or 'Fail'"
                        process.postErrorMessage(errorMessage)
                    }
                }

                if (passFail.equals(Analyte.FAIL)) {
                    String failureMode = tableBean.failureMode.value
                    if (!failureMode) {
                        String errorMessage = "Failure mode required for $libraryStock"
                        process.postErrorMessage(errorMessage)
                    }
                }

                BigDecimal libraryMolarityPm = tableBean.libraryMolarityPm
                if (libraryMolarityPm == null) {
                    String errorMessage = "Library molarity required for $libraryStock"
                    process.postErrorMessage(errorMessage)
                }

                BigDecimal volumeUsed = tableBean.volumeUsed
                if (volumeUsed == null) {
                    String errorMessage = "Volume used required for $libraryStock"
                    process.postErrorMessage(errorMessage)
                }
            } else if (libraryPool) {
                String passFail = tableBean.passFail.value
                if (!passFail) {
                    String errorMessage = "Library Pass/Fail must be set"
                    process.postErrorMessage(errorMessage)
                }

                if (platePassFailValue?.equals(AttributeValues.DONE) && libraryPool.containerNode.isPlate) {
                    if (!passFail.equals(Analyte.FAIL) && !passFail.equals(Analyte.PASS)) {
                        String errorMessage = "if plate ${AttributeValues.DONE} analytes must be either 'Pass' or 'Fail'"
                        process.postErrorMessage(errorMessage)
                    }
                }

                if (passFail.equals(Analyte.FAIL)) {
                    String failureMode = tableBean.failureMode.value
                    if (!failureMode) {
                        String errorMessage = "Failure mode required for $libraryPool"
                        process.postErrorMessage(errorMessage)
                    }
                }

                BigDecimal libraryMolarityPm = tableBean.libraryMolarityPm
                if (libraryMolarityPm == null) {
                    String errorMessage = "Library molarity required for $libraryPool"
                    process.postErrorMessage(errorMessage)
                }

                BigDecimal volumeUsed = tableBean.volumeUsed
                if (volumeUsed == null) {
                    String errorMessage = "Volume used required for $libraryPool"
                    process.postErrorMessage(errorMessage)
                }
            }
        }
    }

    def validatePlate(ContainerNode plateContainerNode, String platePassFailValue) {
        if (!plateContainerNode)
            return null
        if (!(platePassFailValue in [AttributeValues.DONE, AttributeValues.REWORK])) {
            String errorMessage = "Plate Pass/Fail required for plate"
            process.postErrorMessage(errorMessage)
        }
        if (AttributeValues.DONE != platePassFailValue)
            return null
        plateContainerNode.getContentsArtifactNodes().each{ ArtifactNode artifactNode ->
            if (artifactNode.id in process.inputAnalytes*.id)
                return null
            Analyte notInputAnalyte = AnalyteFactory.analyteInstance(artifactNode)
            String qpcrResult = notInputAnalyte.udfLibraryQpcrResult
            if (!( qpcrResult in [AttributeValues.PASS, AttributeValues.FAIL])) {
                process.postErrorMessage("""
Library $notInputAnalyte.name($notInputAnalyte.id)
Invalid '${ClarityUdf.ANALYTE_LIBRARY_QPCR_FAILURE_MODE.udf}' udf:'$qpcrResult'.
If plate ${AttributeValues.DONE} analytes must be either '$AttributeValues.PASS' or '$AttributeValues.FAIL'"
""")
            }
        }
    }

}
