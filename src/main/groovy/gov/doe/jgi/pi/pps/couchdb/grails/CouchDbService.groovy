package gov.doe.jgi.pi.pps.couchdb.grails

import gov.doe.jgi.pi.pps.couchdb.util.CouchDbEntity
import gov.doe.jgi.pi.pps.couchdb.util.CouchDbEntityBeforeAfter
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import grails.core.GrailsApplication
import grails.util.Environment
import grails.util.GrailsUtil
import groovyx.net.http.*
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CouchDbService {

    Logger logger = LoggerFactory.getLogger(CouchDbService.class.name)

    GrailsApplication grailsApplication

    boolean getDisabled() {
        String disabledString = grailsApplication.config.getProperty('couchdb.disabled')
        logger.debug "disabled string: ${disabledString}"
        boolean iAmDisabled = false
        if (disabledString) {
            iAmDisabled = Boolean.valueOf(disabledString)
        }
        if (iAmDisabled) {
            logger.warn "couchdb insert, update, and delete disabled"
        }
        return iAmDisabled
    }

    String getCouchDbBaseUrl() {
        def url = grailsApplication.config.getProperty('couchdb.baseUrl')
        Environment.current
        logger.info "environment: ${Environment.current}, couchdb.baseUrl: ${url}"
        if (url instanceof String) {
            if (url.endsWith('/')) {
                return url
            }
            return url + '/'
        }

        throw new RuntimeException('couchdb.baseUrl not configured for current environment')
    }

    String couchDbUrl(String couchDbName) {
        def url = grailsApplication.config.getProperty("couchdb.${couchDbName}.url")
        if (url instanceof String) {
            if (url.endsWith('/')) {
                return url
            }
            return url + '/'
        }
        return getCouchDbBaseUrl() + "${couchDbName}/"
    }


    void insert(CouchDbEntity couchDbEntity) {
        if (couchDbEntity instanceof CouchDbEntityBeforeAfter) {
            couchDbEntity.beforeInsert()
        }
        JSONObject jsonResponse = insert(couchDbEntity.couchDbName,couchDbEntity.json,couchDbEntity.id)
        couchDbEntity.setId(jsonResponse.'id' as String)
        couchDbEntity.setRevision(jsonResponse.'rev' as String)
        if (couchDbEntity instanceof CouchDbEntityBeforeAfter) {
            couchDbEntity.afterInsert()
        }
    }


    void update(CouchDbEntity couchDbEntity) {
        if (couchDbEntity instanceof CouchDbEntityBeforeAfter) {
            couchDbEntity.beforeUpdate()
        }

        String id = couchDbEntity.id
        String rev = couchDbEntity.revision

        if (!id) {
            throw new RuntimeException("null ID")
        }
        if (!rev) {
            throw new RuntimeException("null revision")
        }

        JSONObject json = couchDbEntity.json
        if (json == null) {
            json = new JSONObject()
        }
        json.'_id' = id
        json.'_rev' = rev

        JSONObject jsonReponse = update(couchDbEntity.couchDbName,json)
        couchDbEntity.setRevision(jsonReponse.'rev' as String)

        if (couchDbEntity instanceof CouchDbEntityBeforeAfter) {
            couchDbEntity.afterUpdate()
        }
    }

    void insertOrUpdate(CouchDbEntity couchDbEntity) {
        if (couchDbEntity.revision) {
            update(couchDbEntity)
        } else {
            insert(couchDbEntity)
        }
    }

    void merge(CouchDbEntity couchDbEntity) {
        JSONObject jsonToMerge = couchDbEntity.json
        if (!get(couchDbEntity)) {
            try {
                insert(couchDbEntity)
                return
            } catch (HttpResponseException e) {
                if (409 != e.response.status) { //conflict
                    throw e
                }
            }
            require(couchDbEntity)
        }
        JSONObject existingJson = couchDbEntity.json
        if (!jsonToMerge) {
            logger.warn "no json to merge for couchdb entity ${couchDbEntity.couchDbName}/${couchDbEntity.id}\""
            return
        }
        if (!JsonUtil.areEqual(existingJson,jsonToMerge)) {
            JsonUtil.mergeMapsNoReplace(existingJson,jsonToMerge)
            couchDbEntity.setJson(existingJson) //existing json has been merged with jsonToMerge
            update(couchDbEntity)
        }
    }

    void mergeReplace(CouchDbEntity couchDbEntity) {
        JSONObject jsonToMergeInto = couchDbEntity.json
        if (!get(couchDbEntity)) {
            try {
                insert(couchDbEntity)
                return
            } catch (HttpResponseException e) {
                if (409 != e.response.status) { //conflict
                    throw e
                }
            }
            require(couchDbEntity)
        }
        JSONObject existingJson = couchDbEntity.json
        if (!jsonToMergeInto) {
            logger.warn "no json to merge for couchdb entity ${couchDbEntity.couchDbName}/${couchDbEntity.id}\""
            return
        }
        jsonToMergeInto.remove('_rev')
        if (!JsonUtil.areEqual(existingJson,jsonToMergeInto)) {
            JsonUtil.mergeMapsNoReplace(jsonToMergeInto,existingJson)
            couchDbEntity.setJson(jsonToMergeInto) //existing json has been merged with jsonToMerge
            update(couchDbEntity)
        }
    }


    void getOrCreate(CouchDbEntity couchDbEntity) {
        if (get(couchDbEntity)) {
            return
        }
        Throwable savedException = null
        try {
            insert(couchDbEntity)
        } catch (HttpResponseException t) {
            if (409 != t.response.status) { //conflict
                throw t
            }
        }
        require(couchDbEntity)
    }


    private boolean getOrRequire(CouchDbEntity couchDbEntity, boolean doRequire, boolean doLog = false) {
        if (couchDbEntity instanceof CouchDbEntityBeforeAfter) {
            couchDbEntity.beforeGet()
        }

        String name = couchDbEntity.couchDbName
        String id = couchDbEntity.id
        if (!id) {
            throw new RuntimeException("null ID")
        }
        if (!name) {
            throw new RuntimeException("null couchdb name")
        }

        JSONObject json = doRequire ? require(name,id,doLog) : get(name,id,doLog)
        if (json) {
            couchDbEntity.revision = json.'_rev'
            couchDbEntity.json = json
        }

        boolean success = (json != null)
        if (success && couchDbEntity instanceof CouchDbEntityBeforeAfter) {
            couchDbEntity.afterGet()
        }
        return success
    }


    boolean get(CouchDbEntity couchDbEntity) {
        return getOrRequire(couchDbEntity,false)
    }


    boolean require(CouchDbEntity couchDbEntity) {
        return getOrRequire(couchDbEntity,true)
    }


    JSONObject get(String couchDbName, String id, boolean doLog = false) {
        JSONObject couchDbRecord = null
        try {
            couchDbRecord = require(couchDbName,id,doLog)
        } catch (HttpResponseException e) {
            //logger.error "error on GET ${couchDbName}/${id}", GrailsUtil.sanitizeRootCause(e)
            if (404 != e.response.status) {
                throw e
            }
        }
        return couchDbRecord
    }


    JSONObject require(String couchDbName, String id, boolean doLog = false) {
        if (getDisabled()) {
            return null
        }
        String url = couchDbUrl(couchDbName)
        HTTPBuilder http = new HTTPBuilder(url)
        JSONObject jsonObj = null
        if (doLog) {
            logger.info "GET ${url}${id}"
        }
        http.request( Method.GET, ContentType.JSON ) { req ->
            uri.path = id
            response.success = { HttpResponseDecorator resp, Map respJson ->
                jsonObj = JsonUtil.toJsonObject(respJson)
                if (doLog) {
                    logger.info "GET ${url}${id}:\n${jsonObj?.toString(2)}"
                }
            }
        }
        return jsonObj
    }


    JSONObject delete(String couchDbName, String id, String revision = null) {
        if (getDisabled()) {
            return null
        }
        JSONObject jsonResponse = null
        if (id) {
            String url = couchDbUrl(couchDbName)
            Map params = [rev:revision]
            if (!revision) {
                params['rev'] = get(couchDbName,id)?.'_rev'
            }
            HTTPBuilder http = new HTTPBuilder(url)
            try {
                http.request(Method.DELETE) { req ->
                    uri.path = id
                    uri.query = params
                    response.success = { HttpResponseDecorator resp, Map respJson ->
                        jsonResponse = JsonUtil.toJsonObject(respJson)
                    }
                }
            } catch (HttpResponseException e) {
                if (404 != e.response.status) {
                    throw e
                }
            }
        }
        return jsonResponse
    }


    JSONObject update(String couchDbName, JSONObject json) {
        if (getDisabled()) {
            return null
        }
        if (json == null) {
            throw new RuntimeException('null JSON for update')
        }
        String url = couchDbUrl(couchDbName)
        HTTPBuilder http = new HTTPBuilder(url)
        String id = json?.'_id'
        String rev = json?.'_rev'
        if (JsonUtil.isNull(id)) {
            logger.error "bad JSON:\n${json.toString(2)}"
            throw new RuntimeException("null ID")
        }
        if (JsonUtil.isNull(rev)) {
            throw new RuntimeException("null revision")
        }

        logger.debug "PUT for update ${url}${id}:\n${json.toString(2)}"
        JSONObject jsonResponse = null
        http.request( Method.PUT, ContentType.JSON ) { req ->
            uri.path = id
            body = json.toString()
            response.success = { HttpResponseDecorator resp, Map respJson ->
                jsonResponse = JsonUtil.toJsonObject(respJson)
                json.'_rev' = jsonResponse.'rev'
            }
        }
        return jsonResponse
    }

    JSONObject insertOrForceUpdate(String couchDbName, JSONObject json, String id, Integer maxAttempts = 4) {
        try {
            JSONObject existingJson = get(couchDbName, id)
            if (!existingJson) {
                logger.warn "insert or force update: inserting ${couchDbName}/${id}"
                return insert(couchDbName,json,id)
            }
            logger.warn "insert or force update: updating ${couchDbName}/${id}"
            json?.'_id' = id
            json?.'_rev' = existingJson.'_rev'
            return update(couchDbName, json)
        } catch (HttpResponseException t) {
            if (409 == t.response.status && maxAttempts && maxAttempts > 0) { //conflict
                logger.debug "error on insert or force update of ${couchDbName}/${id}:\n${json.toString(2)}"
                logger.debug "error on insert or force update (${maxAttempts} attempts remaining):", GrailsUtil.sanitizeRootCause(t)
                return insertOrForceUpdate(couchDbName, json, id, --maxAttempts)
            } else {
                throw t
            }
        }
    }

    JSONObject insert(String couchDbName, JSONObject json = null) {
        String couchDbId = null
        insert(couchDbName, json, couchDbId)
    }

    JSONObject insert(String couchDbName, JSONObject json, String id) {
        if (getDisabled()) {
            return null
        }
        String url = couchDbUrl(couchDbName)
        logger.warn "insert into ${couchDbName} at URL ${url}${id}"
        HTTPBuilder http = new HTTPBuilder(url)
        if (json == null) {
            json = new JSONObject()
        } else {
            json.remove('_rev')
            json.remove('_id')
        }
        //logger.debug "PUT for insert ${url}${id}:\n${json?.toString(2)}"
        JSONObject jsonResponse = null
        http.request( Method.PUT, ContentType.JSON ) { req ->
            uri.path = id
            body = json.toString()
            response.success = { HttpResponseDecorator resp, Map respJson ->
                jsonResponse = JsonUtil.toJsonObject(respJson)
            }
        }
        return jsonResponse
    }
}
