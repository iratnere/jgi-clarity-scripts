package gov.doe.jgi.pi.pps.util.json

import org.grails.web.json.JSONObject

class JsonConverter {

	static JSONObject stackTraceElementToJson(StackTraceElement element) {
		JSONObject json = new JSONObject()
		json['class-name'] = element.getClassName()
		json['file-name'] = element.getFileName()
		json['line-number'] = Integer.valueOf(element.getLineNumber())
		json['method-name'] = element.getMethodName()
		json['element'] = element.toString()
		return json
	}


}
