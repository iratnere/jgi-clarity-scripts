package gov.doe.jgi.pi.pps.couchdb.util

import gov.doe.jgi.pi.pps.util.json.JsonUtil
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by dscott on 4/10/2015.
 */
class CouchDbEntityUtil {

    static Logger logger = LoggerFactory.getLogger(CouchDbEntityUtil.class.name)


    static void populateWithTestData(CouchDbEntity entity, Object propertiesToExclude = CouchDbEntityImpl.metaClass) {
        Set<String> excludedProperties = JsonUtil.extractPropertiesToExclude(propertiesToExclude)
        GroovyObject gObj = (GroovyObject) entity
        gObj.metaClass.properties.each { MetaProperty prop ->
            if (excludedProperties.contains(prop.name)) {
                return
            }
            Object value = null
            try {
                value = gObj.getProperty(prop.name)
            } catch (ignore) {
                //property not accessible
                logger.warn "property ${prop.name} not accessible"
                return
            }
            if (entity.is(value)) {
                return
            }
            if (value instanceof CouchDbEntity) {
                logger.debug "populating ${prop.name} of class ${value.class.simpleName}"
                populateWithTestData(value,propertiesToExclude)
            }
            if (value == null) {
                switch(prop.type) {
                    case String:
                        value = UUID.randomUUID().toString()
                        break
                    case Date:
                        value = new Date()
                        break
                    case Number:
                        value = 42
                        break
                    case Boolean:
                        value = Boolean.TRUE
                        break
                    //case CouchDbEntity:
                        //value = prop.type.newInstance()
                        //populateWithTestData((CouchDbEntity) value,propertiesToExclude)
                        //break
                    default:
                        logger.warn "no handler for class ${prop.type} of property ${prop.name}"
                        //throw new RuntimeException("no handler for class ${prop.type} of property ${prop.name}")
                }
                gObj.setProperty(prop.name,value)
            }
            if (value instanceof Map) {
                if (!value) {
                    //throw new RuntimeException("map ${prop.name} is not populated")
                }
            }
            if (value instanceof Collection) {
                if (!value) {
                    //throw new RuntimeException("list ${prop.name} is not populated")
                }
            }
        }
    }

    static boolean verifyAllPropertiesSet(Object obj, Object propertiesToExclude = null) {
        GroovyObject gObj = (GroovyObject) obj
        Set<String> excludedProperties = JsonUtil.extractPropertiesToExclude(propertiesToExclude)
        boolean allSet = true
        gObj.metaClass.properties.each { MetaProperty prop ->
            if (excludedProperties.contains(prop.name)) {
                return
            }
            Object value = null
            try {
                value = gObj.getProperty(prop.name)
            } catch (ignore) {
                //property not accessible
                logger.warn "property ${prop.name} not accessible"
                return
            }
            if (obj.is(value)) {
                return
            }
            if (value == null) {
                logger.error "property ${prop.name} not set for ${obj} of class ${obj.class}"
                allSet = false
                return
            }
            if (value instanceof CouchDbEntity) {
                if (!verifyAllPropertiesSet(value,propertiesToExclude)) {
                    allSet = false
                }
            }
        }
        return allSet
    }


    static boolean testCopy(CouchDbEntity entity, propertiesToExclude = null) {
        populateWithTestData(entity,propertiesToExclude)
        JSONObject startJson = entity.getJson()
        logger.info "json of object to copy:\n${startJson.toString(2)}"

        CouchDbEntity copyOfEntity = copy(entity)
        JSONObject copyJson = copyOfEntity.getJson()
        logger.info "json of copied object:\n${startJson.toString(2)}"

        /*
        if (verifyPropertiesSet) {
            if (!verifyAllPropertiesSet(entity, propertiesToExclude)) {
                //this shouldn't happen if utility code working properly
                logger.error "not all properties set on entity of class ${entity.class}"
                return false
            }
            if (!verifyAllPropertiesSet(copyOfEntity, propertiesToExclude)) {
                logger.error "not all properties set on entity copied from entity of class ${entity.class}"
                return false
            }
        }
        */
        JsonUtil.areEqual(startJson,copyJson)
    }

    private static void copy(CouchDbEntity entityToCopy, CouchDbEntity copyEntity) {
        copyEntity.id = entityToCopy.id
        copyEntity.revision = copyEntity.revision
        copyEntity.setJson(entityToCopy.getJson())
    }

    private static CouchDbEntity copy(CouchDbEntity entityToCopy) {
        CouchDbEntity copyEntity = entityToCopy.class.newInstance()
        copy(entityToCopy,copyEntity)
        return copyEntity
    }


    /*
    static void populate(Object obj, JSONObject json, Object propertiesToScreenOut = null) {
        GroovyObject gObj = (GroovyObject) obj
        Set<String> propertiesToExclude = extractPropertiesToExclude(propertiesToScreenOut)
        gObj.metaClass.properties.each { MetaProperty prop ->
            if (!propertiesToExclude.contains(prop.name)) {
                String jsonName = camelCaseToDash(prop.name)
                Object jsonVal = json[jsonName]
                if (isNull(jsonVal)) {
                    gObj.setProperty(prop.name, null)
                    return
                }
                if (jsonVal instanceof JSONObject) {
                    Object newVal = prop.type.newInstance()
                    populate(newVal,jsonVal,propertiesToExclude)
                    gObj.setProperty(prop.name, newVal)
                }
                switch(prop.type) {
                    case Date:
                        gObj.setProperty(prop.name, DateUtil.parseDate(jsonVal))
                        break
                    default:
                        if (prop.type.isAssignableFrom(jsonVal.class)) {
                            gObj.setProperty(prop.name, jsonVal)
                        }
                }
            }
        }
    }
    */

}
