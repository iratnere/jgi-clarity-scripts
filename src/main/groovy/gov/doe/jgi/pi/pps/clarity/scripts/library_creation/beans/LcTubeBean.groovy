package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreation
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationTubePacBio

class LcTubeBean extends TubeBean{

    LibraryCreation lcAdapter
    Long sampleId

    @Override
    String validateTubeBean(){
        def errorMsg = StringBuilder.newInstance()
        if (lcAdapter instanceof LibraryCreationTubePacBio)
            return ''
        //continue validate Illumina libraries
        if (!libraryIndexName) {
            errorMsg << "unspecified Index Name $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!libraryMolarityQc) {
            errorMsg << "unspecified Library Molarity QC (pm) $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!libraryActualInsertSize && libraryCreationQueue?.contains(LMP_QUEUE)) {
            errorMsg << "unspecified Library Actual Insert Size (kb). The field is required for LMP libraries. $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (numberPcrCycles == null || numberPcrCycles < 0) {
            errorMsg << "unspecified Number of PCR Cycles $ClarityProcess.WINDOWS_NEWLINE"
        }
        errorMsg
    }

    @Override
	void populateBeanRequiredFields(){
		libraryMolarityQc = 10
		numberPcrCycles = 1
	}
}