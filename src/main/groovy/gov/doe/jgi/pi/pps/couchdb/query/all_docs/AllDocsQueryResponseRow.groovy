package gov.doe.jgi.pi.pps.couchdb.query.all_docs

import org.grails.web.json.JSONObject

/**
 * Created by duncanscott on 6/9/15.
 */
class AllDocsQueryResponseRow {

    /*
            {
            "id": "a4c51cdfa2069f3e905c431114001aff",
            "key": "a4c51cdfa2069f3e905c431114001aff",
            "value": {
                "rev": "1-967a00dff5e02add41819138abb3284d"
            }
        },
     */
    JSONObject json

    AllDocsQueryResponseRow(JSONObject json) {
        this.json = json
    }

    String getKey() {
        json?.'key'
    }

    String getId() {
        json?.'id'
    }

    JSONObject getValue() {
        json?.'value'
    }
    String getRev() {
        value?.'rev'
    }
}
