package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class LcEchoTableBean {
	@FieldMapping(header = 'Source Plate Name', cellType = CellTypeEnum.STRING)
	public String sourcePlateName;
	@FieldMapping(header = 'Source Plate Barcode', cellType = CellTypeEnum.STRING)
	public String sourcePlateBarcode;
	@FieldMapping(header='Source Row', cellType = CellTypeEnum.NUMBER)
	public BigInteger sourceRow
	@FieldMapping(header='Source Column', cellType = CellTypeEnum.NUMBER)
	public BigInteger sourceColumn
	@FieldMapping(header = 'Destination Plate Name', cellType = CellTypeEnum.STRING)
	public String destinationPlateName
	@FieldMapping(header = 'Destination Plate Barcode', cellType = CellTypeEnum.STRING)
	public String destinationPlateBarcode
	@FieldMapping(header='Destination Row', cellType = CellTypeEnum.NUMBER)
	public BigInteger destinationRow
	@FieldMapping(header='Destination Column', cellType = CellTypeEnum.NUMBER)
	public BigInteger destinationColumn
	@FieldMapping(header='Transfer Volume', cellType = CellTypeEnum.NUMBER) //units nL
	public BigDecimal transferVolumeNl

	void populateRequiredFields(ClarityProcess process, int passCount){}

	boolean getRequiresIndex(){
		return false
	}

	void setIndexName(String indexName){
		return
	}
}
