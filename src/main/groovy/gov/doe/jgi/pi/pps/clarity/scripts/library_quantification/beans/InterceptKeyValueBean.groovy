package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping

/**
 * Created by datjandra on 1/8/2016.
 */
class InterceptKeyValueBean {

    @FieldMapping(header = 'Intercept:', cellType = CellTypeEnum.NUMBER)
    public BigDecimal intercept

    int populateRequiredFields(int platePassPercentage){
        intercept = 1
        platePassPercentage
    }
}
