package gov.doe.jgi.pi.pps.util.exception

interface HttpStatusCodeReporter {
	Integer getHttpStatusCode()
}
