package gov.doe.jgi.pi.pps.clarity.util

class LabelBean {

    String pos_1_1 = ''
    String pos_1_2 = ''
    String pos_1_3 = ''
    String pos_1_4 = ''
    String pos_1_5 = ''
    String pos_1_6 = ''
    String pos_2_1 = ''
    String pos_2_2 = ''

    @Override
    String toString() {
        return "$pos_1_1,$pos_1_2,$pos_1_3,$pos_1_4,$pos_1_5,$pos_1_6,$pos_2_1,$pos_2_2"

    }
}