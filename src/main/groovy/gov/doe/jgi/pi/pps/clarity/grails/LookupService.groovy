package gov.doe.jgi.pi.pps.clarity.grails

import gov.doe.jgi.pi.pps.clarity.cv.IdTypeCv
import gov.doe.jgi.pi.pps.clarity.dto.*
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.ArtifactIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.ContainerIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.ProjectIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.SampleIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.*
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import grails.gorm.transactions.Transactional
import grails.util.GrailsUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Transactional
class LookupService {

    static Logger logger = LoggerFactory.getLogger(LookupService.class.name)

    List<String> getProjectIdTypes() {
        List<String> types = []
        types << IdTypeCv.PROJECT_LIMSID.value
        types << IdTypeCv.PROJECT_NAME.value
        types
    }

    List<String> getProcessIdTypes() {
        List<String> types = []
        types << IdTypeCv.PROCESS_LIMSID.value
        types
    }

    List<String> getProjectAndProcessIdTypes() {
        List<String> types = projectIdTypes
        types.addAll(processIdTypes)
        types
    }

    List<String> getArtifactSampleContainerIdTypes() {
        List<String> types = []
        types << IdTypeCv.ARTIFACT_LIMSID.value
        types << IdTypeCv.SAMPLE_LIMSID.value
        types << IdTypeCv.CONTAINER_LIMSID.value
        types << IdTypeCv.CONTAINER_BARCODE.value
        types << IdTypeCv.ARTIFACT_NAME.value
        types << IdTypeCv.SAMPLE_NAME.value
        types << IdTypeCv.UNDETERMINED.value
        types
    }


    Map<String,List<ArtifactDto>> fetchArtifactsByName(String artifactNamesText, boolean errorsForBadLibraryNames = true) {
        fetchArtifactsByName(artifactNamesText?.split(/\s+/)?.toList(), errorsForBadLibraryNames)
    }

    Map<String,List<ArtifactDto>> fetchArtifactsByName(Collection<String> artifactNames, boolean errorsForBadLibraryNames = true, boolean loadSamples = true) {
        Map<String,List<ArtifactDto>> artifactMap = [:]
        artifactNames?.each { String artifactName ->
            if (artifactName && !artifactMap.containsKey(artifactName)) {
                artifactMap[artifactName] = []
            }
        }
        List<ArtifactNode> artifactNodes = lookupArtifactNodesForArtifactNames(artifactMap.keySet(), errorsForBadLibraryNames)
        if (artifactNodes) {
            ClarityWebTransaction clarityWebTransaction = ClarityWebTransaction.requireCurrentTransaction()
            NodeManager nodeManager = clarityWebTransaction.requireNodeManager()
            DtoFactory generator = new DtoFactory(nodeManager)
            artifactNodes.each { ArtifactNode artifactNode ->
                artifactMap[artifactNode.name] << generator.artifactNodeToArtifactDto(artifactNode, loadSamples)
            }
        }
        artifactMap
    }

    List<ArtifactDto> fetchClarityArtifacts(Collection<String> limsids, idType) {
        IdTypeCv idTypeCv = IdTypeCv.toEnum(idType)
        List<ArtifactDto> artifactDtos = []
        lookupClarityEntitiesReturnContainerDtos(limsids, idTypeCv)?.sort()?.each { ContainerDto containerDto ->
            artifactDtos += containerDto.artifacts
        }
        artifactDtos
    }

    List<GeneusNodeDto> fetchProjectsAndProcesses(Collection<String> limsids, IdTypeCv idTypeCv) {
        lookupProjectAndProcessDtos(limsids, idTypeCv)?.sort()
    }

    List<ContainerDto> fetchClarityContainers(Collection<String> limsids, idType) {
        IdTypeCv idTypeCv = IdTypeCv.toEnum(idType)
        lookupClarityEntitiesReturnContainerDtos(limsids, idTypeCv)?.sort()
    }

    private List<ContainerNode> lookupContainerNodesForBarcodes(Collection<String> containerBarcodes, boolean reportBadBarcodes = true) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = webTransaction.requireNodeManager()
        List<String> containerIds = []
        boolean badBarcode = false
        containerBarcodes?.each { String barcode ->
            List<ContainerParameterValue> parameterValues = []
            parameterValues << ContainerParameter.NAME.setValue(barcode)
            ContainerIdIterator containerIdIterator = new ContainerIdIterator(nodeManager.nodeConfig, parameterValues)
            if (containerIdIterator.hasNext()) {
                containerIds += containerIdIterator.findAll()
            } else {
                badBarcode = true
                if (reportBadBarcodes) {
                    webTransaction.addErrorMessage("invalid container barcode [${barcode}]")
                }
            }
        }
        if (badBarcode && reportBadBarcodes) {
            webTransaction.statusCode = 422
            return null
        }
        return lookupContainerNodesForContainerIds(containerIds)
    }


    private List<ContainerNode> lookupContainerNodesForContainerIds(Collection<String> containerIds) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = webTransaction.requireNodeManager()
        List<ContainerNode> containerNodes = null
        try {
            containerNodes = nodeManager.getContainerNodes(containerIds)
        } catch (t) {
            logger.error "error looking up containers", GrailsUtil.sanitizeRootCause(t)
        }
        if (!containerNodes || containerNodes.size() < containerIds.size()) {
            findBadContainerIds(containerIds,webTransaction,nodeManager)
        }
        return containerNodes
    }


    private List<SampleNode> lookupSampleNodesForSampleIds(Collection<String> sampleIds) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = webTransaction.requireNodeManager()
        List<SampleNode> sampleNodes = null
        try {
            return nodeManager.getSampleNodes(sampleIds)
        } catch (t) {
            logger.error "error looking up sample nodes", GrailsUtil.sanitizeRootCause(t)
        }
        if (!sampleNodes || sampleNodes.size() < sampleIds.size()) {
            findBadSampleIds(sampleIds,webTransaction,nodeManager)
        }
        return sampleNodes
    }

    private List<SampleNode> lookupSampleNodesForSampleNames(Collection<String> sampleNames, boolean reportBadSampleNames = true) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = webTransaction.requireNodeManager()
        List<String> sampleIds = []
        boolean badSampleName = false
        sampleNames?.each { String sampleName ->
            List<SampleParameterValue> parameterValues = []
            parameterValues << SampleParameter.NAME.setValue(sampleName)
            SampleIdIterator sampleIdIterator = new SampleIdIterator(nodeManager.nodeConfig, parameterValues)
            if (sampleIdIterator.hasNext()) {
                sampleIds += sampleIdIterator.findAll()
            } else {
                badSampleName = true
                if (reportBadSampleNames) {
                    webTransaction.addErrorMessage("invalid sample name [${sampleName}]")
                }
            }
        }
        if (badSampleName && reportBadSampleNames) {
            webTransaction.statusCode = 422
            return null
        }
        return lookupSampleNodesForSampleIds(sampleIds)
    }

    private static List<ArtifactNode> lookupArtifactNodesForArtifactNames(Collection<String> artifactNames, boolean badArtifactNames = true) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = webTransaction.requireNodeManager()
        List<String> artifactIds = []
        boolean badArtifactName = false
        artifactNames?.each { String artifactName ->
            List<ArtifactParameterValue> parameterValues = []
            parameterValues << ArtifactParameter.NAME.setValue(artifactName)
            ArtifactIdIterator artifactIdIterator = new ArtifactIdIterator(nodeManager.nodeConfig, parameterValues)
            if (artifactIdIterator.hasNext()) {
                artifactIds += artifactIdIterator.findAll()
            } else {
                badArtifactName = true
                if (badArtifactNames) {
                    webTransaction.addErrorMessage("invalid artifact name [${artifactName}]")
                }
            }
        }
        if (badArtifactName && badArtifactNames) {
            webTransaction.statusCode = 422
            return null
        }
        return lookupArtifactNodesForArtifactIds(artifactIds)
    }

    static List<ArtifactNode> lookupArtifactNodesForArtifactIds(Collection<String> artifactIds) {
        if (!artifactIds) {
            return null
        }
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = webTransaction.requireNodeManager()
        List<ArtifactNode> artifactNodes = null
        try {
            artifactNodes = nodeManager.getArtifactNodes(artifactIds)
        } catch (t) {
            logger.error "error looking up artifact nodes", GrailsUtil.sanitizeRootCause(t)
        }
        if (!artifactNodes || artifactNodes.size() < artifactIds.size()) {
            findBadArtifactIds(artifactIds,webTransaction,nodeManager)
        }
        return artifactNodes
    }

    private static void findBadContainerIds(Collection<String> containerIds, ClarityWebTransaction webTransaction, NodeManager nodeManager) {
        Set<String> uniqueIds = []
        containerIds.each { String containerId ->
            if (uniqueIds.contains(containerId)) {
                return
            }
            uniqueIds << containerId
            try {
                nodeManager.getContainerNode(containerId)
            } catch (ignore) {
                webTransaction.addErrorMessage("invalid container LIMSID [${containerId}]")
                webTransaction.statusCode = 422
            }
        }
    }

    private static void findBadSampleIds(Collection<String> sampleIds, ClarityWebTransaction webTransaction, NodeManager nodeManager) {
        Set<String> uniqueIds = []
        sampleIds.each { String sampleId ->
            if (uniqueIds.contains(sampleId)) {
                return
            }
            uniqueIds << sampleId
            try {
                nodeManager.getSampleNode(sampleId)
            } catch (ignore) {
                webTransaction.addErrorMessage("invalid sample LIMSID [${sampleId}]")
                webTransaction.statusCode = 422
            }
        }
    }

    static void findBadArtifactIds(Collection<String> artifactIds, ClarityWebTransaction webTransaction, NodeManager nodeManager) {
        Set<String> uniqueIds = []
        artifactIds?.each { String artifactId ->
            if (!artifactId || uniqueIds.contains(artifactId)) {
                return
            }
            uniqueIds << artifactId
            try {
                nodeManager.getArtifactNode(artifactId)
            } catch (ignore) {
                webTransaction.addErrorMessage("invalid artifact LIMSID [${artifactId}]")
                webTransaction.statusCode = 422
            }
        }
    }

    private static Set<ProcessNode> lookupProjectNodesForProcessIds(Set<String> processIds) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        if (webTransaction.errorMessages) {
            webTransaction.exit(500)
        }
        NodeManager nodeManager = webTransaction.requireNodeManager()
        Set<ProcessNode> processNodes = []
        processIds?.each { String processId ->
            try {
                processNodes << nodeManager.getProcessNode(processId)
            } catch (t) {
                webTransaction.addErrorMessage([code: 'process.notFound.errorMessage', args:[processId,t.message ?: t]]);
            }
        }
        if (webTransaction.errorMessages) {
            webTransaction.exit(422)
        }
        processNodes
    }

    private static Set<ProjectNode> lookupProjectNodesForProjectIds(Set<String> projectIds) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        if (webTransaction.errorMessages) {
            webTransaction.exit(500)
        }
        NodeManager nodeManager = webTransaction.requireNodeManager()
        Set<ProjectNode> projectNodes = []
        projectIds?.each { String projectId ->
            try {
                projectNodes << nodeManager.getProjectNode(projectId)
            } catch (t) {
                logger.error "error looking up project node", GrailsUtil.sanitizeRootCause(t)
                webTransaction.addErrorMessage([code: 'project.notFound.errorMessage', args:[projectId,t.message ?: t]]);
            }
        }
        if (webTransaction.errorMessages) {
            webTransaction.exit(422)
        }
        projectNodes
    }

    private static Set<ProjectNode> lookupProjectNodesForProjectNames(Set<String> projectNames) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        if (webTransaction.errorMessages) {
            webTransaction.exit(500)
        }
        NodeManager nodeManager = webTransaction.requireNodeManager()
        Set<ProjectNode> projectNodes = []
        projectNames?.each { String projectName ->
            ProjectIdIterator projectIdIterator = new ProjectIdIterator(nodeManager.nodeConfig, [ProjectParameter.NAME.setValue(projectName)])
            if (projectIdIterator.hasNext()) {
                while (projectIdIterator.hasNext()) {
                    projectNodes << nodeManager.getProjectNode(projectIdIterator.next())
                }
            } else {
                webTransaction.addErrorMessage([code: 'project.notFound.forPmoProjectId', args:[projectName]]);
            }
        }
        if (webTransaction.errorMessages) {
            webTransaction.exit(422)
        }
        projectNodes
    }

    Set<GeneusNodeDto> lookupProjectAndProcessDtos(Collection<String> limsids, IdTypeCv idTypeCv) {
        Set<GeneusNodeDto> dtos = []
        if (!limsids) {
            return dtos
        }
        Set<String> uniqueIds = []
        limsids.findAll()*.trim().each {
            uniqueIds << it
        }
        if (!uniqueIds) {
            return dtos
        }
        ClarityWebTransaction clarityWebTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = clarityWebTransaction.requireNodeManager()
        DtoFactory generator = new DtoFactory(nodeManager)
        clarityWebTransaction.statusCode = 200

        switch (idTypeCv) {
            case IdTypeCv.PROJECT_LIMSID:
                lookupProjectNodesForProjectIds(uniqueIds).each { ProjectNode projectNode ->
                    ProjectDto projectDto = generator.projectNodeToProjectDto(projectNode)
                    projectDto.limsidSelected = true
                    dtos << projectDto
                }
                break;
            case IdTypeCv.PROJECT_NAME:
                lookupProjectNodesForProjectNames(uniqueIds).each { ProjectNode projectNode ->
                    ProjectDto projectDto = generator.projectNodeToProjectDto(projectNode)
                    projectDto.nameSelected = true
                    dtos << projectDto
                }
                break;
            case IdTypeCv.PROCESS_LIMSID:
                lookupProjectNodesForProcessIds(uniqueIds).each { ProcessNode processNode ->
                    ProcessDto processDto = generator.processNodeToProcessDto(processNode)
                    processDto.limsidSelected = true
                    dtos << processDto
                }
                break;
            case null:
                clarityWebTransaction.exitWithErrorMessage("Please select an ID type.", 422)
                break;
            default:
                clarityWebTransaction.exitWithErrorMessage("no handler for ID type [${idTypeCv}]", 422)
        }
        dtos
    }

    Set<ContainerDto> lookupClarityEntitiesReturnContainerDtos(Collection<String> limsids, IdTypeCv idTypeCv) {
        Set<String> uniqueIds = []
        limsids?.findAll()*.trim().each {
            uniqueIds << it
        }
        if (!uniqueIds) {
            return null
        }
        ClarityWebTransaction clarityWebTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = clarityWebTransaction.requireNodeManager()
        DtoFactory generator = new DtoFactory(nodeManager)
        clarityWebTransaction.statusCode = 200
        Set<ContainerDto> containerDtos = []
        switch (idTypeCv) {
            case IdTypeCv.SAMPLE_LIMSID:
                lookupSampleNodesForSampleIds(uniqueIds)?.each { SampleNode sampleNode ->
                    SampleDto sampleDto = generator.sampleNodeToSampleDto(sampleNode)
                    sampleDto.limsidSelected = true
                    containerDtos << sampleDto.artifact.container
                }
                break;
            case IdTypeCv.SAMPLE_NAME:
                lookupSampleNodesForSampleNames(uniqueIds)?.each { SampleNode sampleNode ->
                    SampleDto sampleDto = generator.sampleNodeToSampleDto(sampleNode)
                    sampleDto.limsidSelected = true
                    containerDtos << sampleDto.artifact.container
                }
                break;
            case IdTypeCv.ARTIFACT_LIMSID:
                lookupArtifactNodesForArtifactIds(uniqueIds)?.each { ArtifactNode artifactNode ->
                    logger.debug "generating DTO for artifact ID [${artifactNode.id}]"
                    ArtifactDto artifactDto = generator.artifactNodeToArtifactDto(artifactNode)
                    artifactDto.limsidSelected = true
                    containerDtos << artifactDto.container
                }
                break;
            case IdTypeCv.ARTIFACT_NAME:
                lookupArtifactNodesForArtifactNames(uniqueIds)?.each { ArtifactNode artifactNode ->
                    logger.debug "generating DTO for artifact ID [${artifactNode.id}]"
                    ArtifactDto artifactDto = generator.artifactNodeToArtifactDto(artifactNode)
                    artifactDto.limsidSelected = true
                    containerDtos << artifactDto.container
                }
                break;
            case IdTypeCv.CONTAINER_LIMSID:
                lookupContainerNodesForContainerIds(uniqueIds)?.each { ContainerNode containerNode ->
                    ContainerDto containerDto = generator.containerNodeToContainerDto(containerNode)
                    containerDto.limsidSelected = true
                    containerDtos << containerDto
                }
                break;
            case IdTypeCv.CONTAINER_BARCODE:
                    lookupContainerNodesForBarcodes(uniqueIds)?.each { ContainerNode containerNode ->
                        ContainerDto containerDto = generator.containerNodeToContainerDto(containerNode)
                    containerDto.nameSelected = true
                    containerDtos << containerDto
                }
                break;
            case IdTypeCv.UNDETERMINED:
                lookupClarityEntitiesForGenericIdsReturnContainerDtos(uniqueIds,generator)?.each { ContainerDto containerDto ->
                    containerDtos << containerDto
                }
                break;
            case null:
                clarityWebTransaction.exitWithErrorMessage("Please select an ID type.", 422)
                break;
            default:
                clarityWebTransaction.exitWithErrorMessage("no handler for ID type [${idTypeCv}]", 422)
        }
        generator.close()
        containerDtos
    }

    private Set<ContainerDto> lookupClarityEntitiesForGenericIdsReturnContainerDtos(Collection<String> limsids, DtoFactory generator) {
        if (!limsids) {
            return null
        }
        Set<ContainerDto> containerDtos = []
        ClarityWebTransaction clarityWebTransaction = ClarityWebTransaction.requireCurrentTransaction()
        NodeManager nodeManager = clarityWebTransaction.requireNodeManager()
        Set<String> uniqueIds = []
        limsids.each { String limsid ->
            if (!limsid) {
                return
            }
            if (uniqueIds.contains(limsid)) {
                return
            }
            uniqueIds << limsid
            ContainerDto containerDto = generator.containerIdToContainerDto(limsid)
            if (containerDto) {
                containerDto.limsidSelected = true
                containerDtos << containerDto
            } else {
                ArtifactDto artifactDto = generator.artifactIdToArtifactDto(limsid)
                if (artifactDto) {
                    artifactDto.limsidSelected = true
                    containerDtos << artifactDto.container
                } else {
                    SampleDto sampleDto = generator.sampleIdToSampleDto(limsid)
                    if (sampleDto) {
                        sampleDto.limsidSelected = true
                        containerDtos << sampleDto.artifact.container
                    } else {
                        List<ContainerNode> containerNodes = lookupContainerNodesForBarcodes([limsid],false)
                        if (containerNodes) {
                            containerNodes.each { ContainerNode containerNode ->
                                ContainerDto barcodeContainer = generator.containerNodeToContainerDto(containerNode)
                                barcodeContainer.nameSelected = true
                                containerDtos << barcodeContainer
                            }
                        } else {
                            List<SampleNode> sampleNodes = lookupSampleNodesForSampleNames([limsid],false)
                            if (sampleNodes) {
                                sampleNodes.each { SampleNode sampleNode ->
                                    sampleDto = generator.sampleNodeToSampleDto(sampleNode)
                                    sampleDto.nameSelected = true
                                    containerDtos << sampleDto.artifact.container
                                }
                            } else {
                                List<ArtifactNode> artifactNodes = lookupArtifactNodesForArtifactNames([limsid], false)
                                if (artifactNodes) {
                                    artifactNodes.each { ArtifactNode artifactNode ->
                                        artifactDto = generator.artifactNodeToArtifactDto(artifactNode)
                                        artifactDto.nameSelected = true
                                        containerDtos << artifactDto.container
                                    }
                                } else {
                                    clarityWebTransaction.addErrorMessage("nothing found for ID [${limsid}]")
                                    clarityWebTransaction.statusCode = 422
                                }
                            }
                        }
                    }
                }
            }
        }
        generator.close()
        containerDtos
    }


}
