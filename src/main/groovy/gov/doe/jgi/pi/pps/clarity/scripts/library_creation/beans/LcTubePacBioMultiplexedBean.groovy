package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class LcTubePacBioMultiplexedBean extends TubeBean{

    @Override
    String validateTubeBean() {
        if (!libraryIndexName) {
            return "unspecified Index Name $ClarityProcess.WINDOWS_NEWLINE"
        }
        return ''
    }

    @Override
    void populateBeanRequiredFields() {
        super.populateBeanRequiredFields()
    }

}
