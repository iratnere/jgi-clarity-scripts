package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules

/**
 * Created by datjandra on 7/6/2015.
 */
abstract class Condition {

    abstract boolean evaluate(ObjectWithDynamicAttributes p)

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Condition)) {
            return super.equals(o)
        }
        return (toString().equals(((Condition) o).toString()))
    }

    @Override
    public int hashCode() {
        return toString().hashCode()
    }

    abstract String toString()
}
