package gov.doe.jgi.pi.pps.clarity.util

import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import org.grails.web.json.JSONObject

import java.util.regex.Matcher

/**
 * Created by dscott on 7/31/2015.
 */

/*
<rt:routing xmlns:rt="http://genologics.com/ri/routing">
  <assign stage-uri="http://localhost:8090/api/v2/configuration/workflows/7/stages/4">
    <artifact uri="http://localhost:8090/api/v2/artifacts/5"/>
    <artifact uri="http://localhost:8090/api/v2/artifacts/6"/>
  </assign>
  <assign workflow-uri="http://localhost:8090/api/v2/configuration/workflows/7">
    <artifact uri="http://localhost:8090/api/v2/artifacts/8"/>
    <artifact uri="http://localhost:8090/api/v2/artifacts/9"/>
  </assign>
  <unassign workflow-uri="http://localhost:8090/api/v2/configuration/workflows/10">
    <artifact uri="http://localhost:8090/api/v2/artifacts/11"/>
    <artifact uri="http://localhost:8090/api/v2/artifacts/12"/>
  </unassign>
</rt:routing>
*/

class RoutingRequest {

    Routing.Action action = Routing.Action.assign
    String routingUri
    String artifactId
    String triggeringEventId

    OnDemandCache<Matcher> cachedMatcher = new OnDemandCache<>()

    void setRoutingUri(String uri) {
        if (uri) {
            if (routingUri) {
                if (uri != routingUri) {
                    throw new RuntimeException('routeToUri already set: it can only be set once')
                }
            } else {
                routingUri = uri
            }
        }
    }

    private void verifyRouteToUri() {
        if (!routingUri) {
            throw new RuntimeException('routeToUri not set')
        }
    }

    void validate() {
        matcher()
    }

    private Matcher matcher() {
        verifyRouteToUri()
        cachedMatcher.fetch { String uriToMatch ->
            Matcher matcher = routingUri =~ /(.+\/configuration\/workflows\/)(\d+)(\/stages\/)(\d+)$/
            if (!matcher) {
                matcher = routingUri =~ /(.+\/configuration\/workflows\/)(\d+)$/
                if (!matcher) {
                    throw new RuntimeException("invalid routing URI ${uriToMatch}")
                }
            }
            return matcher
        }
    }


    boolean getIsStage() {
        matcher()[0].size() == 5
    }

    Integer getStageId() {
        if (isStage) {
            return matcher()[0][4] as Integer
        }
        return null
    }

    Integer getWorkflowId() {
        return matcher()[0][2] as Integer
    }

    String getWorkflowUri() {
        Matcher m = matcher()
        "${m[0][1]}${m[0][2]}"
    }

    String getMapKey() {
        verifyRouteToUri()
        if (!action) {
            throw new RuntimeException('action not set')
        }
        "${routingUri}-${action}"
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false
        RoutingRequest that = (RoutingRequest) o
        if (action != that.action) return false
        if (artifactId != that.artifactId) return false
        if (routingUri != that.routingUri) return false
        return true
    }

    int hashCode() {
        int result
        result = action.hashCode()
        result = 31 * result + routingUri.hashCode()
        result = 31 * result + artifactId.hashCode()
        return result
    }

    String toString() {
        "routing-request[artifact:uri:action]${artifactId}:${routingUri}:${action}"
    }

    static List<RoutingRequest> generateRequestsToRouteArtifactIdsToUri(String uri, Collection<String> artifactIds, Routing.Action action = Routing.Action.assign) {
        List<RoutingRequest> requests = []
        artifactIds?.each { String artifactId ->
            requests << new RoutingRequest(routingUri: uri, artifactId: artifactId, action:action)
        }
        if (requests) {
            requests[0].validate()
        }
        requests
    }

    JSONObject toJson() {
        JSONObject json = new JSONObject()
        json.'artifact-id' = artifactId
        json.'routing-uri' = routingUri
        json.'action' = action?.name()
        if (triggeringEventId) {
            json.'triggering-event-id' = triggeringEventId
        }
        json
    }

    static RoutingRequest fromJson(json) {
        RoutingRequest routingRequest = null
        if (json instanceof JSONObject) {
            routingRequest = new RoutingRequest()
            routingRequest.artifactId = json.'artifact-id'
            routingRequest.routingUri = json.'routing-uri'
            routingRequest.triggeringEventId = json.'triggering-event-id'

            if (!routingRequest.artifactId) {
                throw new WebException("artifact-id not defined", 422)
            }
            if (!routingRequest.routingUri) {
                throw new WebException("routing-uri not defined", 422)
            }

            String action = json.'action'
            if (action) {
                if (action.equalsIgnoreCase(Routing.Action.assign.name())) {
                    routingRequest.action = Routing.Action.assign
                } else if (action.equalsIgnoreCase(Routing.Action.unassign.name())) {
                    routingRequest.action = Routing.Action.unassign
                } else {
                    throw new WebException("invalid routing action [${action}]", 422)
                }
            }
        } else if (json instanceof Map) {
            JSONObject jsonObject = JsonUtil.toJsonObject((Map) json)
        } else if (json != null) {
            JSONObject jsonObject = null
            try {
                jsonObject = new JSONObject(json.toString())
            } catch (t) {
                throw new WebException('invalid routing request JSON', 400, t)
            }
            routingRequest = fromJson(jsonObject)
        }
        routingRequest
    }
}
