package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules
/**
 * Created by datjandra on 7/6/2015.
 */
class AnyCondition extends Condition {

    private Object key
    private Object value

    AnyCondition(Object key, Object value) {
        if (key == null) {
            throw new IllegalArgumentException('Condition requires attribute key')
        }

        if (value == null) {
            throw new IllegalArgumentException('Condition requires accepted value')
        }

        this.key = key
        this.value = value
    }

    @Override
    boolean evaluate(ObjectWithDynamicAttributes percept) {
        boolean returnValue = false
        Collection collection = percept.getAttribute(key)
        collection.each { def item ->
            if (item?.equals(value)) {
                returnValue = true
            }
        }
        return returnValue
    }

    @Override
    public String toString() {
        return "$key = $value"
    }
}
