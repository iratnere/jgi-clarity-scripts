package gov.doe.jgi.pi.pps.util.json

//import com.google.gson.FieldNamingPolicy
//import com.google.gson.Gson
//import com.google.gson.GsonBuilder

import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by dscott on 1/23/2015.
 */
class JsonUtil {

    static Logger logger = LoggerFactory.getLogger(JsonUtil.class.name)

    //static final Gson gson = new GsonBuilder().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();

    static boolean isNull(Object obj) {
        return obj == null
    }

    static String camelCaseToDash(String s) {
        return s.replaceAll(/\B[A-Z]/) { '-' + it }.toLowerCase()
    }


    static Set<String> extractPropertiesToExclude(Object obj = null) {
        Set<String> propertiesToExclude = null
        if (obj == null) {
            propertiesToExclude = Object.metaClass.properties*.name.toSet()
        } else if (obj instanceof MetaClass) {
            propertiesToExclude = obj.properties*.name.toSet()
        } else if (obj instanceof Iterable) {
            propertiesToExclude = []
            obj.each { propertyToExclude ->
                propertiesToExclude += extractPropertiesToExclude(propertyToExclude)
            }
        } else if (obj instanceof CharSequence) {
            propertiesToExclude = []
            propertiesToExclude << obj.toString()
        } else {
            propertiesToExclude = []
            propertiesToExclude = extractPropertiesToExclude(obj.metaClass)
        }
        return propertiesToExclude
    }


    static JSONObject groovyObjectToJson(GroovyObject obj, Object propertiesToScreenOut = null) {
        logger.trace "converting groovy object ${obj} to JSON"
        JSONObject jsonObj = new JSONObject()
        if (obj != null) {
            Set<String> propertiesToExclude = extractPropertiesToExclude(propertiesToScreenOut)
            obj.metaClass.properties*.name.each { String name ->
                if (!propertiesToExclude.contains(name)) {
                    String jsonName = camelCaseToDash(name)
                    Object propVal = obj.getProperty(name)
                    if (obj.is(propVal)) {
                        return
                    }
                    logger.trace "converting property ${name} to JSON"
                    Object jsonVal = toJson(propVal)
                    if (jsonVal != null && !((jsonVal instanceof JSONArray && !jsonVal) || (jsonVal instanceof JSONObject && !jsonVal))) {
                        logger.trace "returned value is of class ${jsonVal.class?.name}"
                        jsonObj[jsonName] = jsonVal
                    }
                }
            }
        }
        jsonObj
    }

    static String toString(Object o) {
        if (o instanceof JSONObject) {
            return o.toString(2)
        } else if (o instanceof JSONArray) {
            return o.toString(2)
        } else {
            return "${o}"
        }
    }


    static void mergeMapsNoReplace(Map map, Map mapToMerge, boolean throwErrorOnConflict = false) {
        mapToMerge?.each { key, val ->
            if (map.containsKey(key)) {
                Object existingVal = map[key]
                if (isNull(existingVal)) {
                    map[key] = val
                }  else if (existingVal instanceof Map && val instanceof Map) {
                    mergeMapsNoReplace(existingVal,val)
                } else if (throwErrorOnConflict) {
                    throw new RuntimeException("maps share key ${key}")
                }
            } else {
                map[key] = val
            }
        }
    }

    static Object toJson(Object obj) {
        if (isNull(obj)) {
            return null
        }
        if (obj instanceof Date) {
            return DateUtil.dateToJsonArray((Date) obj)
        }
        if (obj instanceof Map) {
            return toJsonObject(obj)
        }
        if (obj instanceof Iterable) {
            return toJsonArray(obj)
        }
        if (obj instanceof Number) {
            return obj
        }
        if (obj instanceof CharSequence) {
            return obj.toString()
        }
        if (obj instanceof Boolean) {
            return obj.booleanValue()
        }
        if (obj instanceof GroovyObject) {
            return groovyObjectToJson(obj)
        }
        return obj
    }


    static JSONObject toJsonObject(Map obj) {
        if (obj instanceof JSONObject) {
            return obj
        }
        JSONObject json = new JSONObject()
        obj?.each { key, val ->
            json["${key}"] = toJson(val)
        }
        return json
    }


    static JSONArray toJsonArray(Iterable obj) {
        if (obj instanceof JSONArray) {
            return obj
        }
        JSONArray json = new JSONArray()
        obj?.each { val ->
            json << toJson(val)
        }
        return json
    }


    static JSONObject removeNulls(JSONObject json) {
        JSONObject clean = new JSONObject()
        json.each { key,val ->
            if (val instanceof JSONObject) {
                clean[key] = removeNulls(val)
            } else if(!isNull(val)) {
                clean[key] = val
            }
        }
        clean
    }

    static JSONArray replaceListValuesDeep(JSONArray jsonArray, JSONObject replacementValues) {
        JSONArray copy = new JSONArray()
        jsonArray?.each { value ->
            if (value instanceof JSONObject) {
                copy << replaceObjectValuesDeep((JSONObject) value, replacementValues)
            } else if (value instanceof JSONArray) {
                copy << replaceListValuesDeep((JSONArray) value, replacementValues)
            } else if (replacementValues?.containsKey(value)) {
                copy << replacementValues[value]
            } else {
                copy << value
            }
        }
        copy
    }

    static JSONObject replaceObjectValuesDeep(JSONObject jsonObject, JSONObject replacementValues) {
        JSONObject copy = new JSONObject()
        jsonObject?.each { key,value ->
            if (value instanceof JSONObject) {
                copy[key] = replaceObjectValuesDeep((JSONObject) value, replacementValues)
            } else if (value instanceof JSONArray) {
                copy[key] = replaceListValuesDeep((JSONArray) value, replacementValues)
            } else if (replacementValues?.containsKey(value)) {
                copy[key] = replacementValues[value]
            } else {
                copy[key] = value
            }
        }
        copy
    }

    static void removeEmpty(JSONObject json) {
        List nullKeys = []
        json?.each { key,val ->
            if((val instanceof Map || val instanceof List) && !val) {
                nullKeys << key
            }
        }
        nullKeys.each { key ->
            json.remove(key)
        }
    }


}
