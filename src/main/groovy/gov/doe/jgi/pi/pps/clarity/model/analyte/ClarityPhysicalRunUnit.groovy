package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import groovy.transform.PackageScope

/**
 * Created by dscott on 4/8/2014.
 */
class ClarityPhysicalRunUnit extends Analyte {

    public static final String clusterGenFailureMode = 'Operator Error'
    public static final String sequencingFailureMode = 'Instrument'

    @PackageScope
    ClarityPhysicalRunUnit(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
    }

    void setUdfLibraryConversionFactor(def libraryConversionFactor) {
        if (libraryConversionFactor instanceof BigDecimal) {
            artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_CONVERSION_FACTOR.udf, libraryConversionFactor)
        } else {
            artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_CONVERSION_FACTOR.udf,
                    new BigDecimal(libraryConversionFactor))
        }
    }

    BigDecimal getUdfLibraryConversionFactor() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LIBRARY_CONVERSION_FACTOR.udf)
    }

    void setUdfPhixSpikeIn(def phixSpikeIn) {
        if (phixSpikeIn instanceof BigDecimal) {
            artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_PHIX_SPIKE_IN.udf, phixSpikeIn)
        } else {
            artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_PHIX_SPIKE_IN.udf,
                    new BigDecimal(phixSpikeIn))
        }
    }

    BigDecimal getUdfPhixSpikeIn() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_PHIX_SPIKE_IN.udf)
    }

    void setSequencingFailureMode(String sequencingFailureMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_SEQUENCING_FAILURE_MODE.udf, sequencingFailureMode)
    }

    String getSequencingFailureMode(){
        artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_SEQUENCING_FAILURE_MODE.udf)
    }

    void setClusterGenFailureMode(String clusterGenFailureMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_CLUSTER_GEN_FAILURE_MODE.udf, clusterGenFailureMode)
    }

    String getClusterGenFailureMode(){
        artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_CLUSTER_GEN_FAILURE_MODE.udf)
    }

    def getLane() {
        String outputLocation = artifactNodeInterface.location
        return outputLocation.substring(0, outputLocation.indexOf(':'))
    }
}
