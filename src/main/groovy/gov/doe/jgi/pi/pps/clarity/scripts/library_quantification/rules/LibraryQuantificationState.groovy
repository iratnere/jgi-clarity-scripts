package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules

import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules.ObjectWithDynamicAttributes

/**
 * Created by datjandra on 7/7/2015.
 */
class LibraryQuantificationState extends ObjectWithDynamicAttributes {

    @Override
    void setAttribute(Object key, Object value) {
        if (!key instanceof AttributeKeys) {
            throw new IllegalArgumentException("attribute key must be enum type $AttributeKeys")
        }
        super.setAttribute(key, value)
    }
}
