package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface

/**
 * Created by dscott on 11/25/2014.
 */
class AnalyteFactory {

    private static Analyte generateAnalyte(ArtifactNodeInterface artifactNodeInterface) {
        if (artifactNodeInterface.isControlAnalyte) {
            if (artifactNodeInterface.isSample) {
                return new ControlSampleAnalyte(artifactNodeInterface)
            } else {
                return new ControlAnalyte(artifactNodeInterface)
            }
        }
        String processTypeString = artifactNodeInterface.parentProcessType
        if (processTypeString) {
            ProcessType processType = ProcessType.toEnum(processTypeString)
            Analyte analyte = processType?.outputAnalyteGenerator?.call(artifactNodeInterface)
            if (!analyte) {
                analyte = new Analyte(artifactNodeInterface)
            }
            return analyte
        }
        return new SampleAnalyte(artifactNodeInterface)
    }

    static final Analyte analyteInstance(ArtifactNodeInterface artifactNodeInterface) {
        if (!artifactNodeInterface) {
            return null
        }
        if (!artifactNodeInterface.cache) {
            artifactNodeInterface.cache = generateAnalyte(artifactNodeInterface)
        }
        return  (Analyte) artifactNodeInterface.cache
    }

}