package gov.doe.jgi.pi.pps.webtransaction.exception

import gov.doe.jgi.pi.pps.util.exception.BundleAwareException
import gov.doe.jgi.pi.pps.util.exception.HttpStatusCodeReporter
import grails.validation.ValidationErrors
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError

class MessageListException extends RuntimeException implements HttpStatusCodeReporter {

	private List<Map> messages = []
	private Integer status
	
	MessageListException() {
		super()
	}

	MessageListException(Throwable cause) {
		super(cause)
	}

	MessageListException(List<Map> messages, Integer status = null) {
		super()
		addMessages(messages)
		this.status = status
	}

	MessageListException(Map message, Integer status = null) {
		super()
		addMessage(message)
		this.status = status
	}

	MessageListException(BundleAwareException bae, Integer status = null) {
		super()
		addBundleAwareException(bae)
		this.status = status
	}

	MessageListException(List<Map> messages, String message, Integer status = null) {
		super(message)
		addMessages(messages)
		this.status = status
	}

	MessageListException(ValidationErrors errors, Map msgMap, Integer status){
		super()
		addValidationErrors(errors, msgMap)
		this.status = status
	}

	MessageListException(ValidationErrors errors, Integer status) {
		super()
		addValidationErrors(errors)
		this.status = status
	}

	static List<Map> validationErrorsToMessageMaps(ValidationErrors errors, Map msgMap = null) {
		List<Map> messageMaps = []
		errors?.allErrors?.each() { ObjectError objectError ->
			Map errorMsg = [:]
			List<String> args = objectError.arguments*.toString()
			if (msgMap) {
				errorMsg.putAll(msgMap)
			}
			errorMsg['code'] = objectError.code
			errorMsg['entity'] = objectError.objectName
			if (objectError instanceof FieldError) {
				errorMsg['key'] = ((FieldError) objectError).field
			}
			messageMaps << errorMsg
		}
		return messageMaps
	}

	void addBundleAwareException(BundleAwareException bae) {
		addMessage(bae?.messageBundleParams)
	}

	private addValidationErrors(ValidationErrors errors, Map msgMap = null) {
		messages.addAll(validationErrorsToMessageMaps(errors, msgMap))
	}

	void setHttpStatusCode(Integer statusCode) {
		this.status = statusCode
	}

	Integer getHttpStatusCode() {
		return status
	}

	List<Map> getMessages() {
		return messages
	}

	void addMessage(Map message) {
		if (message != null) {
			messages.add(message)
		}
	}

	void addMessages(List<Map> messages) {
		messages?.each {Map message -> addMessage(message)}
	}

	String toString() {
		StringBuilder msg =  new StringBuilder()
		if (message) {
			msg << message
		}
		if (messages) {
			messages.each{Map msgParams ->
				msg << '['
				msg << msgParams.keySet().sort().collect{ key ->
					"${key}:${msgParams[key]}"
				}.join(',')
				msg << ']'
			}
		}
		if (cause) {
			msg << " (caused by ${cause})"
		}
		msg.toString()
	}
	
	
}
