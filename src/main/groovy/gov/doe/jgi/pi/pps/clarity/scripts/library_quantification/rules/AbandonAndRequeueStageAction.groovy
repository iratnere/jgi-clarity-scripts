package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules.Action

/**
 * Created by datjandra on 4/6/2016.
 */
class AbandonAndRequeueStageAction implements Action {

    @Override
    Stage perform(def subject) {
        return Stage.ABANDON_QUEUE
    }

    @Override
    boolean isNoOp() {
        return false
    }
}
