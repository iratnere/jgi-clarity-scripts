package gov.doe.jgi.pi.pps.couchdb.query.changes

import gov.doe.jgi.pi.pps.couchdb.util.CouchDb

/**
 * Created by dscott on 6/4/2015.
 */
class ChangesQuery {

    CouchDb couchdb
    def since
    def limit

    ChangesQuery() {}

    ChangesQuery(CouchDb couchdb, since, limit) {
        this.couchdb = couchdb
        this.since = since ?:  BigInteger.ZERO
        this.limit = limit
    }

    String toString() {
        "changes-query couchdb:${couchdb};since:${since};limit:${limit}"
    }

}
