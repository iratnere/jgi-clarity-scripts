package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules

/**
 * Created by datjandra on 7/6/2015.
 */
class LessThanCondition extends Condition {

    private Object key
    private Object value

    LessThanCondition(Object key, Object value) {
        if (key == null) {
            throw new IllegalArgumentException('Condition requires attribute key')
        }

        if (value == null) {
            throw new IllegalArgumentException('Condition requires accepted value')
        }

        this.key = key
        this.value = value
    }

    @Override
    boolean evaluate(ObjectWithDynamicAttributes percept) {
        Object otherValue = percept.getAttribute(key)
        if (otherValue != null) {
            return (otherValue < value)
        }
        return false
    }

    @Override
    public String toString() {
        return "$key = $value"
    }
}
