package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.grails.PostProcessService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.KeyValueSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ProcessUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.AttributeValues
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.PlatePassFailKeyValueBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.QpcrTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.email_notification.LibraryPlateFailedQPcrEmailNotification
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules.*
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules.Action
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerStates
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.ActionType
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.NextAction

/**
 * Created by tlpaley on 3/2/15.
 */
class LqRouteToNextWorkflow extends ActionHandler {
    ExcelWorkbook excelWorkbook
    List<Analyte> failedPlateLibraries = []

    void execute() {
        excelWorkbook = (process as LibraryQpcr).getQpcrWorkbook()
        loadExcelWorkbook(excelWorkbook)
        performLastStepActions()
        sendEmailNotification()
    }

    void sendEmailNotification(List<Analyte> analytes = failedPlateLibraries){
        if(analytes)
            new ProcessUtility(this.process).sendEmailNotification(analytes, new LibraryPlateFailedQPcrEmailNotification())
    }

    void performLastStepActions() {
        updateInputsFromWorkbook(excelWorkbook)
        Map<String,List<Analyte>> stageQueue = populateStageQueue(excelWorkbook)
        stageQueue.each { String uri, List<Analyte> analytes ->
            ClarityWebTransaction.logger.info "moving ${analytes.size()} artifacts to $uri"
            process.routeArtifactIdsToUri(uri, analytes*.id)
        }
        setNextActions(getRepeatAnalytes(stageQueue))
        postAutomaticProcess(getRepeatAnalytes(stageQueue), Stage.AUTOMATIC_REQUEUE_QPCR)
        postAutomaticProcess(getSacQueueAnalytes(stageQueue), Stage.AUTOMATIC_REQUEUE_LIBRARY_CREATION)
        discardOutputContainers()
    }

    List<String> getPassFailList(List<QpcrTableBean> dilutionTable) {
        List<String> passFailList = new ArrayList<String>()
        analytesToRoute.each { Analyte inputAnalyte ->
            QpcrTableBean tableBean = findTableBean(dilutionTable, inputAnalyte.name)
            if (tableBean) {
                passFailList.add(tableBean.passFail.value)
            } else {
                passFailList.add(inputAnalyte.artifactNode.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QPCR_RESULT.udf))
            }
        }
        return passFailList
    }

    static QpcrTableBean findTableBean(List<QpcrTableBean> dilutionTable, String libraryName) {
        return dilutionTable.find {it.libraryName == libraryName}
    }

    void discardOutputContainers() {
        Set<ContainerNode> outputContainers = process.outputAnalytes.collect { it.containerNode }.toSet()
        outputContainers.each { ContainerNode containerNode ->
            containerNode.setState(ContainerStates.DISCARDED)
        }
    }

    void postAutomaticProcess(List<Analyte> analytes, Stage stage) {
        if (!analytes)
            return
        PostProcessService taskGenerationService = (PostProcessService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(PostProcessService)
        taskGenerationService.postInputProcess(process, stage, analytes*.artifactNode)
    }

    void setNextActions(List<Analyte> repeatAnalytes) {
        if (!repeatAnalytes)
            return
        List<NextAction> nextActions = []
        process.inputAnalytes.each { Analyte inputAnalyte ->
            Analyte outputAnalyte = AnalyteFactory.analyteInstance(process.processNode.getOutputAnalytes(inputAnalyte.id).first())
                if (inputAnalyte in repeatAnalytes) {
                    nextActions << outputAnalyte.getNextAction(ActionType.REPEAT, '')
                } else {
                    nextActions << outputAnalyte.getNextAction(ActionType.COMPLETE, '')
                }
            }
        if (nextActions) {
            process.setArtifactActions(nextActions)
        }
    }

    static List<Analyte> getRepeatAnalytes(Map<String, List<Analyte>> stageQueue) {
        List<Analyte> repeats = []
        stageQueue.each { String uri, List<Analyte> analyteList ->
            if (Stage.LIBRARY_QPCR.uri.contains(uri)) {
                repeats.addAll(analyteList)
            }
        }
        return repeats?.unique()
    }

    static List<Analyte> getSacQueueAnalytes(Map<String, List<Analyte>> stageQueue) {
        List<Analyte> sacQueueAnalytes = []
        stageQueue.each { String uri, List<Analyte> analyteList ->
            if (Stage.ALIQUOT_CREATION_DNA.uri.contains(uri)) {
                sacQueueAnalytes.addAll(analyteList)
            }
            if (Stage.ALIQUOT_CREATION_RNA.uri.contains(uri)) {
                sacQueueAnalytes.addAll(analyteList)
            }
        }
        return sacQueueAnalytes?.unique()
    }

    List<Analyte> getAnalytesToRoute() {
        List<ContainerNode> containersToRoute = process.inputAnalytes.collect {it.containerNode}?.unique()
        List<Analyte> analytesToRoute = []
        containersToRoute.each{ ContainerNode inputContainer ->
            analytesToRoute.addAll( inputContainer.contentsArtifactNodes.collect{ AnalyteFactory.analyteInstance(it) } )
        }
        return analytesToRoute
    }

    Map<String,List<Analyte>> populateStageQueue(ExcelWorkbook excelWorkbook){
        List<QpcrTableBean> dilutionTable = getDilutionTable(excelWorkbook)
        List<String> passFailList = getPassFailList(dilutionTable)
        String platePassFailValue = getPlatePassFailKeyValue(excelWorkbook).passFail.value
        List<String> requeueList = passFailList.findAll { it == AttributeValues.REWORK }

        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        Map<String,List<Analyte>> stageQueue = [:].withDefault { new ArrayList<Analyte>() }

        analytesToRoute.each{ Analyte inputAnalyte ->

            QpcrTableBean tableBean = findTableBean(dilutionTable, inputAnalyte.name)
            LibraryQuantificationState state = new LibraryQuantificationState()
            if (tableBean) {
                state.setAttribute(AttributeKeys.ANALYTE_STATUS, tableBean.passFail.value)
            } else {
                state.setAttribute(AttributeKeys.ANALYTE_STATUS,
                        inputAnalyte.artifactNode.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QPCR_RESULT.udf))
            }

            state.setAttribute(AttributeKeys.CONTAINER_CLASS,
                    inputAnalyte.containerNode.containerClassEnum.value)
            state.setAttribute(AttributeKeys.DOP, inputAnalyte.udfDegreeOfPooling.toInteger())
            state.setAttribute(AttributeKeys.PLATE_STATUS, platePassFailValue)
            state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, passFailList)
            state.setAttribute(AttributeKeys.LC_ATTEMPT, inputAnalyte.maxLcAttempt)
            state.setAttribute(AttributeKeys.EXOME, inputAnalyte.isExomeCapture)
            state.setAttribute(AttributeKeys.ITAG, inputAnalyte.isItag)
            state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, inputAnalyte.isInternalSingleCell)
            state.setAttribute(AttributeKeys.EXTERNAL, inputAnalyte.claritySample.isExternal)
            state.setAttribute(AttributeKeys.ANALYTE_CLASS, inputAnalyte.class)
            SequencerModelCv seqModelCv = inputAnalyte.runModeCv.sequencerModel
            String flowcellType = seqModelCv.flowcellType
            String seqModel = seqModelCv.sequencerModel.replace(flowcellType, "").trim()
            state.setAttribute(AttributeKeys.SEQUENCER_MODEL, seqModel)

            Action action = routingRules.execute(state)
            if (!action) {
                String warningMessage = "Routing not performed for ${inputAnalyte}"
                ClarityWebTransaction.logger.warn warningMessage
            }

            def stage = action?.perform(inputAnalyte)
            String stageUri = (stage instanceof Stage) ? stage.uri : stage
            if (!stageUri) {
                String warningMessage = "Could not lookup stage for ${inputAnalyte}, maybe run mode is not set?"
                ClarityWebTransaction.logger.warn warningMessage
            } else {
                stageQueue.get(stageUri).add(inputAnalyte)
            }

            if (inputAnalyte.containerNode.isPlate) {
                if (!requeueList) {
                    ClaritySample claritySample = inputAnalyte.claritySample
                    if (action instanceof AbandonStageAction) {
                        if (claritySample.isDnaProject) {
                            stageQueue.get(Stage.ALIQUOT_CREATION_DNA.uri).add(claritySample.getSampleAnalyte())
                        } else {
                            stageQueue.get(Stage.ALIQUOT_CREATION_RNA.uri).add(claritySample.getSampleAnalyte())
                        }
                    } else if (action instanceof AbandonAndRequeueStageAction) {
                        stageQueue.get(Stage.REQUEUE_LIBRARY_CREATION.uri).add(claritySample.getSampleAnalyte())
                        stageQueue.get(Stage.ABANDON_WORK.uri).add(claritySample.getSampleAnalyte())
                    }
                }
            } else {
                ClaritySample claritySample = inputAnalyte.claritySample
                if (action instanceof AbandonStageAction) {
                    if (claritySample.isDnaProject) {
                        stageQueue.get(Stage.ALIQUOT_CREATION_DNA.uri).add(claritySample.getSampleAnalyte())
                    } else {
                        stageQueue.get(Stage.ALIQUOT_CREATION_RNA.uri).add(claritySample.getSampleAnalyte())
                    }
                } else if (action instanceof AbandonAndRequeueStageAction) {
                    stageQueue.get(Stage.REQUEUE_LIBRARY_CREATION.uri).add(claritySample.getSampleAnalyte())
                    stageQueue.get(Stage.ABANDON_WORK.uri).add(claritySample.getSampleAnalyte())
                }
            }
        }
        stageQueue
    }

    void updateInputsFromWorkbook(ExcelWorkbook excelWorkbook) {
        Map<String, Analyte> inputsByName = new HashMap<String, Analyte>()
        List<Analyte> inputs = process.inputAnalytes
        for (Analyte input : inputs) {
            inputsByName.put(input.name, input)
        }
        ClarityWebTransaction.logger.info "Loaded ${inputs.size()} input analytes"

        String platePassFailValue = getPlatePassFailKeyValue(excelWorkbook).passFail.value
        ClarityWebTransaction.logger.info "Loaded rows from dilution table"

        Set<String> observedInputs = new HashSet<String>()
        for (QpcrTableBean tableBean : getDilutionTable(excelWorkbook)) {
            String[] words = tableBean.library.split('\\s+')
            String name = words.take(words.size()-1).join(' ')
            Analyte inputAnalyte = inputsByName.get(name)
            if (!inputAnalyte) {
                continue
            }

            if (observedInputs.contains(inputAnalyte.id)) {
                continue
            }
            observedInputs.add(inputAnalyte.id)

            String passFailValue = tableBean.passFail.value
            if (passFailValue.equals(AttributeValues.PASS)) {
                inputAnalyte.artifactNode.setSystemQcFlag(Boolean.TRUE)
            } else if (passFailValue.equals(AttributeValues.FAIL)) {
                inputAnalyte.artifactNode.setSystemQcFlag(Boolean.FALSE)
                if(inputAnalyte.isOnPlate)
                    failedPlateLibraries << inputAnalyte
            }

            ClarityLibraryStock libraryStock = null
            ClarityLibraryPool libraryPool = null
            if (inputAnalyte instanceof ClarityLibraryStock) {
                libraryStock = inputAnalyte as ClarityLibraryStock
            } else if (inputAnalyte instanceof ClarityLibraryPool) {
                libraryPool = inputAnalyte as ClarityLibraryPool
            }

            if (libraryStock) {
                String passFail = tableBean.passFail.value
                if (!passFail) {
                    String errorMessage = "Library Pass/Fail must be set"
                    process.postErrorMessage(errorMessage)
                }

                if (platePassFailValue?.equals(AttributeValues.DONE)) {
                    if (!passFail.equals(Analyte.FAIL) && !passFail.equals(Analyte.PASS) && libraryStock.containerNode.isPlate) {
                        String errorMessage = "if plate ${AttributeValues.DONE} analytes must be either 'Pass' or 'Fail'"
                        process.postErrorMessage(errorMessage)
                    }
                }

                libraryStock.setUdfLibraryQpcrResult(passFail)
                if (passFail.equals(Analyte.FAIL)) {
                    String failureMode = tableBean.failureMode.value
                    if (!failureMode) {
                        String errorMessage = "Failure mode required for $libraryStock"
                        process.postErrorMessage(errorMessage)
                    }
                    libraryStock.setUdfLibraryQpcrFailureMode(failureMode)
                }

                BigDecimal libraryMolarityPm = tableBean.libraryMolarityPm
                if (libraryMolarityPm != null) {
                    libraryStock.setUdfLibraryMolarityPm(libraryMolarityPm)
                } else {
                    String errorMessage = "Library molarity required for $libraryStock"
                    process.postErrorMessage(errorMessage)
                }

                BigDecimal volumeUsed = tableBean.volumeUsed
                if (volumeUsed != null) {
                    libraryStock.setUdfVolumeUl(libraryStock.udfVolumeUl - tableBean.volumeUsed)
                } else {
                    String errorMessage = "Volume used required for $libraryStock"
                    process.postErrorMessage(errorMessage)
                }

                if (tableBean.notes) {
                    libraryStock.setUdfNotes(tableBean.notes)
                }
            } else if (libraryPool) {
                String passFail = tableBean.passFail.value
                if (!passFail) {
                    String errorMessage = "Library Pass/Fail must be set"
                    process.postErrorMessage(errorMessage)
                }

                if (platePassFailValue?.equals(AttributeValues.DONE)) {
                    if (!passFail.equals(Analyte.FAIL) && !passFail.equals(Analyte.PASS) && libraryPool.containerNode.isPlate) {
                        String errorMessage = "if plate ${AttributeValues.DONE} analytes must be either 'Pass' or 'Fail'"
                        process.postErrorMessage(errorMessage)
                    }
                }

                libraryPool.setUdfLibraryQpcrResult(passFail)
                if (passFail.equals(Analyte.FAIL)) {
                    String failureMode = tableBean.failureMode.value
                    if (!failureMode) {
                        String errorMessage = "Failure mode required for $libraryPool"
                        process.postErrorMessage(errorMessage)
                    }
                    libraryPool.setUdfLibraryQpcrFailureMode(failureMode)
                }

                BigDecimal libraryMolarityPm = tableBean.libraryMolarityPm
                if (libraryMolarityPm != null) {
                    libraryPool.setUdfLibraryMolarityPm(libraryMolarityPm)
                } else {
                    String errorMessage = "Library molarity required for $libraryPool"
                    process.postErrorMessage(errorMessage)
                }

                BigDecimal volumeUsed = tableBean.volumeUsed
                if (volumeUsed != null) {
                    libraryPool.setUdfVolumeUl(libraryPool.udfVolumeUl - tableBean.volumeUsed)
                } else {
                    String errorMessage = "Volume used required for $libraryPool"
                    process.postErrorMessage(errorMessage)
                }

                if (tableBean.notes) {
                    libraryPool.setUdfNotes(tableBean.notes)
                }
            }

            ContainerNode inputContainer = inputAnalyte.containerNode
            if (inputContainer.isPlate) {
                if (!platePassFailValue) {
                    String errorMessage = "Plate Pass/Fail required for plate"
                    process.postErrorMessage(errorMessage)
                }
                inputContainer.setUdf(ClarityUdf.CONTAINER_LIBRARY_QPCR_RESULT.udf, platePassFailValue)
            }
        }
    }

    static Map<String,BigDecimal> getLibraryNameToConcentration(ExcelWorkbook excelWorkbook) {
        Map<String,BigDecimal> concByLibraryName = [:]
        List<QpcrTableBean> namesTable = excelWorkbook.sections.values()[2].getData()
        for (QpcrTableBean tableBean : namesTable) {
            concByLibraryName.put(tableBean.name, tableBean.concentration)
        }
        concByLibraryName
    }

    static PlatePassFailKeyValueBean getPlatePassFailKeyValue(ExcelWorkbook excelWorkbook) {
        return (PlatePassFailKeyValueBean) excelWorkbook.sections.values()[1].data
    }

    static List<QpcrTableBean> getDilutionTable(ExcelWorkbook excelWorkbook) {
        return (List<QpcrTableBean> ) excelWorkbook.sections.values()[0].getData()
    }

    static ExcelWorkbook loadExcelWorkbook(ExcelWorkbook excelWorkbook){
        Section dilutionSection = new TableSection(1, QpcrTableBean.class, 'H27', 'V395')
        excelWorkbook.addSection(dilutionSection)

        Section passFailSection = new KeyValueSection(1, PlatePassFailKeyValueBean.class, 'H22')
        excelWorkbook.addSection(passFailSection)

        TableSection nameSection = new TableSection(1, QpcrTableBean.class, 'C1', 'C385', true)
        excelWorkbook.addSection(nameSection)
        excelWorkbook.load()
        return excelWorkbook
    }
}