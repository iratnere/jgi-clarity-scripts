package gov.doe.jgi.pi.pps.webtransaction.util

import org.grails.web.json.JSONElement

/**
 * Created by dscott on 4/14/2015.
 */
interface JsonEntity {

    JSONElement getJson()

}