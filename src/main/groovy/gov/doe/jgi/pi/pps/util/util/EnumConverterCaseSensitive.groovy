package gov.doe.jgi.pi.pps.util.util

class EnumConverterCaseSensitive<K> {

    private final OnDemandCache<Map<String, K>> cachedConversionMap = new OnDemandCache<>()
    private K enumType

    EnumConverterCaseSensitive(K enumType) {
        this.enumType = enumType
    }

    private final Map<String, K> getConversionMap() {
        return cachedConversionMap.fetch {
            Map<String, K> cMap = [:]
            enumType.values().each { K enumValue ->
                String strVal = enumValue.toString().trim()
                if (cMap.containsKey(strVal)) {
                    throw new RuntimeException("duplicate case-sensitive enum value [${strVal}]")
                }
                cMap[strVal] = enumValue
            }
            cMap
        }
    }

    final K toEnum(Object value) {
        String trimValue = value?.toString()?.trim()
        K matchingType = null
        if (trimValue) {
            matchingType = conversionMap[trimValue]
        }
        return matchingType
    }

}
