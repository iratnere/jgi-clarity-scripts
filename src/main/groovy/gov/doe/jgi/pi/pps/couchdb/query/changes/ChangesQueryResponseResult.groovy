package gov.doe.jgi.pi.pps.couchdb.query.changes

import gov.doe.jgi.pi.pps.couchdb.util.CouchDb
import org.grails.web.json.JSONObject

/**
 * Created by dscott on 6/4/2015.
 */
class ChangesQueryResponseResult {

    BigInteger seq
    String id
    List<Change> changes = []
    boolean deleted
    ChangesQueryResponse changesQueryResponse

    CouchDb getCouchDb() {
        return changesQueryResponse?.changesQuery?.couchdb
    }

    ChangesQueryResponseResult(JSONObject json, ChangesQueryResponse changesQueryResponse) {
        this.changesQueryResponse = changesQueryResponse
        this.seq = json.'seq' as BigInteger
        this.id = json.'id' as String
        this.deleted = json.'deleted'
        json?.'changes'?.each { JSONObject changeJson ->
            changes << new Change(changeJson)
        }
    }

    String toString() {
        "changes-result couchdb[${couchDb}]id[${id}]seq[${seq}]deleted[${deleted}]changes${changes}"
    }

}
