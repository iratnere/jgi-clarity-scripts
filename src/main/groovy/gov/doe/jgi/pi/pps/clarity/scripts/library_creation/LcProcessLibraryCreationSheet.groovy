package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

/**
 * Created by tlpaley on 12/5/14.
 */
class LcProcessLibraryCreationSheet extends ActionHandler {

    @Override
    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        ClarityProcess clarityProcess = process as LibraryCreationProcess

        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        clarityProcess.lcAdapter.validateProcessUdfs()
        clarityProcess.lcAdapter.updateLibraryStockUdfs()
        process.setCompleteStage()
    }

}