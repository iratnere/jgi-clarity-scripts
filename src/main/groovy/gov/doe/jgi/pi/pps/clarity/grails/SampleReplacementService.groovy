package gov.doe.jgi.pi.pps.clarity.grails


import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovy.json.JsonBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method

@Transactional
class SampleReplacementService {
    GrailsApplication grailsApplication

    int replaceSamples(def operatorId, Set<Long> sampleIds) {
        if (!sampleIds || operatorId == null)
            return
        def jsonRequest = new JsonBuilder()
        jsonRequest {
            'submitted-by'(operatorId.longValue())
            'sample-ids'(sampleIds)
        }
        return callSampleReplacementService(jsonRequest)
    }

    int callSampleReplacementService(def jsonRequest){
        String sampleReplacedUrl = "${grailsApplication.config.getProperty('sampleReplaced.url')}"
        int responseCode
        HTTPBuilder http = new HTTPBuilder(sampleReplacedUrl)
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = jsonRequest.toPrettyString()
            response.success = { resp, reader ->
                ClarityWebTransaction.logger.info "GlsSampleReplaced response - ${resp.statusLine}"
                responseCode = resp.statusLine.statusCode
            }
            response.failure = {resp, reader ->
                ClarityWebTransaction.logger.error "GlsSampleReplaced response - ${resp.statusLine}"
                responseCode = resp.statusLine.statusCode
                throw new WebException(reader, resp.statusLine.statusCode)

            }
        }
        return responseCode
    }
}
