package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules

/**
 * Created by datjandra on 7/6/2015.
 */
interface Action {
    def perform(Object subject)
    boolean isNoOp()
}
