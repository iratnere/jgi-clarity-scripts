package gov.doe.jgi.pi.pps.clarity.web_transaction

import gov.doe.jgi.pi.pps.clarity.grails.ClarityCouchdbService
import gov.doe.jgi.pi.pps.couchdb.transaction.CouchdbWebTransactionSuccessOnlyRecorder
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransactionRecorder

/**
 * Created by dscott on 3/31/2016.
 */
class ClarityWebTransactionSuccessRecorder extends CouchdbWebTransactionSuccessOnlyRecorder implements WebTransactionRecorder {

    ClarityWebTransactionSuccessRecorder() {
        super(ClarityCouchdbService.CouchDatabase.WEB_TRANSACTION.couchDb)
    }

}
