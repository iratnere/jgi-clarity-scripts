package gov.doe.jgi.pi.pps.clarity.model.researcher

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.ResearcherUriIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.ParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.ResearcherParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ResearcherNode

/**
 * Created by duncanscott on 5/1/15.
 */
class ResearcherFactory {

    private static Researcher generateResearcher(ResearcherNode researcherNode) {
        return new LabUser(researcherNode)
        //return new Researcher(researcherNode)
    }

    static final Researcher researcherInstance(ResearcherNode researcherNode) {
        if (!researcherNode) {
            return null
        }
        if (!researcherNode.cache) {
            researcherNode.cache = generateResearcher(researcherNode)
        }
        return  (Researcher) researcherNode.cache
    }

    static Researcher researcherForContactId(NodeManager nodeManager, Long contactId) {
        if (contactId != null) {
            return researcherInstance(researcherNodeForContactId(nodeManager,contactId))
        }
        return null
    }

    static Researcher researcherForLimsId(NodeManager nodeManager, String researcherLimsId) {
        if (researcherLimsId) {
            ResearcherNode researcherNode = nodeManager.getResearcherNode(researcherLimsId)
            /*
            Long contactId = researcherNode.getUdfAsBigInteger(ClarityUdf.RESEARCHER_CONTACT_ID.udf)?.toLong()
            if (contactId != null) {
                return researcherForContactId(nodeManager,contactId)
            }
            */
            return researcherInstance(researcherNode)
        }
        return null
    }

    private static final ResearcherNode researcherNodeForContactId(NodeManager nodeManager, Long contactId) {
        ResearcherNode unlockedResearcherNode = null
        ResearcherNode lockedResearcherNode = null
        ParameterValue rpv = ResearcherParameter.UDF.setValue(contactId).setUdf(ClarityUdf.RESEARCHER_CONTACT_ID.value)
        Iterator<String> researcherUriIterator = new ResearcherUriIterator(nodeManager.nodeConfig, [rpv])
        List<String> researcherUris = researcherUriIterator.toList()
        while (!unlockedResearcherNode && researcherUris) {
            String researcherUri = researcherUris.pop() //take last first (we want to use last account creataed for contact_id)
            String researcherId = extractResearcherIdFromUrl(researcherUri)
            ResearcherNode researcherNode = nodeManager.getResearcherNode(researcherId)
            if (!researcherNode.accountLocked) {
                unlockedResearcherNode = researcherNode
            } else if (!lockedResearcherNode) {
                lockedResearcherNode = researcherNode
            }
        }
        return unlockedResearcherNode ?: lockedResearcherNode
    }


    static String extractResearcherIdFromUrl(String url) {
        return url.split('/').last()
    }

}
