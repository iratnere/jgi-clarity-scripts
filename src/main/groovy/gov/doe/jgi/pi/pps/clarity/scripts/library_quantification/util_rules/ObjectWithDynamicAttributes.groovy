package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules

/**
 * Created by datjandra on 7/6/2015.
 */
abstract class ObjectWithDynamicAttributes {

    protected Map<Object, Object> attributes = new LinkedHashMap<Object, Object>()

    void setAttribute(Object key, Object value) {
        attributes.put(key, value)
    }

    Object getAttribute(Object key) {
        return attributes.get(key)
    }

    void removeAttribute(Object key) {
        attributes.remove(key)
    }

    String describeType() {
        return getClass().getSimpleName()
    }

    String describeAttributes() {
        StringBuilder sb = new StringBuilder()
        sb.append("[")
        boolean first = true
        for (Object key : attributes.keySet()) {
            if (first) {
                first = false
            } else {
                sb.append(", ")
            }

            sb.append(key)
            sb.append("==")
            sb.append(attributes.get(key))
        }
        sb.append("]")
        return sb.toString()
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return super.equals(o)
        }
        return attributes.equals(((ObjectWithDynamicAttributes) o).attributes)
    }

    @Override
    public int hashCode() {
        return attributes.hashCode()
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
        sb.append(describeType())
        sb.append(describeAttributes())
        return sb.toString()
    }
}
