package gov.doe.jgi.pi.pps.couchdb.query.all_docs

import org.grails.web.json.JSONObject

/**
 * Created by duncanscott on 6/9/15.
 */
class AllDocsQueryResponse {

    /*
    Response JSON Object:

    offset (number) – Offset where the document list started
    rows (array) – Array of view row objects. By default the information returned contains only the document ID and revision.
    total_rows (number) – Number of documents in the database/view. Note that this is not the number of rows returned in the actual query.
    update_seq (number) – Current update sequence for the database
    */

    AllDocsQuery allDocsQuery

    BigInteger offset
    BigInteger totalRows
    BigInteger updateSeq
    List<AllDocsQueryResponseRow> rows = []

    AllDocsQueryResponse(AllDocsQuery allDocsQuery, JSONObject json) {
        this.allDocsQuery = allDocsQuery
        this.offset = json.'offset' as BigInteger
        this.totalRows = json.'total_rows' as BigInteger
        this.updateSeq = json.'update_seq' as BigInteger
        json.'rows'.each { JSONObject rowJson ->
            this.rows << new AllDocsQueryResponseRow(rowJson)
        }
    }

}
