package gov.doe.jgi.pi.pps.clarity.cv

import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseInsensitive

/**
 * Created by lvishwas on 2/13/17.
 */
enum SowItemPurposeCv {

    ANTICIPATED_PLANNED_WORK('Anticipated Planned Work'),
    REWORK('Rework'),
    TROUBLESHOOTING('Troubleshooting'),
    UNANTICIPATED_ADDITIONAL_WORK('Unanticipated Additional Work'),
    UNKNOWN('Unknown'),
    VALIDATION('Validation')

    private final String value

    private static final EnumConverterCaseInsensitive<SowItemPurposeCv> converter = new EnumConverterCaseInsensitive(SowItemPurposeCv)

    private SowItemPurposeCv(String value) {
        this.value = value
    }

    String getValue() {
        return value
    }

    String toString() {
        return value
    }

    static SowItemPurposeCv toEnum(String value) {
        return converter.toEnum(value)
    }
}