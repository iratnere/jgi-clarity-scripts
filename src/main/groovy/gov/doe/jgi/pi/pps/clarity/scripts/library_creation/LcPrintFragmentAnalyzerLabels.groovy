package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode

/**
 * Created by tlpaley on 12/5/14.
 */
class LcPrintFragmentAnalyzerLabels extends ActionHandler {
    
    void execute() {
        process.writeLabels(collectPrintLines())
        LibraryCreationProcess clarityProcess = process as LibraryCreationProcess
        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        //for testing only: will not be used on production
        if (clarityProcess.lcAdapter instanceof LibraryCreationDapSeq)
            clarityProcess.lcAdapter.uploadExtraFiles()
    }

    Set<String> collectPrintLines(List<ContainerNode> containerNodes = (process as LibraryCreationProcess)
            .getContainerNodes(process.outputAnalytes)) {
        def lines = containerNodes.collect { it.id + '.FA,' + it.id +'.FA' } as Set
        return lines
    }
}
