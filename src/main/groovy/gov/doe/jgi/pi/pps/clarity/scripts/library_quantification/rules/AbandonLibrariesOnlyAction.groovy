package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules.Action

/**
 * Created by datjandra on 7/7/2015.
 */
class AbandonLibrariesOnlyAction implements Action {

    @Override
    Stage perform(def subject) {
        return Stage.ABANDON_QUEUE
    }

    @Override
    boolean isNoOp() {
        return false
    }
}
