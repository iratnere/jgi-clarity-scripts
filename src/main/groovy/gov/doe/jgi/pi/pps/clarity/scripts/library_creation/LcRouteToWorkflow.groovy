package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.FreezerService
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

/**
 * Created by tlpaley on 12/5/14.
 */
class LcRouteToWorkflow extends ActionHandler {

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        doRefresh()
        performLastStepActions()
    }

    void performLastStepActions() {
        ClarityProcess clarityProcess = process as LibraryCreationProcess
        FreezerService freezerService = (FreezerService) ClarityWebTransaction.requireCurrentTransaction()?.requireApplicationBean(FreezerService)
        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        clarityProcess.lcAdapter.updateLibraryPoolUdfs()
        freezerService.freezerCheckoutInputAnalytes(clarityProcess)
        clarityProcess.lcAdapter.updateInputsAndSamples()
        if (clarityProcess.printLabelsTriggerUnused)
            clarityProcess.lcAdapter.createPrintLabelsFile()
        clarityProcess.lcAdapter.createPoolPrintLabelsFile()
        clarityProcess.lcAdapter.updateLcaUdfs()
        clarityProcess.lcAdapter.moveToNextWorkflow()
    }
}