package gov.doe.jgi.pi.pps.webtransaction.util;

import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder

class ExternalId {
	
	String type
	String id
	
	ExternalId(type, id) {
		this.type = type as String
		this.id = id as String
	}
	
	ExternalId() {}
	
	String toString() {
		return "${type}.${id}"
	}
	
	@Override
	boolean equals(o) {
		if (o == null) return false
		if (this.is(o)) return true
		if (!(o instanceof ExternalId)) return false

        ExternalId that = (ExternalId) o
		new EqualsBuilder()
				.append(id, that.id)
				.append(type, that.type)
				.isEquals()
	}

	@Override
	int hashCode() {
		new HashCodeBuilder()
				.append(id)
				.append(type)
				.toHashCode()
	}
}
