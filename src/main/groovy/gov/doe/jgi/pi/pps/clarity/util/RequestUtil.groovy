package gov.doe.jgi.pi.pps.clarity.util

import gov.doe.jgi.pi.pps.util.exception.WebException

import javax.servlet.http.HttpServletRequest

/**
 * Created by datjandra on 3/27/2015.
 */
final class RequestUtil {

    private RequestUtil() {}

    static String getTestModeBaseUrl() {
        return new File('./web-app/').canonicalFile.toURI().toString()
    }

    static String extractBaseUrlString(String url) {
        def matcher = url =~ /^(.+\/\/[^\/]+\/[^\/]+\/).+/
        return matcher[0][1]
    }

    static String extractBaseUrl(HttpServletRequest servletRequest, boolean testMode = false) throws WebException {
        String requestUrl = servletRequest?.requestURL?.toString()
        if (testMode) {
            return getTestModeBaseUrl()
        }

        try {
            return extractBaseUrlString(requestUrl)
        } catch (Exception e) {
            throw new WebException([code: 'parse.failure.url.base', args: [requestUrl]], 500)
        }
    }
}
