package gov.doe.jgi.pi.pps.couchdb.query.changes

import org.grails.web.json.JSONObject

/**
 * Created by dscott on 6/4/2015.
 */
class Change {

    //JSONObject json
    String rev

    Change(JSONObject json) {
        //this.json = json
        this.rev = json.'rev'
    }

    String toString() {
        "change rev:${rev}"
    }
}
