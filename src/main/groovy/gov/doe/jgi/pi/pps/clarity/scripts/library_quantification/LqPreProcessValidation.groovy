package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes

/**
 * Created by tlpaley on 3/2/15.
 *
 */
class LqPreProcessValidation extends ActionHandler {
    void execute() {
        validateInputsContainerType()
        validateAnalytes(process.inputAnalytes)
    }

    static void validateAnalytes(List<Analyte> inputAnalytes){
        inputAnalytes.each {
            def errorMsg = StringBuilder.newInstance()
            if (it instanceof ClarityLibraryPool || it instanceof ClarityLibraryStock) {
                validateUdfs(it, errorMsg)
            } else {
                errorMsg << "$it is not ClarityLibraryPool or ClarityLibraryStock"
            }
            if (errorMsg?.length()) {
                def fullErrorMsg = "Please correct the errors below " << "$it:$ClarityProcess.WINDOWS_NEWLINE" << errorMsg
                ClarityProcess.postErrorMessage(fullErrorMsg as String)
            }
        }
    }

    void validateInputsContainerType(List<ContainerNode> containerNodes = process.inputAnalytes.collect { it.containerNode }.unique()) {
        List<ContainerNode> plateContainerNodes = containerNodes.findAll{it.isPlate}?.unique()
        if ( plateContainerNodes && plateContainerNodes.size() != 1) {
            process.postErrorMessage("""
                Process can only run on a single ${ContainerTypes.WELL_PLATE_96.value}.
                Please select a single ${ContainerTypes.WELL_PLATE_96.value} as the input.
            """)
        }
    }

    static void validateUdfs(analyte, def errorMsg) {
        if (analyte.udfVolumeUl == null) {
            errorMsg << "$ClarityUdf.ANALYTE_VOLUME_UL is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (analyte.udfLibraryMolarityQcPm == null) {
            errorMsg << "$ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM.value is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (analyte.udfConcentrationNgUl == null) {
            errorMsg << "$ClarityUdf.ANALYTE_CONCENTRATION_NG_UL is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (analyte.udfDegreeOfPooling == null) {
            errorMsg << "$ClarityUdf.ANALYTE_DEGREE_POOLING is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (analyte.udfActualTemplateSizeBp == null) {
            errorMsg << "$ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (analyte.maxLcAttempt == null || analyte.maxLcAttempt < 1) {
            errorMsg << "LC attempt is required and must be >= 1" << ClarityProcess.WINDOWS_NEWLINE
        }
    }
}
