package gov.doe.jgi.pi.pps.dapseq

import doe.jgi.pi.pps.dapseq.couch.CouchDb
import doe.jgi.pi.pps.dapseq.couch.CouchDbResponse
import doe.jgi.pi.pps.dapseq.parser.DapSeqJsonSubmission
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import groovy.util.logging.Slf4j
import org.apache.http.HttpStatus

@Slf4j
@Transactional
class DapSeqJsonSubmissionService {

    DapSeqJsonRetrievalService dapSeqJsonRetrievalService

    CouchDb getCouchDb() {
        dapSeqJsonRetrievalService.couchDb
    }

    String submitJsonForDapSeqLibraryPool(DapSeqJsonSubmission submission, ClarityLibraryPool dapSeqPool, Long submittedByContactId) {
        //note, submission should have been previously validated
        if (!submission.passedValidation) {
            throw new WebException("JSON submission for ${dapSeqPool} is not marked as having passed validation.", 422)
        }
        if(submittedByContactId) {
            submission.setSubmittedByContactId(submittedByContactId)
        }
        submission.setDapSeqPoolLimsId(dapSeqPool.id)
        submission.setDapSeqPoolName(dapSeqPool.name)
        submission.setSubmissionDate()
        //submission.setLimsScheduledSampleIds(dapSeqPool.parentScheduledSampleIds)
        //submission.setLimsPmoSampleIds(dapSeqPool.parentPmoSampleIds)

        CouchDb couchDb = getCouchDb()
        CouchDbResponse response = couchDb.insert(submission.json)
        if(response.statusLine.statusCode != HttpStatus.SC_CREATED) {
            log.error "error submitting JSON ${response}:\n${submission.json.toJSONString()}"
            throw new WebException("unexpected response from couchdb insert ${response}",response.statusCode)
        }

        String documentId = response.json.'id'
        if (!documentId) {
            throw new WebException("document ID not extracted from CouchDb response JSON: ${response.json.toJSONString()}",500)
        }
        String documentUrl = "${couchDb.databaseUrl}${documentId}"
        log.info "saved dap-seq JSON submission for ${dapSeqPool} to ${documentUrl}"
        documentUrl
    }

}
