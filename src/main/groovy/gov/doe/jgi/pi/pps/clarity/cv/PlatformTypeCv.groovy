package gov.doe.jgi.pi.pps.clarity.cv

import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseInsensitive

public enum PlatformTypeCv {
    ILLUMINA('Illumina'),
    PACBIO('PacBio'),
	OXFORD('Oxford')
	
	private final String value
	
	private PlatformTypeCv(String value) {
		this.value = value
	}

	public String getValue() {
		return this.value
	}
	
	public String toString() {
		return value
	}
	
	private static final EnumConverterCaseInsensitive<PlatformTypeCv> converter = new EnumConverterCaseInsensitive(PlatformTypeCv)
	
	public static PlatformTypeCv toEnum(value) {
		return converter.toEnum(value)
	}

	static int toSortIndex(value) {
		if (PACBIO.value == value)
			return 1
		return -1
	}

}
