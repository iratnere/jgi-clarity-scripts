package gov.doe.jgi.pi.pps.clarity.web_transaction

import gov.doe.jgi.pi.pps.clarity.grails.ConfigurationService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransaction
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransactionChildSession
import gov.doe.jgi.pi.pps.webtransaction.util.ExternalId
import groovy.util.logging.Slf4j


/**
 * Created by dscott on 11/20/2015.
 */
@Slf4j
class NodeManagerSession extends WebTransactionChildSession<NodeManager> {

    private boolean isOpen = false
    private NodeManager nodeManager
    final boolean testMode
    private final String username
    private final String password
    private final boolean useUsername

    NodeManagerSession(username, password) {
        this.testMode = false
        this.username = username?.toString()
        this.password = password?.toString()
        this.useUsername = true
    }

    NodeManagerSession(boolean testMode) {
        this.testMode = testMode
        this.username = null
        this.password = null
        this.useUsername = false
    }

    synchronized void openSession(WebTransaction webTransaction) {
        if (isOpen) {
            return
        }
        ConfigurationService configurationService = (ConfigurationService) webTransaction.requireApplicationBean(ConfigurationService)
        NodeConfig nodeConfig = useUsername ?  configurationService.userNodeConfig(username,password) : configurationService.clarityNodeConfig
        Map keyValues = new HashMap()
        keyValues[WebTransaction.TRANSACTION_ID] = webTransaction.transactionId
        ExternalId externalId = webTransaction.externalId
        if (externalId) {
            keyValues[externalId.type] = externalId.id
        }
        nodeManager = new NodeManager(nodeConfig, webTransaction.transactionId, keyValues)
        nodeManager.testMode = testMode
        isOpen = true
    }

    synchronized void closeSession() {
        try {
            nodeManager?.close()
        } finally {
            isOpen = false
        }
    }

    NodeManager getSession() {
        return nodeManager
    }

    boolean getIsOpen() {
        return isOpen
    }

}
