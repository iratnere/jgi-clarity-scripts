package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules

/**
 * Created by datjandra on 7/6/2015.
 */
class EqualCondition extends Condition {

    private Object key
    private Object value

    EqualCondition(Object key, Object value) {
        if (key == null) {
            throw new IllegalArgumentException('Condition requires attribute key')
        }

        if (value == null) {
            throw new IllegalArgumentException('Condition requires accepted value')
        }

        this.key = key
        this.value = value
    }

    @Override
    boolean evaluate(ObjectWithDynamicAttributes percept) {
        return value.equals(percept.getAttribute(key))
    }

    @Override
    public String toString() {
        return "$key = $value"
    }
}
