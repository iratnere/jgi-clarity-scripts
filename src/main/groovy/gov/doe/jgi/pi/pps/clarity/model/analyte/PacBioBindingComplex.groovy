package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.util.LabelBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import groovy.transform.PackageScope

/**
 * Created by lvishwas on 4/6/2015.
 */
class PacBioBindingComplex  extends Analyte{

    @PackageScope
    PacBioBindingComplex(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
        artifactNodeInterface.setName(artifactNodeInterface.singleParentAnalyte.name.replace('la', 'lb'))
    }

    PacBioAnnealingComplex getParentAnalyte(){
        ArtifactNodeInterface pacBioAnnealingComplex = artifactNodeInterface.singleParentAnalyte
        return (PacBioAnnealingComplex)AnalyteFactory.analyteInstance(pacBioAnnealingComplex)
    }

    String getUdfBindingKitBoxBarcode() {
        return artifactNodeInterface.parentProcessNode.getUdfAsString(ClarityUdf.PROCESS_BINDING_KIT_BOX_BARCODE.udf)
    }

    BigDecimal getUdfPacBioMolarityNm() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_PACBIO_MOLARITY_NM.udf)
    }

    void setUdfPacBioMolarityNm(BigDecimal pacBioMolarity) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_PACBIO_MOLARITY_NM.udf, pacBioMolarity)
    }

    @Override
    String getPrintLabelText() {
        if (isOnPlate) {
            return plateLabelBean() as String
        }
        return new LabelBean(
                pos_1_1: containerName,
                pos_1_2: name,
                pos_1_3: claritySample.sequencingProject.udfMaterialCategory,
                pos_1_5: containerName,
                pos_2_2: name
        ) as String
    }

    LabelBean plateLabelBean(String prefix = '') {
        return new LabelBean(
                pos_1_1: "$prefix$containerId",
                pos_1_2: claritySample.containerUdfLabel,
                pos_1_4: claritySample.sequencingProject.udfMaterialCategory,
                pos_1_5: containerId
        )
    }

    static List<ProcessType> getParentLibrariesProcessTypes() {
        return [ProcessType.LC_LIBRARY_CREATION, ProcessType.LP_POOL_CREATION, ProcessType.LC_PLATE_TRANSFER]
    }

    @Override
    void setNewRunMode(String runMode) {
        ancestorsForProcessTypes(parentLibrariesProcessTypes).each { Analyte parentLibrary ->
            if (this.is(parentLibrary)) { //this should never happen: infinite recursion protection
                return
            }
            parentLibrary.setNewRunMode(runMode)
        }
    }

    @Override
    Analyte getParentLibrary() {
        PacBioAnnealingComplex annealingComplexParent = parentAnalyte
        return annealingComplexParent.parentAnalyte //could be ClarityLibraryStock or ClarityLibraryPool
    }
}
