package gov.doe.jgi.pi.pps.clarity.grails

import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.util.LogTiming
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import grails.core.GrailsApplication
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.Method
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class StatusService {

    Logger logger = LoggerFactory.getLogger(StatusService.class.name)

    GrailsApplication grailsApplication

    OnDemandCache<Boolean> cachedPostStatusChanges = new OnDemandCache<>()


    Boolean getPostStatusChanges() {
        cachedPostStatusChanges.fetch {
            String enabledString = grailsApplication.config.getProperty('statusService.postStatusChanges')
            Boolean enabled = false
            if (enabledString) {
                enabled = Boolean.valueOf(enabledString)
            }
            return enabled
        }
    }

    String getSowItemStatusUpdateUrl() {
        "${grailsApplication.config.getProperty('sowItemStatusUpdate.url')}"
    }

    String getAbandonSowItemsUrl() {
        "${grailsApplication.config.getProperty('sowItemsAbandon.url')}"
    }

    String getCompleteSowItemsUrl() {
        "${grailsApplication.config.getProperty('sowItemsComplete.url')}"
    }

    String getSampleStatusUpdateUrl() {
        "${grailsApplication.config.getProperty('sampleStatusUpdate.url')}"
    }

    String getSowItemStatusGetUrl() {
        "${grailsApplication.config.getProperty('sowItemStatusGet.url')}"
    }

    String getSampleStatusGetUrl() {
        "${grailsApplication.config.getProperty('sampleStatusGet.url')}"
    }

    void submitSampleStatus(Map<Long, SampleStatusCv> sampleStatusMap, Long submittedByContactId = null) {
        if (!sampleStatusMap) {
            logger.warn "empty sample status map for update"
            return
        }
        JSONObject submissionJson = new JSONObject()
        submissionJson['submitted-by'] = submittedByContactId ?: ClarityWebTransaction.currentTransaction?.submittedBy

        JSONObject statusJson = new JSONObject()
        submissionJson['sample-status'] = statusJson
        sampleStatusMap.each { Long sampleId, SampleStatusCv status ->
            statusJson[sampleId] = status.value
        }
        if (!getPostStatusChanges()) {
            logger.warn "posting of status changes disabled in current environment, skipping sample status update:\n${submissionJson.toString(2)}"
            return
        }
        if (!submissionJson['submitted-by']) {
            throw new RuntimeException("no contact ID submitted and contact ID not extracted from WebTransaction")
        }
        logger.warn "update sample status JSON:\n${submissionJson.toString(2)}"
        HTTPBuilder http = new HTTPBuilder(sampleStatusUpdateUrl)
        Long startTime = System.currentTimeMillis()
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = submissionJson.toString()
            response.success = { HttpResponseDecorator resp, respJson ->
                log.warn "sample status update success response json: ${respJson}"
            }
            response.failure = {resp, reader ->
                String errMessage = "sample status update failure [${resp.status}] : ${reader}"
                logger.error errMessage
                throw new RuntimeException(errMessage)
            }
        }
        Long endTime = System.currentTimeMillis()
        LogTiming.logMethodCall(startTime,endTime,'WorkflowStatusService.submitSampleStatus',[count:sampleStatusMap.size()])
    }

    void submitSowItemStatus(Map<Long, SowItemStatusCv> sowItemStatusMap, Long submittedByContactId = null) {
        if (!sowItemStatusMap) {
            logger.warn "empty SOW status map for update"
            return
        }
        JSONObject submissionJson = new JSONObject()
        submissionJson['submitted-by'] = submittedByContactId ?: (ClarityWebTransaction.currentTransaction?.submittedBy as Long)

        JSONObject statusJson = new JSONObject()
        submissionJson['sow-item-status'] = statusJson
        sowItemStatusMap.each { Long sowItemId, SowItemStatusCv status ->
            statusJson[sowItemId] = status.value
        }
        if (!getPostStatusChanges()) {
            logger.warn "posting of status changes disabled in current environment, skipping sow-item status update:\n${submissionJson.toString(2)}"
            return
        }
        if (!submissionJson['submitted-by']) {
            throw new RuntimeException("no contact ID submitted and contact ID not extracted from WebTransaction")
        }
        logger.warn "update sow-item status JSON:\n${submissionJson.toString(2)}"
        HTTPBuilder http = new HTTPBuilder(sowItemStatusUpdateUrl)
        Long startTime = System.currentTimeMillis()
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = submissionJson.toString()
            response.success = { HttpResponseDecorator resp, respJson ->
                log.warn "sow-item status update success response json: ${respJson}"
            }
            response.failure = {resp, reader ->
                String errMessage = "sow-item update failure [${resp.status}] : ${reader}"
                logger.error errMessage
                throw new RuntimeException(errMessage)
            }
        }
        Long endTime = System.currentTimeMillis()
        LogTiming.logMethodCall(startTime,endTime,'WorkflowStatusService.submitSowItemStatus',[count:sowItemStatusMap.size()])
    }

    Map<Long,String> getSowItemStatusBatch(Collection sowItemIds) {
        HTTPBuilder http = new HTTPBuilder(sowItemStatusGetUrl)
        JSONArray sowItemIdJson = new JSONArray(sowItemIds)
        Map<Long,String> responseMap = [:]
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = sowItemIdJson.toString()
            response.success = { HttpResponseDecorator resp, Map respMap ->
                respMap.each { key, val ->
                    responseMap[key as Long] = val as String
                }
            }
        }
        responseMap
    }

    Map<Long,String> getSampleStatusBatch(Collection sampleIds) {
        HTTPBuilder http = new HTTPBuilder(sampleStatusGetUrl)
        JSONArray sampleIdJson = new JSONArray(sampleIds)
        Map<Long,String> responseMap = [:]
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = sampleIdJson.toString()
            response.success = { HttpResponseDecorator resp, Map respMap ->
                respMap.each { key, val ->
                    responseMap[key as Long] = val as String
                }
            }
        }
        responseMap
    }

    String getSampleStatus(Long pmoSampleId) {
        if (pmoSampleId == null) {
            logger.warn "null sample ID submitted to retrieve sample status"
            return null
        }
        Map<Long,String> statusMap = getSampleStatusBatch([pmoSampleId])
        return statusMap[pmoSampleId]
    }

    String getSowItemStatus(Long sowItemId) {
        if (sowItemId == null) {
            logger.warn "null sow-item ID submitted to retrieve sow-item status"
            return null
        }
        Map<Long,String> statusMap = getSowItemStatusBatch([sowItemId])
        return statusMap[sowItemId]
    }

    void abandonSowItems(List<Long> sowItemIds, long submittedByContactId, String doPlate='N'){
        if(!sowItemIds)
            return
        JSONObject submissionJson = new JSONObject()
        submissionJson['submitted-by'] = submittedByContactId ?: (ClarityWebTransaction.currentTransaction?.submittedBy as Long)
        submissionJson['sow-item-ids'] = sowItemIds
        submissionJson['do-plate'] = doPlate
        if (!submissionJson['submitted-by']) {
            throw new RuntimeException("no contact ID submitted and contact ID not extracted from WebTransaction")
        }
        logger.warn "abandon sow-items JSON:\n${submissionJson.toString(2)}"
        HTTPBuilder http = new HTTPBuilder(abandonSowItemsUrl)
        Long startTime = System.currentTimeMillis()
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = submissionJson.toString()
            response.success = { HttpResponseDecorator resp, respJson ->
                log.warn "Abandon sow-item success response json: ${respJson}"
            }
            response.failure = {resp, reader ->
                String errMessage = "Abandon sow-item failure [${resp.status}] : ${reader}"
                logger.error errMessage
                throw new RuntimeException(errMessage)
            }
        }
        Long endTime = System.currentTimeMillis()
        LogTiming.logMethodCall(startTime,endTime,'WorkflowStatusService.abandonSowItems',[count:sowItemIds.size()])
    }

    void completeSowItems(List<Long> sowItemIds, long submittedByContactId){
        if(!sowItemIds)
            return
        JSONObject submissionJson = new JSONObject()
        submissionJson['submitted-by'] = submittedByContactId ?: (ClarityWebTransaction.currentTransaction?.submittedBy as Long)
        submissionJson['sow-item-ids'] = sowItemIds
        if (!submissionJson['submitted-by']) {
            throw new RuntimeException("no contact ID submitted and contact ID not extracted from WebTransaction")
        }
        logger.warn "abandon sow-items JSON:\n${submissionJson.toString(2)}"
        HTTPBuilder http = new HTTPBuilder(completeSowItemsUrl)
        Long startTime = System.currentTimeMillis()
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = submissionJson.toString()
            response.success = { HttpResponseDecorator resp, respJson ->
                log.warn "Complete sow-item success response json: ${respJson}"
            }
            response.failure = {resp, reader ->
                String errMessage = "Complete sow-item failure [${resp.status}] : ${reader}"
                logger.error errMessage
                throw new RuntimeException(errMessage)
            }
        }
        Long endTime = System.currentTimeMillis()
        LogTiming.logMethodCall(startTime,endTime,'WorkflowStatusService.completeSowItems',[count:sowItemIds.size()])
    }

}
