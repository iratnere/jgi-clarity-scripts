package gov.doe.jgi.pi.pps.couchdb.transaction

import gov.doe.jgi.pi.pps.couchdb.grails.CouchDbUtilityService
import gov.doe.jgi.pi.pps.couchdb.util.CouchDb
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransaction
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransactionRecorder
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransactionUtil
import grails.util.GrailsUtil
import groovy.util.logging.Slf4j
import org.grails.web.json.JSONObject

/**
 * Created by dscott on 2/11/2015.
 */
@Slf4j
class CouchdbWebTransactionRecorder implements WebTransactionRecorder {

    final CouchDb couchDb
    boolean enabled = true
    final JSONObject logRecord = new JSONObject()

    CouchdbWebTransactionRecorder(CouchDb couchDb) {
        this.couchDb = couchDb
    }

    @Override
    void recordSuccess(WebTransaction webTransaction) {
        if (!enabled) {
            return
        }
        CouchDbUtilityService couchDbUtilityService = (CouchDbUtilityService) webTransaction.requireApplicationBean(CouchDbUtilityService)
        try {
            couchDbUtilityService.logWebTransaction(webTransaction,this)
        } catch (t) {
            log.error "error logging ${webTransaction} in couchdb", GrailsUtil.sanitizeRootCause(t)
        }
    }

    @Override
    void recordFailure(WebTransaction webTransaction) {
        if (!enabled) {
            return
        }
        CouchDbUtilityService couchDbUtilityService = (CouchDbUtilityService) webTransaction.requireApplicationBean(CouchDbUtilityService)
        try {
            couchDbUtilityService.logWebTransaction(webTransaction,this)
        } catch (t) {
            log.error "error logging ${webTransaction} in couchdb", GrailsUtil.sanitizeRootCause(t)
        }
    }

    final JSONObject couchDbLogRecord(WebTransaction webTransaction) {
        logRecord.putAll(WebTransactionUtil.webTransactionToJsonObject(webTransaction))
        logRecord
    }

}
