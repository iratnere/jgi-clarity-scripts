package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules

/**
 * Created by datjandra on 7/6/2015.
 */
class NegationCondition extends Condition {

    private Condition condition

    NegationCondition(Condition condition) {
        this.condition = condition
    }

    @Override
    boolean evaluate(ObjectWithDynamicAttributes percept) {
        return !(condition.evaluate(percept))
    }

    @Override
    public String toString() {
        return condition?.toString()
    }
}
