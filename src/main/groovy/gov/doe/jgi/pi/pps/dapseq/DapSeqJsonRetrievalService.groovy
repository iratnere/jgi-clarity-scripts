package gov.doe.jgi.pi.pps.dapseq

import doe.jgi.pi.pps.dapseq.couch.CouchDb
import doe.jgi.pi.pps.dapseq.couch.CouchDbResponse
import doe.jgi.pi.pps.dapseq.parser.DapSeqJsonSubmission
import duncanscott.org.groovy.utils.json.util.DateUtil
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import grails.core.GrailsApplication
import groovy.util.logging.Slf4j
import org.apache.http.HttpStatus
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.json.simple.JSONValue

import java.text.DateFormat
import java.text.SimpleDateFormat

@Slf4j
class DapSeqJsonRetrievalService {

    GrailsApplication grailsApplication

    CouchDb getCouchDb() {
        String couchDbUrl = grailsApplication.config.getProperty('couchdb.dapseq.url')
        String username = grailsApplication.config.getProperty('couchdb.dapseq.username')
        String password = grailsApplication.config.getProperty('couchdb.dapseq.password')
        return new CouchDb(couchDbUrl,username,password)
    }

    CouchDbResponse getSchemas() {
        String designDocument = 'schema'
        String viewName = 'schemas'
        Map params = ['descending':true]
        CouchDbResponse couchDbResponse = couchDb.getFromView(designDocument,viewName,params)
        if (couchDbResponse.success) {
            return couchDbResponse
        }
        throw new WebException("error querying view ${couchDbResponse}", couchDbResponse.statusCode)
    }

    private CouchDbResponse extractResponseForId(String designDocument, String viewName, Map params, String notFoundErrorMessage = 'nothing found') {
        CouchDbResponse couchDbResponse = couchDb.getFromView(designDocument,viewName,params)
        if (couchDbResponse.success) {
            String docId = couchDbResponse.json.rows?.find()?.id
            if (docId) {
                CouchDbResponse docResponse = couchDb.get(docId)
                if (docResponse.success) {
                    return  docResponse
                }
                throw new WebException("error retrieving document ${docResponse}", docResponse.statusCode)
            }
            throw new WebException(notFoundErrorMessage, 404)
        }
        throw new WebException("error querying view ${couchDbResponse}", couchDbResponse.statusCode)
    }

    private CouchDbResponse getSchemaForId(String schemaId) {
        String designDocument = 'schema'
        String viewName = 'schema-id'
        Map params = ['key': JSONValue.toJSONString(schemaId)]
        extractResponseForId(designDocument, viewName, params, "no schema found for \$id [${schemaId}]")
    }


    CouchDbResponse getLatestSchema() {
        String designDocument = 'schema'
        String viewName = 'schemas'
        Map params = ['descending':true, 'limit':1]
        extractResponseForId(designDocument, viewName, params, 'no schema registered')
    }


    JSONArray retrieveSchemas() {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        CouchDbResponse couchDbResponse = schemas
        webTransaction.statusCode = couchDbResponse.statusCode
        webTransaction.jsonResponse['couchdb-response'] = JsonUtil.toJsonArray(couchDbResponse.json.rows)
    }


    JSONObject retrieveSchemaForId(String schemaId) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        CouchDbResponse couchDbResponse = getSchemaForId(schemaId)
        webTransaction.statusCode = couchDbResponse.statusCode
        webTransaction.jsonResponse['couchdb-response'] = JsonUtil.toJsonObject(couchDbResponse.json)
    }


    JSONObject retrieveLatestSchema() {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        CouchDbResponse couchDbResponse = latestSchema
        webTransaction.statusCode = couchDbResponse.statusCode
        webTransaction.jsonResponse['couchdb-response'] = JsonUtil.toJsonObject(couchDbResponse.json)
    }


    JSONArray retrieveViewRowsSortedByInsertDate(String designDocument, String viewName, String key) {
        ClarityWebTransaction clarityWebTransaction = ClarityWebTransaction.requireCurrentTransaction()
        JSONObject transactionParams = new JSONObject()
        clarityWebTransaction.jsonResponse.'couchdb-queries' = transactionParams

        JSONArray json = new JSONArray()
        CouchDb couchDb = getCouchDb()
        Map params = ['key': JSONValue.toJSONString(key)]
        CouchDbResponse couchDbResponse = couchDb.getFromView(designDocument,viewName,params)
        transactionParams["${viewName}"] = couchDbResponse.toJson()

        if (!couchDbResponse.success) {
            throw new WebException("error querying view ${couchDbResponse}", couchDbResponse.statusCode)
        }
        List<org.json.simple.JSONObject> documents = []
        couchDbResponse.json.rows?.each { org.json.simple.JSONObject row ->
            CouchDbResponse docResponse = couchDb.get((String) row.id)
            transactionParams["${row.id}"] = docResponse.toJson()
            if (docResponse.success) {
                documents << docResponse.json
            } else {
                throw new WebException("error retrieving view document ${docResponse}", docResponse.statusCode)
            }
        }
        //sort documents by insert-date descending
        documents.sort { org.json.simple.JSONObject row ->
            DapSeqJsonSubmission submission = DapSeqJsonSubmission.newSubmission(row)
            submission.submissionDate
        }.reverse().each { org.json.simple.JSONObject row ->
            json << new JSONObject(row.toJSONString())
        }
        json
    }

    JSONArray submissionsForDemultiplexedLibraryName(String demultiplexedLibraryName) {
        String designDocument = 'lookup'
        String viewName = 'demultiplexed-library-name'
        JSONArray allDocs = retrieveViewRowsSortedByInsertDate(designDocument,viewName,demultiplexedLibraryName)
        Map<String,Object> poolIdDocument = [:] //linked hash map of string,JSONObject
        for (doc in allDocs) {
            DapSeqJsonSubmission submission = DapSeqJsonSubmission.newSubmission(doc)
            if (submission?.dapSeqPoolLimsId) {
                if (!poolIdDocument.containsKey(submission.dapSeqPoolLimsId)) {
                    poolIdDocument[submission.dapSeqPoolLimsId] = doc
                }
            }
        }
        JSONArray selectedDocs = new JSONArray()
        if (poolIdDocument) {
            selectedDocs.addAll(poolIdDocument.values())
        }
        selectedDocs
    }

    JSONArray submissionsForDapSeqPoolLimsId(String dapSeqPoolLimsId) {
        String designDocument = 'lookup'
        String viewName = 'library-limsid'
        retrieveViewRowsSortedByInsertDate(designDocument,viewName,dapSeqPoolLimsId)
    }

    JSONArray submissionsForDapSeqPoolName(String dapSeqPoolName) {
        String designDocument = 'lookup'
        String viewName = 'library-name'
        retrieveViewRowsSortedByInsertDate(designDocument,viewName,dapSeqPoolName)
    }

    void retrieveSubmissionsForDemultiplexedLibraryName(String demultiplexedLibraryName) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        JSONArray submissions = submissionsForDemultiplexedLibraryName(demultiplexedLibraryName)
        webTransaction.jsonResponse['dap-seq-json'] = submissions
        if (submissions) {
            webTransaction.statusCode = HttpStatus.SC_OK
        } else {
            webTransaction.statusCode = HttpStatus.SC_NOT_FOUND
        }

    }

    void retrieveSubmissionForDapSeqPoolLimsId(String dapSeqPoolLimsId) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        JSONArray submissions = submissionsForDapSeqPoolLimsId(dapSeqPoolLimsId)
        if (submissions) {
            webTransaction.jsonResponse['dap-seq-json'] = submissions[0]
            webTransaction.statusCode = HttpStatus.SC_OK
        } else {
            webTransaction.jsonResponse['dap-seq-json'] = new JSONObject()
            webTransaction.statusCode = HttpStatus.SC_NOT_FOUND
        }
    }

    void retrieveSubmissionForDapSeqPoolName(String dapSeqPoolName) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        JSONArray submissions = submissionsForDapSeqPoolName(dapSeqPoolName)
        if (submissions) {
            webTransaction.jsonResponse['dap-seq-json'] = submissions[0]
            webTransaction.statusCode = HttpStatus.SC_OK
        } else {
            webTransaction.jsonResponse['dap-seq-json'] = new JSONObject()
            webTransaction.statusCode = HttpStatus.SC_NOT_FOUND
        }
    }

    private static final Integer submissionDateLimit = 50
    private static final Map submissionDateViewParams = [
            'limit':submissionDateLimit,
            'descending':true
    ]

    private static String localDateFormatText = 'yyyy-MMM-dd HH:mm:ss.SSS z'
    private static final DateFormat localDateFormat = new SimpleDateFormat(localDateFormatText)
    static {
        localDateFormat.setTimeZone(TimeZone.getTimeZone('PST'))
    }

    JSONArray latestSubmissions(Map queryParams) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        JSONObject transactionParams = new JSONObject()
        webTransaction.jsonResponse.'couchdb-queries' = transactionParams

        String designDocument = 'lookup'
        String viewName = 'submission-date_library-limsid'
        JSONArray json = new JSONArray()
        Map params = new HashMap<>()
        params.putAll(submissionDateViewParams)
        if (queryParams) {
            params.putAll(queryParams)
        }
        if (queryParams['limit']?.toString()?.isInteger()) {
            params['limit'] = queryParams['limit'].toString()
        } else {
            params['limit'] = submissionDateLimit
        }
        CouchDb couchDb = getCouchDb()
        CouchDbResponse couchDbResponse = couchDb.getFromView(designDocument,viewName,params)
        transactionParams["${viewName}"] = couchDbResponse.toJson()

        couchDbResponse.json.rows?.each { org.json.simple.JSONObject row ->
            JSONObject responseRow = new JSONObject(row.value) //put all elements of value object
            responseRow['id'] = row.'id'
            responseRow['key'] = row.'key'
            responseRow['submission-date-pst'] = localDateFormat.format(DateUtil.stringToDate(row.'key' as String))
            json << responseRow
        }
        json
    }

    void retrieveLatestSubmission(Map queryParams) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        JSONArray submissions = latestSubmissions(queryParams)
        webTransaction.jsonResponse['dap-seq-json'] = submissions
        if (submissions) {
            webTransaction.statusCode = HttpStatus.SC_OK
        } else {
            webTransaction.statusCode = HttpStatus.SC_NOT_FOUND
        }
    }


    void retrieveSubmission(String documentId) {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        CouchDbResponse couchDbResponse = couchDb.get(documentId)
        webTransaction.statusCode = couchDbResponse.statusCode
        if (webTransaction.statusCode != HttpStatus.SC_OK) {
            if (webTransaction.statusCode == HttpStatus.SC_NOT_FOUND) {
                webTransaction.exitWithErrorMessage("no dap-seq document found for ID [${documentId}]")
            }
            webTransaction.exitWithErrorMessage(couchDbResponse.reasonPhrase)
        }
        webTransaction.jsonResponse['couchdb-response'] = JsonUtil.toJsonObject(couchDbResponse.json)
    }

}
