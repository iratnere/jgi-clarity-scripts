package gov.doe.jgi.pi.pps.webtransaction.transaction

interface WebTransactionRecorder {

    void recordSuccess(WebTransaction webTransaction)
    void recordFailure(WebTransaction webTransaction)

}
