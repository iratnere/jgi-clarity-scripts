package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import groovy.transform.PackageScope

/**
 * Created by datjandra on 4/4/2016.
 */
class ControlSampleAnalyte extends SampleAnalyte {

    @PackageScope
    ControlSampleAnalyte(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
    }

    @Override
    void setUdfConcentrationNgUl(BigDecimal concentration) {
        throw new UnsupportedOperationException("setUdfConcentrationNgUl() not allowed for control")
    }

    @Override
    void setUdfVolumeUl(BigDecimal volume) {
        throw new UnsupportedOperationException("setUdfVolumeUl() not allowed for control")
    }

    @Override
    void setContainerUdfNotes(String containerUdfNotes) {
        throw new UnsupportedOperationException("setContainerUdfNotes() not allowed for control")
    }

    @Override
    void setContainerUdfLibraryQcFailureMode(String containerUdfLibraryQcFailureMode) {
        throw new UnsupportedOperationException("setContainerUdfLibraryQcFailureMode() not allowed for control")
    }

    @Override
    void setContainerUdfLibraryQcResult(String containerUdfLibraryQcResult) {
        throw new UnsupportedOperationException("setContainerUdfLibraryQcResult() not allowed for control")
    }

    @Override
    void setContainerUdfFinalAliquotMassNg(BigDecimal containerUdfFinalAliquotMassNg) {
        throw new UnsupportedOperationException("setContainerUdfFinalAliquotMassNg() not allowed for control")
    }

    @Override
    void setContainerUdfLabel(String containerUdfLabel) {
        throw new UnsupportedOperationException("setContainerUdfLabel() not allowed for control")
    }
}
