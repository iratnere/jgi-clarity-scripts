package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules

/**
 * Created by datjandra on 7/6/2015.
 */
class Rule {

    private Condition condition
    private Action action

    Rule(Condition condition, Action action) {
        if (null == condition) {
            throw new IllegalArgumentException('Condition must not be null')
        }

        if (null == action) {
            throw new IllegalArgumentException('Action must not be null')
        }

        this.condition = condition
        this.action = action
    }

    boolean evaluate(ObjectWithDynamicAttributes p) {
        return (condition.evaluate(p))
    }

    Action getAction() {
        return action
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Rule)) {
            return super.equals(o)
        }
        return (toString().equals(((Rule) o).toString()))
    }

    @Override
    public int hashCode() {
        return toString().hashCode()
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
        return sb.append("if ").append(condition).append(" then ").append(action)
                .append(".").toString()
    }
}
