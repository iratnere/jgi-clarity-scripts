package gov.doe.jgi.pi.pps.webtransaction.grails

import gov.doe.jgi.pi.pps.util.exception.HttpStatusCodeReporter
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransaction
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransactionAppender
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransactionExit
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransactionRecorder
import gov.doe.jgi.pi.pps.webtransaction.util.RequestUtils
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import grails.util.GrailsUtil
import groovy.util.logging.Slf4j
import org.grails.web.json.JSONElement
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.grails.web.util.WebUtils
import org.slf4j.MDC
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.transaction.annotation.Propagation

import javax.servlet.http.HttpServletRequest

@Slf4j
@Transactional
class WebTransactionService {

    ExecutionService executionService
    MessageSource messageSource
    GrailsApplication grailsApplication

    WebTransactionAppender webTransactionAppender = new WebTransactionAppender()

    private static void processError(WebTransaction webTransaction, Throwable t) {
        Throwable rootCause = GrailsUtil.sanitizeRootCause(t)
        if (rootCause instanceof WebTransactionExit) {
            if (((WebTransactionExit) rootCause).httpStatusCode) {
                webTransaction.statusCode = ((WebTransactionExit) rootCause).httpStatusCode
            }
        } else {
            webTransaction.statusCode = 500
            if (rootCause instanceof HttpStatusCodeReporter) {
                if (rootCause.httpStatusCode) {
                    webTransaction.statusCode = rootCause.httpStatusCode
                }
            }
            webTransaction.addExceptionErrorMessage(rootCause)
        }
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    void performTransaction(WebTransaction webTransaction, WebTransactionRecorder webTransactionRecorder, Closure action) {
        try {
            MDC.put(WebTransaction.WEB_TRANSACTION,webTransaction.transactionId)
            if (webTransaction.externalId) {
                MDC.put(WebTransaction.EXTERNAL_ID, "${webTransaction.externalId}")
            }
            webTransaction.webTransactionHeaders?.each {String headerName, String headerValue ->
                MDC.put(headerName,headerValue)
            }

            webTransaction.messageSource = messageSource
            webTransaction.locale = LocaleContextHolder.getLocale()
            webTransaction.grailsApplication = grailsApplication
            if (webTransactionRecorder) {
                webTransaction.webTransactionRecorder = webTransactionRecorder
            }
            if (webTransaction.extractJsonSubmission) {
                GrailsWebRequest grailsRequest = WebUtils.retrieveGrailsWebRequest()
                HttpServletRequest currentRequest = grailsRequest.getCurrentRequest()
                JSONElement submission = RequestUtils.extractJsonElement(currentRequest)
                webTransaction.jsonSubmission = submission
            }
            if (!webTransaction.opened) {
                webTransaction.open()
            }
            webTransactionAppender?.addWebTransaction(webTransaction)
            executionService.execute(webTransaction, action)
        } catch (t) {
            try {
                processError(webTransaction, t)
                webTransaction.generateJsonResponse()
            } finally {
                try {
                    webTransactionRecorder?.recordFailure(webTransaction)
                } finally {
                    try {
                        webTransaction.close()
                    } finally {
                        webTransactionAppender?.removeWebTransaction()
                    }
                }
            }
        } finally {
            MDC.clear()
        }
    }

}
