package gov.doe.jgi.pi.pps.webtransaction.util

import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject

class JsonResponse extends JSONObject {

	public static final String ERRORS = 'errors'
	private boolean errorsInitialized = false

    JsonResponse() {
		super()
	}
	
	private void intitiateErrorsArray() {
		if(errorsInitialized) {
			return
		}
		synchronized(this) {
			if (!errorsInitialized) {
				this[ERRORS] = new JSONArray()
				errorsInitialized = true
			}
		}
	}
	
	void addErrorMessage(JsonErrorMessage errorMessage) {
		if (errorMessage) {
			intitiateErrorsArray()
			((Collection<JsonErrorMessage>) this[ERRORS]).add(errorMessage)
		}
	}
	
	void addErrorMessages(Collection<JsonErrorMessage> errorMessages) {
		if (errorMessages) {
			for (JsonErrorMessage errorMessage : errorMessages) {
				addErrorMessage(errorMessage)
			}
		}
	}
}
