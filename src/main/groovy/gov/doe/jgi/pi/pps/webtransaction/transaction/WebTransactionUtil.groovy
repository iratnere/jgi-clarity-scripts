package gov.doe.jgi.pi.pps.webtransaction.transaction

import gov.doe.jgi.pi.pps.util.json.DateUtil
import grails.util.GrailsUtil
import org.grails.web.json.JSONObject
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.grails.web.util.WebUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.context.support.StaticMessageSource
import org.springframework.web.context.request.RequestContextHolder

import javax.servlet.http.HttpServletRequest

/**
 * Created by dscott on 2/18/2016.
 */
class WebTransactionUtil {

    static final ThreadLocal<WebTransaction> threadTestInstance = new ThreadLocal<>()
    static final Logger staticLogger = LoggerFactory.getLogger(WebTransaction.class.name)

    static final String SUBMISSION = 'submission'
    static final String LOG_TIME_STRING = 'log-time-string'
    static final String LOG_TIME_ARRAY = 'log-time-array'
    static final String RESPONSE = 'response'
    static final String HTTP_STATUS_CODE = 'http-status-code'
    static final String LOG = 'log'
    static final String ACTION = 'action'
    static final List<String> zuulHeaders = [
            'zuul-consul-id',
            'zuul-consul-parent-id',
            'zuul-consul-root-id'
    ]

    static JSONObject webTransactionToJsonObject(WebTransaction webTransaction) {
        Date logTime = new Date()
        JSONObject couchDbRecord = new JSONObject()
        JSONObject logData = webTransaction.logData
        if (logData) {
            couchDbRecord.putAll(logData)
        }
        couchDbRecord[SUBMISSION] = webTransaction.jsonSubmission
        couchDbRecord[RESPONSE] = webTransaction.jsonResponse

        if (webTransaction.loggingEnabled) {
            couchDbRecord[LOG] = webTransaction.getLoggerMessages()
        }

        String action = webTransaction.action
        if (action) {
            couchDbRecord[ACTION] = action
        }

        couchDbRecord[WebTransaction.TRANSACTION_ID] = "${webTransaction.transactionId}"
        couchDbRecord[WebTransaction.EXTERNAL_ID] = webTransaction.externalId?.toString()
        couchDbRecord[WebTransaction.START_TIME] = DateUtil.dateToJsonArray(webTransaction.startTime)
        couchDbRecord[WebTransaction.END_TIME] = DateUtil.dateToJsonArray(webTransaction.endTime)
        couchDbRecord[HTTP_STATUS_CODE] = webTransaction.statusCode

        couchDbRecord[LOG_TIME_STRING] = logTime.format(webTransaction.dateFormatString)
        couchDbRecord[LOG_TIME_ARRAY] = DateUtil.dateToJsonArray(logTime)
        return couchDbRecord
    }

    static void generateResponse(WebTransaction webTransaction) {
        webTransaction.endTime = new Date()
        webTransaction.jsonResponse[WebTransaction.END_TIME] = webTransaction.endTimeString
        webTransaction.jsonResponse[WebTransaction.END_TIME_ARRAY] = DateUtil.dateToJsonArray(webTransaction.endTime)
        webTransaction.jsonResponse[WebTransaction.START_TIME_ARRAY] = DateUtil.dateToJsonArray(webTransaction.startTime)
        if (webTransaction.messages) {
            webTransaction.jsonResponse['messages'] = webTransaction.messages
        }
        if (webTransaction.errorMessages) {
            webTransaction.jsonResponse['errors'] = webTransaction.errorMessages
        }
        //webTransaction.jsonResponse['log'] = webTransaction.loggerMessages
        webTransaction.jsonResponse['submitted-by'] = webTransaction.submittedBy
        webTransaction.jsonResponse[WebTransaction.STATUS] = webTransaction.statusCode
    }

    static void setCurrentTransaction(WebTransaction webTransaction) {
        try {
            if (webTransaction.testMode) {
                staticLogger.debug "${webTransaction} attached to thread in test-mode"
                threadTestInstance.set(webTransaction)
                webTransaction.messageSource = new StaticMessageSource()
                webTransaction.locale = LocaleContextHolder.getLocale()
            } else {
                staticLogger.debug "${webTransaction} attached to request"
                GrailsWebRequest grailsRequest = WebUtils.retrieveGrailsWebRequest()
                HttpServletRequest currentRequest = grailsRequest.getCurrentRequest()
                currentRequest.setAttribute(WebTransaction.WEB_TRANSACTION, webTransaction)
                grailsRequest.currentResponse.setHeader(WebTransaction.WEB_TRANSACTION, webTransaction.transactionId)
                zuulHeaders.each{String headerName ->
                    String headerValue = currentRequest.getHeader(headerName)
                    if (headerValue) {
                        webTransaction.webTransactionHeaders[headerName] = headerValue
                        grailsRequest.currentResponse.setHeader(headerName,headerValue)
                    }
                }
            }
            webTransaction.jsonResponse[WebTransaction.TRANSACTION_ID] = "${webTransaction.transactionId}"
            if (webTransaction.externalId) {
                webTransaction[WebTransaction.EXTERNAL_ID] = "${webTransaction.externalId}"
            }
            webTransaction.jsonResponse[WebTransaction.START_TIME] = webTransaction.startTimeString
            webTransaction.childSessions.each() { String key, WebTransactionChildSession child ->
                child.openSession(webTransaction)
            }
        } catch (Throwable t) {
            try {
                webTransaction.close()
            } finally {
                throw t
            }
        }
    }

    static WebTransaction getCurrentTransaction() {
        WebTransaction webTransaction = null
        try {
            GrailsWebRequest grailsRequest = (GrailsWebRequest) RequestContextHolder.currentRequestAttributes()
            HttpServletRequest currentRequest = grailsRequest?.currentRequest
            webTransaction = (WebTransaction) currentRequest.getAttribute(WebTransaction.WEB_TRANSACTION)
        } catch (ignore) {}
        if (!webTransaction) {
            webTransaction = threadTestInstance.get()
        }
        return webTransaction
    }

    static WebTransaction requireCurrentTransaction() {
        WebTransaction webTransaction = currentTransaction
        if (!webTransaction) {
            throw new RuntimeException('no current transaction')
        }
        return webTransaction
    }

    static void close(WebTransaction webTransaction) {
        staticLogger.debug "${webTransaction} closing"
        if(webTransaction.closed) {
            staticLogger.debug "${webTransaction} already closed"
        } else {
            try {
                if (webTransaction.testMode) {
                    staticLogger.debug "${webTransaction} removing from thread"
                    threadTestInstance.remove()
                } else {
                    staticLogger.debug "${webTransaction} removing from request"
                    GrailsWebRequest grailsRequest = WebUtils.retrieveGrailsWebRequest()
                    HttpServletRequest currentRequest = grailsRequest?.getCurrentRequest()
                    currentRequest?.removeAttribute(WebTransaction.WEB_TRANSACTION)
                }
            } catch (t) {
                staticLogger.error "${webTransaction}", GrailsUtil.sanitizeRootCause(t)
            } finally {
                Collection<String> keys = webTransaction.childSessions.keySet()
                keys.each { String key ->
                    try {
                        WebTransactionChildSession childSession = webTransaction.removeChildSession(key)
                        staticLogger.debug "${webTransaction} closing child session ${childSession}"
                        childSession?.closeSession()
                    } catch (t) {
                        staticLogger.error "${webTransaction}", GrailsUtil.sanitizeRootCause(t)
                    }
                }
            }
        }
    }

    static Logger getLogger() {
        currentTransaction?.transactionLogger ?: staticLogger
    }
}
