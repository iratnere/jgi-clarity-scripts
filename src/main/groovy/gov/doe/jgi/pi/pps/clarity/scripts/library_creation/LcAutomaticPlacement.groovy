package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

/**
 * Automates the placement of samples into new containers on the placement screen.
 * The output plate placement should match the input plate placement ( mirror plate ) except Single Cell samples
 * Created by tlpaley on 10/22/15.
 */
class LcAutomaticPlacement extends ActionHandler {

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        LibraryCreationProcess clarityProcess = process as LibraryCreationProcess
        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        clarityProcess.lcAdapter.automaticPlacement()
    }

}
