package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.util.LabelBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import groovy.transform.PackageScope

/**
 * Created by lvishwas on 4/6/2015.
 */
class PacBioAnnealingComplex  extends Analyte{

    @PackageScope
    PacBioAnnealingComplex(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
    }

    Analyte getParentAnalyte() {
        ArtifactNode artifactNode = artifactNodeInterface.singleParentAnalyte
        return AnalyteFactory.analyteInstance(artifactNode) //return ClarityLibraryStock or ClarityLibraryPool
    }

    BigDecimal getUdfPacBioMolarityNm() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_PACBIO_MOLARITY_NM.udf)
    }

    void setUdfPacBioMolarityNm(BigDecimal pacBioMolarity) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_PACBIO_MOLARITY_NM.udf, pacBioMolarity)
    }

    @Override
    String getPrintLabelText() {
        if (isOnPlate) {
            return plateLabelBean() as String
        }
        return new LabelBean(
                pos_1_1: containerName,
                pos_1_2: name,
                pos_1_3: claritySample.sequencingProject.udfMaterialCategory,
                pos_1_5: containerName,
                pos_2_2: name
        ) as String
    }

    LabelBean plateLabelBean(String prefix = '') {
        return new LabelBean(
                pos_1_1: "$prefix$containerId",
                pos_1_2: claritySample.containerUdfLabel,
                pos_1_4: claritySample.sequencingProject.udfMaterialCategory,
                pos_1_5: containerId
        )
    }

    @Override
    Analyte getParentLibrary() {
        return parentAnalyte //could be ClarityLibraryStock or ClarityLibraryPool
    }
}
