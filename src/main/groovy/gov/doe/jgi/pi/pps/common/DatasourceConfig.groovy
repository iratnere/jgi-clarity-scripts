package gov.doe.jgi.pi.pps.common


import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import javax.sql.DataSource

@Configuration
@EnableConfigurationProperties
class DatasourceConfig {

    @Bean
    DataSource datasource() {
        return DataSourceBuilder.create()
        // .driverClassName("oracle.jdbc.driver.OracleDriver")
        .url("jdbc:oracle:thin:@clarity-devdb01.jgi.doe.gov:1521/gendev2")
                .username('USSRW')
                .password('funnyb1z')
                .build();
    }

}
