package gov.doe.jgi.pi.pps.clarity.config

import gov.doe.jgi.pi.pps.clarity_node_manager.util.UdfAttachedTo
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UdfFieldTypes
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UserDefinedField

/*
 * Created by dscott on 4/30/2014.
 *
 * http://frow.jgi-psf.org:8080/api/v2/configuration/udfs
 <cnf:udfs xmlns:cnf="http://genologics.com/ri/configuration">
 <udfconfig attach-to-category="" name="cocktail" attach-to-name="Analyte" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/1"/>
 <udfconfig attach-to-category="" name="comments" attach-to-name="Analyte" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/56"/>
 <udfconfig attach-to-category="ProcessType" name="Risk Level" attach-to-name="SM 100 Assign Bio Safety Level" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/4"/>
 <udfconfig attach-to-category="ProcessType" name="Sample Received" attach-to-name="SM 400 Sample Receipt" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/6"/>
 <udfconfig attach-to-category="ProcessType" name="Sample Received" attach-to-name="Unknown Type" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/7"/>
 <udfconfig attach-to-category="ProcessType" name="Sample Received" attach-to-name="SM 500 Sample QC" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/101"/>
 <udfconfig attach-to-category="ProcessType" name="Sample Received" attach-to-name="Unknown Type" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/102"/>
 <udfconfig attach-to-category="" name="JGI Barcode" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/51"/>
 <udfconfig attach-to-category="" name="Vendor Barcode" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/52"/>
 <udfconfig attach-to-category="" name="Sample Receipt Pass / Fail" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/53"/>
 <udfconfig attach-to-category="" name="Sample Receipt Dry Ice" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/54"/>
 <udfconfig attach-to-category="" name="Sample Receipt Comments" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/55"/>
 <udfconfig attach-to-category="" name="PMO Sample ID" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/2"/>
 <udfconfig attach-to-category="" name="Risk Level" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/3"/>
 <udfconfig attach-to-category="" name="Sample Receipt Comments" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/5"/>
 <udfconfig attach-to-category="" name="Long Comments" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/57"/>
 <udfconfig attach-to-category="" name="RNA / DNA" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/103"/>
 </cnf:udfs>

 <cnf:field xmlns:cnf="http://genologics.com/ri/configuration" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/103" type="String">
 <name>RNA / DNA</name>
 <attach-to-name>Sample</attach-to-name>
 <show-in-lablink>false</show-in-lablink>
 <allow-non-preset-values>false</allow-non-preset-values>
 <first-preset-is-default-value>false</first-preset-is-default-value>
 <show-in-tables>true</show-in-tables>
 <is-editable>false</is-editable>
 <is-deviation>false</is-deviation>
 <is-controlled-vocabulary>false</is-controlled-vocabulary>
 <preset>DNA</preset>
 <preset>RNA</preset>
 <is-required>false</is-required>
 <attach-to-category/>
 </cnf:field>
 */

enum ClarityUdf {

    RESEARCHER_CONTACT_ID('contact_id', UdfAttachedTo.RESEARCHER, UdfFieldTypes.NUMERIC),

    PROJECT_SEQUENCING_PROJECT_ID('PMO Sequencing Project ID', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SAMPLE_CONTACT('Sample Contact', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SAMPLE_CONTACT_ID('Sample Contact Id', UdfAttachedTo.PROJECT, UdfFieldTypes.NUMERIC),
    PROJECT_SEQUENCING_PROJECT_NAME('Sequencing Project Name', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SEQUENCING_PRODUCT_NAME('Sequencing Product Name', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SEQUENCING_PROJECT_PI('Sequencing Project PI', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SEQUENCING_PROJECT_PI_CONTACT_ID('Sequencing Project PI Contact Id', UdfAttachedTo.PROJECT, UdfFieldTypes.NUMERIC),
    PROJECT_PROPOSAL_ID('Proposal Id', UdfAttachedTo.PROJECT, UdfFieldTypes.NUMERIC),
    PROJECT_SEQUENCING_PROJECT_MANAGER_ID('Sequencing Project Manager Id', UdfAttachedTo.PROJECT, UdfFieldTypes.NUMERIC),
    PROJECT_SEQUENCING_PROJECT_MANAGER('Sequencing Project Manager', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_MATERIAL_CATEGORY('Material Category', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_AUTO_SCHEDULE_SOW_ITEMS('AutoSchedule Sow Items', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_CREATED_BY('Created By', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),

    CONTAINER_JGI_BARCODE('JGI Barcode', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_VENDOR_BARCODE('Vendor Barcode', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_PMO_SAMPLE_ID('PMO Sample ID',UdfAttachedTo.CONTAINER,UdfFieldTypes.NUMERIC),
    CONTAINER_SAMPLE_RECEIPT_PASS_FAIL('Sample Receipt Pass / Fail', UdfAttachedTo.CONTAINER, UdfFieldTypes.BOOLEAN),
    CONTAINER_SAMPLE_RECEIPT_DRY_ICE('Sample Receipt Dry Ice', UdfAttachedTo.CONTAINER, UdfFieldTypes.BOOLEAN),
    CONTAINER_SAMPLE_RECEIPT_COMMENTS('Sample Receipt Comments', UdfAttachedTo.CONTAINER, UdfFieldTypes.TEXT),
    CONTAINER_LABEL('Label', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_NOTES('Notes', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_FINAL_ALIQUOT_VOLUME_UL('Final Aliquot Volume (uL)', UdfAttachedTo.CONTAINER, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    CONTAINER_FINAL_ALIQUOT_MASS_NG('Final Aliquot Mass (ng)', UdfAttachedTo.CONTAINER, UdfFieldTypes.NUMERIC, UdfUnits.NG),
    CONTAINER_QUEUE_DATE('Queue Date', UdfAttachedTo.CONTAINER, UdfFieldTypes.DATE),

    CONTAINER_LOCATION('Location', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_LOCATION_DATE('Location Date', UdfAttachedTo.CONTAINER, UdfFieldTypes.DATE),
    CONTAINER_FREEZER_LOCATION('Freezer Location', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_LIBRARY_QC_FAILURE_MODE('Library QC Failure Mode', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_LIBRARY_QC_RESULT('Library QC Result', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_SAMPLE_QC_RESULT('Sample QC Result', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_LIBRARY_QPCR_RESULT('Library qPCR Result', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_LAB_RESULT('Lab Result', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),

    SAMPLE_BIOSAFETY_MATERIAL_CATEGORY('Biosafety Material Category',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_ESTIMATED_GENOME_SIZE_MB('Estimated Genome Size (mb)',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC),
    SAMPLE_LC_ATTEMPT('LC Attempt',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC),
    SAMPLE_LC_FAILED_ATTEMPT('LC Failed Attempt',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC),
    SAMPLE_DEGREE_OF_POOLING('Degree of Pooling', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_RUN_MODE('Run Mode', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_ITAG_PRIMER_SET('iTag Primer Set', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_EXOME_CAPTURE_PROBE_SET('Exome Capture Probe Set', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_LIBRARY_CREATION_QUEUE_ID('Library Creation Queue Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_LIBRARY_CREATION_SPECS_ID('Library Creation Specs Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_MATERIAL_TYPE('Material Type', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_VOLUME_UL('Volume (uL)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_INITIAL_VOLUME_UL('Initial Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_GEL_DILUTE_VOLUME_UL('Gel Diluent Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_PURITY_VOLUME_UL('Purity Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_QUALITY_VOLUME_UL('Quality Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_QUANTITY_VOLUME_UL('Quantity Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_TARGET_ALIQUOT_MASS_NG('Target Aliquot Mass (ng)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.NG),
    SAMPLE_TARGET_TEMPLATE_SIZE_BP('Target Template Size (bp)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.BP),
    SAMPLE_NOTES('Notes', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SEQUENCING_PROJECT_PI('Sequencing Project PI',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_FORMAT('Format',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_COLLABORATOR_VOLUME_UL('Collaborator Volume (ul)',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_COLLABORATOR_CONCENTRATION_NGUL('Collaborator Concentration (ng/ul)',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC, UdfUnits.NG_PER_UL),
    SAMPLE_COLLABORATOR_SAMPLE_NAME('Collaborator Sample Name',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_ID('SOW Item ID', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_NUMBER('Sow Item Number', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_LIMSID('Sample LIMSID', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SCHEDULED_SAMPLE('Scheduled Sample', UdfAttachedTo.SAMPLE, UdfFieldTypes.BOOLEAN),
    SAMPLE_LOGICAL_AMOUNT_UNITS('Logical Amount Units',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_TYPE('Sow Item Type',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_TARGET_LOGICAL_AMOUNT('Target Logical Amount',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC),
    SAMPLE_BLR_RECEIPT('BLR and Receipt',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_CONTACT('Sample Contact',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_LABEL('Label', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_LOGICAL_AMOUNT_COMPLETED('Total Logical Amount Completed',UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_ACCOUNT_ID('Account ID',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_TARGET_INSERT_SIZE_KB('Target Insert Size (kb)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_LIBRARY_INDEX_SET('Library Index Set',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SUBMISSION_ID('Submission ID',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_QC_TYPE('Sow Item QC Type',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_QC_TYPE_ID('Sow Item QC Type ID',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_PURPOSE('Sow Item Purpose',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_LINKED_SOW_ITEM_ID('Linked Sow Item ID',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_LINKED_SOW_ITEM_SUBMISSION_ID('Linked Sow Item Submission ID',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_TIGHT_INSERT('Tight Insert',UdfAttachedTo.SAMPLE, UdfFieldTypes.BOOLEAN),
    SAMPLE_RRNA_DEPLETION('rRNA Depletion',UdfAttachedTo.SAMPLE, UdfFieldTypes.BOOLEAN),
    SAMPLE_OVERLAPPING_READS('Overlapping Reads',UdfAttachedTo.SAMPLE, UdfFieldTypes.BOOLEAN),
    SAMPLE_POLYA_SELECTION('PolyA Selection',UdfAttachedTo.SAMPLE, UdfFieldTypes.BOOLEAN),
    SAMPLE_SCIENTIFIC_PROGRAM('Scientific Program',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_STOP_AT_RECEIPT('Stop at Receipt',UdfAttachedTo.SAMPLE, UdfFieldTypes.BOOLEAN),
    SAMPLE_RECEIPT_NOTES('Sample Receipt Notes', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_RECEIPT_MISSING_DRY_ICE('Sample Receipt Missing Dry Ice', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_RECEIPT_DATE('Sample Receipt Date', UdfAttachedTo.SAMPLE, UdfFieldTypes.DATE),
    SAMPLE_RECEIPT_RESULT('Sample Receipt Result', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_QC_RESULT('Sample QC Result', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_QC_FAILURE_MODE('Sample QC Failure Mode', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_QC_RESULT('Sow Item QC Result', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_QC_FAILURE_MODE('Sow Item QC Failure Mode', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CUMULATIVE_QC_TYPE_ID('Cumulative QC Type Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_CURRENT_QC_TYPE('Current QC Type', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_QC_TYPE_ID('QC Type Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_REQUIRED_QC_TYPE('Required QC Type', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_QC_DATE('Sample QC Date', UdfAttachedTo.SAMPLE, UdfFieldTypes.DATE),
    SAMPLE_QC_INSTRUMENT('Sample QC Instrument', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CONCENTRATION_NG_UL('Concentration (ng/uL)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.NG_PER_UL),
    SAMPLE_ABSORBANCE_260_230('Absorbance 260/230', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_ABSORBANCE_260_280('Absorbance 260/280', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_HMW_GDNA_Y_N('HMW gDNA Eval', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_QUALITY_SCORE('Quality Score', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_RRNA_RATIO('rRNA Ratio', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_QC_NOTES('Sample QC Notes', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SM_INSTRUCTIONS('SM Instructions', UdfAttachedTo.SAMPLE, UdfFieldTypes.TEXT),
    SAMPLE_LC_INSTRUCTIONS('LC Instructions', UdfAttachedTo.SAMPLE, UdfFieldTypes.TEXT),
    SAMPLE_SQ_INSTRUCTIONS('SQ Instructions', UdfAttachedTo.SAMPLE, UdfFieldTypes.TEXT),
    SAMPLE_EXTERNAL('External', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CONTROL_TYPE('Control Type', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CONTROL_ORGANISM_NAME('Control Organism Name', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CONTROL_ORGANISM_TAX_ID('Control Organism Tax Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_DEST_CONTAINER_NAME('Destination Container Name', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_DEST_CONTAINER_LOCATION('Destination Container Location', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_POOL_NUMBER('Pool Number', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_BATCH_ID('Batch Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_INTERNAL_COLLABORATOR_SAMPLE_NAME('Internal Collaborator Sample Name', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_GROUP_NAME('Group Name', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_ISOTOPE_LABEL('Isotope Label', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_METABOLOMICS_SAMPLE_ID('Metabolomics Sample Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_ISOTOPE_ENRICHMENT('Isotope Enrichment (at%)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),

    //ANALYTE_SUPPORT_NOTES('Support Notes', UdfAttachedTo.ANALYTE, UdfFieldTypes.TEXT),
    ANALYTE_AUTOMATION_NAME('Automation Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_VOLUME_UL('Volume (uL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    ANALYTE_ADJUSTED_ALIQUOT_VOLUME_UL('Adjusted Aliquot Volume (ul)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    ANALYTE_CONCENTRATION_NG_UL('Concentration (ng/uL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.NG_PER_UL),
    ANALYTE_DENSITY_G_ML('Density (g/mL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_DESTINATION_BARCODE('Destination Barcode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_ACTUAL_TEMPLATE_SIZE_BP('Actual Template Size (bp)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.BP),
    ANALYTE_ACTUAL_INSERT_SIZE_KB('Actual Insert Size (kb)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.KB),
    ANALYTE_LIBRARY_MOLARITY_QC_PM('Library Molarity QC (pm)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.PM),
    ANALYTE_LIBRARY_MOLARITY_PM('Library Molarity (pm)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.PM),
    ANALYTE_INDEX_NAME('Index Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_INDEX_CONTAINER_BARCODE('Index Container Barcode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_NOTES('Notes', UdfAttachedTo.ANALYTE,UdfFieldTypes.STRING),
    ANALYTE_NUMBER_PCR_CYCLES("Number of PCR Cycles", UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_ACTUAL_LIBRARY_CREATION_QUEUE_ID('Actual Library Creation Queue Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_CREATION_QUEUE_ID('Library Creation Queue Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_CREATION_QUEUE('Library Creation Queue', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_CREATION_SPECS_ID('Library Creation Specs Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_CREATOR('Library Creator', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_QC_FAILURE_MODE('Library QC Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_QC_RESULT('Library QC Result', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_RUN_MODE('Run Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_DEGREE_POOLING('Degree of Pooling', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_CONVERSION_FACTOR('Library Conversion Factor', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_PHIX_SPIKE_IN('PhiX Spike in %', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_PROTOCOL_ID('Protocol Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_FAILURE_MODE('Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_CLUSTER_GEN_FAILURE_MODE('Cluster Generation Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_SEQUENCING_FAILURE_MODE('Sequencing Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_QPCR_FAILURE_MODE('Library qPCR Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_QPCR_RESULT('Library qPCR Result', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LAB_RESULT('Lab Result', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_VOLUME_USED_UL('Volume Used (uL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    ANALYTE_MDA('MDA', UdfAttachedTo.ANALYTE, UdfFieldTypes.BOOLEAN),
    ANALYTE_SAMPLE_COMMENTS('Sample Comments', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_SPIKE_IN_CONTROL('Spike-In Control', UdfAttachedTo.ANALYTE, UdfFieldTypes.BOOLEAN),
    ANALYTE_SPIKE_IN_NAME('Spike-In Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_ACQUISITION_TIME('Acquisition Time', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_PACBIO_INSERT_SIZE('PacBio Insert Size', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_SEQUENCING_MODE('Sequencing Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_GENERATE_CCS_DATA('Generate CCS Data', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_SIZE_SELECTION('Size Selection', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_SAMPLE_DESCRIPTION('Sample Description', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_REUSE_COMPLEX('Reuse Complex', UdfAttachedTo.ANALYTE, UdfFieldTypes.BOOLEAN),
    ANALYTE_USE_2ND_LOOK('Use 2nd Look', UdfAttachedTo.ANALYTE, UdfFieldTypes.BOOLEAN),
    ANALYTE_NUMBER_OF_COLLECTIONS('Number of Collections', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_STAGE_START('Stage Start', UdfAttachedTo.ANALYTE, UdfFieldTypes.BOOLEAN),
    ANALYTE_BASECALLER('Basecaller', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_SECONDARY_ANALYSIS_PROTOCOL('Secondary Analysis Protocol', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_COLLECTION_PROTOCOL('Collection Protocol', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_MAGBEAD_CLEANUP_DATE('MagBead Cleanup Date', UdfAttachedTo.ANALYTE, UdfFieldTypes.DATE),
    ANALYTE_PACBIO_MOLARITY_NM('PacBio Molarity (nm)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.NM),
    ANALYTE_FINAL_ALIQUOT_MASS_NG('Final Aliquot Mass (ng)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.NG),
    ANALYTE_FINAL_ALIQUOT_VOLUME_UL('Final Aliquot Volume (uL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    ANALYTE_EXTERNAL('External', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_COLLABORATOR_LIBRARY_NAME('Collaborator Library Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_COLLABORATOR_POOL_NAME('Collaborator Pool Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_IMMOBILATION_DURATION('Immobilization Duration', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALAYTE_SEQUENCING_KIT_BOX_BARCODE('Sequel Sequencing Kit Box Barcode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALAYTE_DNA_CONTROL_COMPLEX_BOX_BARCODE('Sequel DNA Control Complex Box Barcode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_CREATION_SITE('Library Creation Site', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_PACBIO_LOADING_CONCENTRATION_NM('PacBio Loading Concentration (nM)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_PERCENTAGE_SOF('Library Percentage with SOF (%)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LP_ACTUAL_WITH_SOF('Library Actual Percentage with SOF (%)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_PEU_COUNTDOWN('PEU Countdown', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_KAPA_LOT_ID('Kapa Lot Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_KAPA_BATCH_ID('Kapa Batch Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_KAPA_TAG_NAME('Kapa Tag Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_KAPA_TAG_SEQUENCE('Kapa Tag Sequence', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_KAPA_TAG_MASS_NG('Kapa Tag Mass (ng)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),

    PROCESS_BARCODE_PRINTER("Printer", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_QC_TODO("QC TODO", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_FLOWCELL_BARCODE('Flowcell Barcode', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_REAGENT_KIT_LOT('Reagent Kit Lot', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_REAGENT_PLATE_ID('Reagent Plate Id', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_BUFFER_LOT('Buffer Lot', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SEQUENCING_LOT('Sequencing Lot', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_BUFFER_KIT('Buffer Kit', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_ACCESSORY_KIT('Accessory Kit', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_INSTRUMENT('Instrument', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_CLUSTER_KIT('Cluster Kit', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_ANALYZER_BUFFER("Analyzer Buffer", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_ANALYZER_KIT_LOT("Analyzer Kit Lot", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_MULTIPLEXING_LOT("Multiplexing Lot", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_READ_2_RESYNTHESIS_LOT("Read 2 Resynthesis Lot", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DNA_SEQUENCING_KIT_4("DNA Sequencing Kit 4.0", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SMRT_CELL_8PAC_V3("SMRT Cell 8Pac V3", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DNA_POLYMERASE_BINDING_KIT_P6("DNA/Polymerase Binding Kit P6", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_BINDING_KIT_LOT("Binding Kit Lot", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_BINDING_KIT_BOX_BARCODE("Binding Kit Box Barcode", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SMRTBELL_TEMPLATE_PREP_KIT("SMRTbell Template Prep Kit", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT("PacBio SMRTbell Template Prep Kit", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DNA_TEMPLATE_PREP_KIT_BOX_BARCODE("DNA Template Prep Kit Box Barcode", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_AMPURE_XP_BEADS("AMPure XP Beads", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_LIQUID_HANDLING_INSTRUMENT('Liquid Handling Instrument', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_PACBIO_SEQUENCER('PacBio Sequencer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SEQUEL_SEQUENCER('Sequel Sequencer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_WEB_TRANSACTION_ID('Web Transaction ID', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_WEB_TRANSACTION_SUBMISSION_URI('Web Transaction Submission URI', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_BINDING_BUFFER('Binding Buffer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_AMBION_WATER('Ambion Water', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_1X_ELUTION_BUFFER('1x Elution Buffer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SEQUENCING_PRIMER('Sequencing Primer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_10X_PRIMER_BUFFER('10x Primer Buffer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DNTP_V3('dNTP v3', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_COLUMN_CONDITIONING_BUFFER('Column Conditioning Buffer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_CLEAN_UP_COLUMNS('Clean up Columns', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),

    PROCESS_QUBIT_ASSAY_KIT('Qubit Assay Kit', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SMRT_OIL_KIT('SMRT Oil Kit', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_MAGBEAD_LOT('MagBead Lot', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_MAGBEAD_BINDING_BUFFER('MagBead Binding Buffer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_MAGBEAD_WASH_BUFFER('MagBead Wash Buffer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_PACBIO_SEQUEL_SMRT_CELLS_4PAC('PacBio Sequel SMRT Cells 4Pac', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SMRT_LINK_UUID('SMRT Link UUID', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),

    PROCESS_DNA_INTERNAL_CONTROL_COMPLEX('DNA Internal Control Complex', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DNA_INTERNAL_CONTROL_COMPLEX_LOT('DNA Internal Control Complex Lot', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DNA_SEQUENCING_REAGENT_KIT('DNA Sequencing Reagent Kit', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_KAPA_SPIKE_IN_PLATE_BARCODE("Kapa Spike-in Plate Barcode", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_MIN_DNA_CONCENTRATION_NG_UL('Minimum DNA Concentration (ng/uL)', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC),
    PROCESS_REPLICATES_FAILURE_PERCENT_ALLOWED('Replicates Failure Percent Allowed', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC),
    PROCESS_CONTROLS_FAILURE_PERCENT_ALLOWED('Controls Failure Percent Allowed', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC),
    PROCESS_MINIMUM_ISOTOPE_ENRICHMENT('Minimum Isotope Enrichment (at%)', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC),

    final String value
    final UdfAttachedTo udfAttachedTo
    final UdfFieldTypes udfFieldType
    final UdfUnits udfUnits
    final UserDefinedField udf

    final static String DEFAULT_UDF_VALUE = 'Unknown'

    private static final Map<UdfAttachedTo, Map<String, ClarityUdf>> converter = new HashMap<UdfAttachedTo, Map<String, ClarityUdf>>()
    private static boolean converterInitialized = false
    private static void initializeConverter() {
        if (!converterInitialized) {
            synchronized(converter) {
                converter.clear()
                if (!converterInitialized) {
                    for (ClarityUdf udfs : ClarityUdf.values()) {
                        UdfAttachedTo owner = udfs.udfAttachedTo
                        Map<String, ClarityUdf> nameMap = converter[owner]
                        if (!nameMap) {
                            nameMap = [:]
                            converter[owner] = nameMap
                        }
                        if (nameMap.containsKey(udfs.value)) {
                            throw new RuntimeException("duplicate UDF entry for name ${udfs.value} and owner ${owner}")
                        }
                        nameMap[udfs.value] = udfs
                    }
                    converterInitialized = true
                }
            }
        }
    }

    static ClarityUdf toEnum(String udfName, UdfAttachedTo owner = null) {
        ClarityUdf foundUdfs = null
        initializeConverter()
        if (owner) {
            Map<String, ClarityUdf> nameMap = converter[owner]
            if (nameMap) {
                foundUdfs = nameMap[udfName]
            }
        } else {
            for (Map<String, ClarityUdf> nameMap : converter.values()) {
                if (nameMap) {
                    foundUdfs = nameMap[udfName]
                }
                if (foundUdfs) {
                    break
                }
            }
        }
        return foundUdfs
    }

    private ClarityUdf(String value, UdfAttachedTo udfAttachedTo, UdfFieldTypes udfFieldType, UdfUnits udfUnits = null) {
        this.value = value
        this.udfAttachedTo = udfAttachedTo
        this.udfFieldType = udfFieldType
        this.udfUnits = udfUnits
        this.udf = new UserDefinedField(name:value,attachedTo:udfAttachedTo,type:udfFieldType,units:udfUnits, attachToName: udfAttachedTo.value)
    }

    String toString() {return value}
}
