package gov.doe.jgi.pi.pps.clarity.dto
/**
 * Created by dscott on 3/7/2016.
 */
class SampleDto extends GeneusNodeDto {

    ArtifactDto artifact

    protected SampleDto() {}

}
